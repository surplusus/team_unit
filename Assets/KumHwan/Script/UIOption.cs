﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIOption : MonoBehaviour
{
    [SerializeField] bool isSilent = false; 
    public void ChangeText(Text text)
    {
        if (text.text == "켬")
            text.text = "끔";
        else
            text.text = "켬";
    }
    public void ToggleBGM()
    {
        isSilent = !isSilent;
        if (isSilent)
            SoundManager.SetAllSoundsVolume(0);
        else
            SoundManager.SetAllSoundsVolume(1);
    }
}
