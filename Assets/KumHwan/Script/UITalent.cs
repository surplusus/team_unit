﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITalent : MonoBehaviour
{
    public void OnButton(int n)
    {
        OnDown();

        this.transform.GetChild(n).gameObject.SetActive(true);
    }

    public void OnDown()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
