﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInGameLevel : MonoBehaviour
{
    // Start is called before the first frame update
    public Image expBar;
    public Text text1;
    public Text text2;
    Player player;
    DATA.Status status;

    private void Start()
    {
    }

    private string WaitUntilPlayerOn()
    {
        throw new NotImplementedException();
    }

    private void OnEnable()
    {
        EventSystem.EventManager.AddListener(EventSystem.EVENT_TYPE.SEARCHPLAYER, SetActiveExpBar);
    }
    private void OnDisable()
    {
        EventSystem.EventManager.RemoveListener(EventSystem.EVENT_TYPE.SEARCHPLAYER, SetActiveExpBar);
    }

    void SetActiveExpBar(EventSystem.EventHandler e)
    {
        var evt = e as EventSystem.SearchPlayerEvent;

        player = evt.player;
        status = player.Status;

        expBar.fillAmount = status.Exp.GetExpRate();
    }

// Update is called once per frame
    void FixedUpdate()
    {
        if (player != null)
        {
            expBar.fillAmount = status.Exp.GetExpRate();
            text1.text = string.Format($"Lv.{status.Exp.Level}");
            text2.text = string.Format($"Lv.{status.Exp.Level}");
        }
    }
}
