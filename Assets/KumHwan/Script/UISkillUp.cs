﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UISkillUp : MonoBehaviour
{
    // Start is called before the first frame update
    List<Image> images;
    public Button button;
    public List<Sprite> sprites;
    public float moveSpeed;
    public bool IsAdjust = false;
    string selectImageName;
    void Awake()
    {

    }

    // Update is called once per frame
    private void OnEnable()
    {
    }

    IEnumerator OnSkillSlot()
    {
        if (images == null)
            images = new List<Image>(GetComponentsInChildren<Image>());
        
        foreach (var image in images)
        {
            ImageChange(image);
        }
        //Debug.Log(gameObject.transform.parent.name + " : " + moveSpeed);
        button.interactable = false;

        while (moveSpeed > 1.0)
        {
            moveSpeed -= 0.02f;
            ImageMove();
            yield return new WaitForSecondsRealtime(0.01f);
        }

        while(!IsAdjust)
        {
            ImageAdjust();
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }

    void ImageChange(Image image)
    {
        int n = Random.Range(0, sprites.Count);
        image.sprite = sprites[n];
    }
    void ImageAdjust()
    {
        ImageMove();

        foreach (var image in images)
        {
            if(image.transform.position.y > 385 && image.transform.position.y < 390)
            {
                Vector3 pos = image.transform.position;
                pos.y = 385;
                image.transform.position = pos;
                moveSpeed = 0;
                IsAdjust = true;
                selectImageName = image.sprite.name;
                button.interactable = true;
            }
        }
    }
    void ImageMove()
    {
        foreach(var image in images)
        {
            Vector3 pos = image.transform.position;
            pos.y = pos.y - moveSpeed;
            if (pos.y <= -5)
            {
                float gap = -5 - pos.y;
                pos.y = 645 - gap;
                ImageChange(image);
            }

            image.transform.position = pos;
        }

        ImageFillAmount();
    }

    void ImageFillAmount()
    {
        foreach (var image in images)
        {
            if (image.transform.position.y > 385)
            {
                image.fillOrigin = (int)Image.OriginVertical.Bottom;
                image.fillAmount = (500.0f - image.transform.position.y) * 0.01f;
            }
            else
            {
                image.fillOrigin = (int)Image.OriginVertical.Top;
                image.fillAmount = (image.transform.position.y - 280.0f) * 0.01f;
            }
        }
    }

    public void SystemSendEventManager()
    {
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.SELECTABILLITY, new EventSystem.SelectAbillityEvent(int.Parse(selectImageName)));
    }
}
