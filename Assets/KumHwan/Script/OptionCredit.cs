﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionCredit : MonoBehaviour
{
    [SerializeField] float moveSpeed = 0.1f;
    [SerializeField] Vector3 startPos;
    void Start()
    {
        startPos = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = this.transform.position;
        pos.y+=moveSpeed;
        this.transform.position = pos;
    }
    private void OnDisable()
    {
        this.transform.position = startPos;
    }
}
