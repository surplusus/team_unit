﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInGamePauseSound : MonoBehaviour
{
    private Image image;
    [SerializeField] Sprite soundOn;
    [SerializeField] Sprite soundOff;
    bool toggle = true;
    void Start()
    {
        image = this.transform.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {   
    }

    public void Switch()
    {
        if (toggle)
        {
            toggle = false;
            image.sprite = soundOff;
            SoundManager.AllSoundStop();
        }
        else
        {
            toggle = true;
            image.sprite = soundOn;
            SoundManager.AllSoundPlay();
        }
    }
}
