﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInGameCoin : MonoBehaviour
{
    // Start is called before the first frame update
    public Text text1;
    public Text text2;
    float Coin;
    void Start()
    {
        Coin = 0;
    }
    public void OnEnable()
    {
        EventSystem.EventManager.AddListener(EventSystem.EVENT_TYPE.STAGEEND, CoinUpdate);
    }
    void CoinUpdate(EventSystem.EventHandler e)
    {
        var evt = e as EventSystem.StageEndEvent;
        Coin += evt.pointOfCoin;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!text1.text.Equals(Coin.ToString()))
        {
            text1.text = Coin.ToString();
            text2.text = Coin.ToString();
        }
    }


}
