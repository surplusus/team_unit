﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class StageLoading : MonoBehaviour
{
    AsyncOperation async_operation;
    public GameObject progressImage;
    public Text text;
    void Start()
    {
        StartCoroutine(LoadingNextScene("UnitTest"));
    }

    // Update is called once per frame
    void Update()
    {
        Loading();
    }

    float debugTime = 0.0f;
    float overTime = 5.0f;
    int count = 1;
    public bool IsDebug = true;
    void Loading()
    {
        if (IsDebug)
        {
            debugTime += Time.deltaTime;

            float progress = debugTime / overTime;
            int per = (int)(progress * 100);
            if(per > count*(100/20))
            {
                count++;
                Instantiate(progressImage, progressImage.transform.parent.transform);
            }
            Debug.Log("progress :" + progress);
            Debug.Log("per :" + per);

            text.text = "세계 진입 중";
            for(int i = 0; i < per%7; i++)
            {
                text.text = text.text + ".";
            }

            if (debugTime >= overTime)
            {
                async_operation.allowSceneActivation = true;
            }
        }
        else
        {
            //async_operation.progress
            float progress = async_operation.progress;
            Debug.Log(progress);
            //progressBar.fillAmount = progress;
            int per = (int)progress * 100;
            text.text = per.ToString() + "%";

            if (async_operation.progress >= 0.9f)
            {
                async_operation.allowSceneActivation = true;
                //progressBar.fillAmount = 1;
                text.text = "100%";
            }
        }
    }

    IEnumerator LoadingNextScene(string sceneName)
    {
        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();

        async_operation = SceneManager.LoadSceneAsync(sceneName);
        async_operation.allowSceneActivation = false;
        while (async_operation.progress >= 0.9f)
        {
            // async_operation.isDone
            yield return true;
        }

        //async_operation.allowSceneActivation = true;
    }
}
