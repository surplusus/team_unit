﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWorld : MonoBehaviour
{
    [SerializeField] DATA.UserData userData;
    [SerializeField] GameObject MinusImage;
    public void GoToStage()
    {
        if (userData.ActionPoint.MinusRemainder(5))
        {
            MinusImage.SetActive(true);
            MinusImage.GetComponent<Animation>().Play();
            Invoke("MoveNext", 1.0f);
        }
    }
    void MoveNext()
    {
        //GameManager.MoveToInGameScene();
        GameManager.MoveToSceneNameOf("stageloading");
    }
}
