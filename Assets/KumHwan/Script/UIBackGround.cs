﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBackGround : MonoBehaviour
{
    public GameObject prefab;
    public GameObject defaultUI;
    public GameObject stage;

    float dragTime = 0.0f;
    float dragTimeOver;
    float direction = 0.0f;
    float destination = 0.0f;
    float moveSpeed = 0.0f;
    Vector3 lastMousePosition = Vector3.zero;
    public Vector2 delta;
    public bool dragged = false;
    bool moveSW = false;
    bool adjustSW = false;
    public bool isDebug = false;
    RectTransform rect;
    RectTransform stageRect;
    UI_MenuBar_BackBoard defaultUIscript = null;

    int stageCount;
    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                Instantiate(prefab, new Vector3(-28.5f + j*3, -4.5f + i*3, 0), new Quaternion(), transform.GetChild(0).transform);
                Instantiate(prefab, new Vector3(-27 + j*3, -3.0f + i*3, 0), new Quaternion(), transform.GetChild(0).transform);
            }
        }
        defaultUIscript = defaultUI.GetComponentInChildren<UI_MenuBar_BackBoard>();
        rect = GetComponent<RectTransform>();
        stageRect = stage.transform.GetChild(0).GetComponent<RectTransform>();
        stageCount = stage.transform.GetChild(0).childCount;
        dragTimeOver = 0.5f;
        moveSpeed = 100.0f;
    }
    // Update is called once per frame
    void Update()
    {
        Drag();

        if (stage.activeSelf)
        {
            StageMove();
            if (adjustSW)
                StageAdjust();
        }
        else
            Move();

        if (moveSW)
            MoveAI();
        else if (adjustSW)
            Adjust();

    }

    void Drag()
    {
        if (isDebug)
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastMousePosition = Input.mousePosition;
            }

            if (Input.GetMouseButton(0))
            {
                delta = Input.mousePosition - lastMousePosition;
                dragTime += Time.deltaTime;
                if (!dragged && delta.magnitude > 5 && lastMousePosition.y > 100)
                {
                    dragged = true;
                }

                lastMousePosition = Input.mousePosition;
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (dragged && dragTime < dragTimeOver)
                {
                    Vector3 pos = rect.position;

                    if (!stage.activeSelf)
                        moveSW = true;

                    if (direction > 0)
                    {
                        destination = pos.x + 10.9f;
                        if (destination > 21.8f)
                            destination = 21.8f;
                    }
                    else if (direction < 0)
                    {
                        destination = pos.x - 10.9f;
                        if (destination < -21.8f)
                            destination = -21.8f;
                    }
                    else
                        destination = pos.x;
                }
                dragged = false;
                dragTime = 0.0f;

                if (stage.activeSelf)
                    adjustSW = true;
            }
        }
        else
        {
            if (1 <= Input.touchCount)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    case TouchPhase.Moved:
                        if (!dragged && touch.deltaPosition.magnitude > 250)
                        {
                            dragged = true;
                            direction = touch.deltaPosition.x / touch.deltaPosition.magnitude;
                        }
                        break;
                    case TouchPhase.Ended:
                    case TouchPhase.Canceled:
                        dragged = false;
                        break;
                }
            }
        }
    }


    void Move()
    {
        if (dragged)
        {
            if (delta.magnitude == 0)
                return;

            direction = delta.x / delta.magnitude * 10.0f;
            Vector3 pos = rect.position;
            pos.x += direction * Time.deltaTime;

            if (pos.x > 25)
                pos.x = 25;
            else if (pos.x < -25)
                pos.x = -25;

            rect.position = pos;
        }
    }

    void Adjust()
    {
        Vector3 pos = rect.position;

        if (pos.x <= -15.70f) // option
        {
            pos.x = -21.8f;
            defaultUIscript.SelectOptionButton();
        }
        else if(pos.x <= -5.23f) // talent
        {
            pos.x = -10.9f;
            defaultUIscript.SelectTalentButton();
        }
        else if(pos.x <= 5.23f) // world
        {
            if (pos.x < 0)
                defaultUIscript.SelectWorldButton("Left");
            else if (pos.x > 0)
                defaultUIscript.SelectWorldButton("Right");
            pos.x = 0;
        }
        else if(pos.x < 15.70f) // inven
        {
            pos.x = 10.9f;
            defaultUIscript.SelectInventoryButton();
        }
        else // store
        {
            pos.x = 21.8f;
            defaultUIscript.SelectStoreButton();
        }

        rect.position = pos;
        adjustSW = false;
    }

   public void MoveAIOn(float d)
    {
        moveSW = true;
        destination = d;    //  21.8 | 10.9 | 0 | -10.9 | -21.8
    }

    void MoveAI()
    {
        Vector3 pos = rect.position;
        if(System.Math.Abs(pos.x - destination) <= moveSpeed*Time.deltaTime)
        {
            pos.x = destination;
            moveSW = false;
            adjustSW = true;
        }
        else if (pos.x < destination)
            pos.x += moveSpeed*Time.deltaTime;
        else if (pos.x > destination)
            pos.x -= moveSpeed*Time.deltaTime;

        rect.position = pos;
    }

    void StageMove()
    {
        if (dragged)
        {
            if (delta.magnitude == 0)
                return;

            direction = delta.x / delta.magnitude * 10.0f;
            Vector3 pos = stageRect.position;
            Debug.Log(pos.x);
            pos.x += direction * Time.deltaTime;

            if (pos.x > 25)
                pos.x = 25;
            else if (pos.x < -25)
                pos.x = -25;

            stageRect.position = pos;
        }
    }

    void StageAdjust()
    {

        Debug.Log("StageAdjust");
        Vector3 pos = stageRect.position;

        for (int i = 0; i < stageCount; i++)
        {
            if (2.5f+i*-5 > pos.x && pos.x > -2.5f+i*-5)
            {
                pos.x = i * -5;
            }
        }

        if (pos.x > 2.5f) pos.x = 2.5f;
        else if (pos.x < -2.5f + stageCount * -5) pos.x = -2.5f + stageCount * -5;

        stageRect.position = pos;
        adjustSW = false;
    }
}
