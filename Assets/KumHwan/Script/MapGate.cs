﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGate : MonoBehaviour
{
    [SerializeField] Sprite sprite;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void GateOpen(bool isOpen)
    {
        if (isOpen)
        {
            ImageChange();
            ParticleOn();
        }
    }

    private void ImageChange()
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
    }

    private void ParticleOn()
    {
        this.transform.GetChild(0).gameObject.SetActive(true);
    }
}
