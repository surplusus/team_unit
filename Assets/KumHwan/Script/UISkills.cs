﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISkills : MonoBehaviour
{
    // Start is called before the first frame update
    float moveSpeed;
    float gap;
    List<UISkillUp> uISkillUps = new List<UISkillUp>();
    void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            uISkillUps.Add(transform.GetChild(i).GetComponentInChildren<UISkillUp>());
        }
        moveSpeed = 5.0f;
        gap = 2.0f;
    }

    private void OnEnable()
    {
        for (int i = 0; i < uISkillUps.Count; i++)
        {
            uISkillUps[i].moveSpeed = moveSpeed + (i * gap);
            uISkillUps[i].IsAdjust = false;
            uISkillUps[i].StartCoroutine("OnSkillSlot");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
