﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIGameResult : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI stageNum;
    public TextMeshProUGUI chapterNum;
    public TextMeshProUGUI coinNum;
    public GameObject inputArea;
    [SerializeField] Image imageBG;
    [SerializeField] Image imageTitleBG;

    private void OnEnable()
    {
        if (inputArea == null)
            inputArea = GameObject.FindGameObjectWithTag("InputArea");

        inputArea?.SetActive(false);
    }

    public void TurnGreen()
    {
        imageBG.color = new Color(0, 255, 0);
        imageTitleBG.color = new Color(0, 255, 0);
    }
}
