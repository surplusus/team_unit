﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISkillUpCtrl : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject inputArea;
    public GameObject skillUp;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnableUISkillUp()
    {
        skillUp.SetActive(true);
        inputArea = GameObject.FindGameObjectWithTag("InputArea");
        inputArea.SetActive(false);
        Time.timeScale = 0;
    }

    public void DisableUISkillUp()
    {
        skillUp.SetActive(false);
        inputArea.SetActive(true);
        Time.timeScale = 1;
    }
}
