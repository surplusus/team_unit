﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIInGamePause : MonoBehaviour
{
    public GameObject inputArea;
    public GameObject pause;
    public GameObject gameResult;
    public WhiteBG whiteBG;

    public void GoToLobbyScene()
    {
        pause.SetActive(false);
        gameResult.SetActive(false);
        Time.timeScale = 1;
        whiteBG.gameObject.SetActive(true);
        whiteBG.StartReturnRobby();
    }
    public void EnablePause()
    {
        pause.SetActive(true);
        if(inputArea == null)
            inputArea = GameObject.FindGameObjectWithTag("InputArea");

        inputArea.SetActive(false);
        Time.timeScale = 0;
    }
    public void DisablePause()
    {
        pause.SetActive(false);
        inputArea.SetActive(true);
        Time.timeScale = 1;
    }

}
