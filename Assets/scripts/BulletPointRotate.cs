﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPointRotate : MonoBehaviour
{
    // Start is called before the first frame update
    Transform rotateTransform;
    void Start()
    {
        rotateTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.transform.rotation = rotateTransform.rotation;
    }
}
