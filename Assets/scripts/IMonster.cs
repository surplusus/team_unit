﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMonster
{
    int HP { get; set; }
    int EXP { get; set; }
    float Damage { get; set; }
    float MoveSpeed { get; set; }

    void Move();
    void Attack();
    void Die();
    void Hit(float damage);
}
