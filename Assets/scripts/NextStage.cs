﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextStage : MonoBehaviour
{
    GameObject whiteBG;
    new Camera camera;
    WhiteBG whiteBGscript;
    
    private void Start()
    {
        whiteBG = GameObject.FindGameObjectWithTag("WhiteBG");
        whiteBGscript = whiteBG.GetComponent<WhiteBG>();
        camera = GameObject.FindGameObjectWithTag("SubCamera").GetComponent<Camera>();
    }
    void OnTriggerEnter(Collider other)
    {        
        if (other.gameObject.tag == "Player")
        {

            whiteBG.SetActive(true);

            whiteBG.transform.position = camera.WorldToScreenPoint(camera.transform.position);

            whiteBGscript.StartNextStageWhite();
        }
    }
}
