﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventSystem;

public class FoodController : MonoBehaviour
{
    [SerializeField] Animator anim;
    [SerializeField] ParticleSystem[] particle;
    [SerializeField] bool isItem = false;
    [SerializeField] bool isRendomItem = false;
    [ReadOnly(false), SerializeField] string ItemName;
    Transform player;
    public bool IsStageEnd { get; set; }


    private void Awake()
    {
        if (isRendomItem)
        {
            ChangeSpriteByItemName();
        }
    }
    private void OnEnable()
    {
        EventManager.AddListener(EVENT_TYPE.STAGEEND, TurnStageEndOn);
        anim.SetTrigger("PopUp");
        if (isItem)
            PlayParticles();
    }
    void TurnStageEndOn(EventHandler e)
    {
        IsStageEnd = true;
    }
    private void OnDisable()
    {
        IsStageEnd = false;
        EventManager.RemoveListener(EVENT_TYPE.STAGEEND, TurnStageEndOn);
    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            IsStageEnd = true;
            print("Fly Food");
        }
        if (IsStageEnd)
            FlyToThePlayer();

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Empty"))
            print("Empty Animation");
    }

    void FlyToThePlayer()
    {
        if (!gameObject.activeInHierarchy)
            return;

        if (player == null)
            player = GameObject.FindWithTag("Player").transform;
        var dist = Vector3.Distance(transform.position, player.position);

        if (dist < 0.5f)
            PoolOfFoodController.TakeBackPoolingFood(this);
        else
            transform.position = Vector3.Lerp(transform.position, player.position, 0.1f);
    }
    void PlayParticles()
    {
        if (particle.Length == 0 || particle == null)
            return;

        foreach (var it in particle)
            it.Play();
    }

    void ChangeSpriteByItemName()
    {
        //var sList = DataManager.Me.ItemIndicatorScript.SpriteNames;
        //int index = Random.Range(0, sList.Count-1);
        //ItemName = sList[index];
        //var sprite = DataManager.GetSpriteFromItemIndicator(ItemName);
    }
}
