﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EventSystem
{
    public enum EVENT_TYPE
    {
        NONE = -1,
        TEST,
        HIT,
        DEAD,
        SELECTABILLITY,
        MONSTERCOUNT,
        SEARCHPLAYER,
        DATA_SAVESO,
        DATA_LOADPREB,
        DATA_LOADSO,
        DATA_LOADSLOT,
        EPUIPMENT_OPENFLOATINGWINDOW,
        UI_MOVEAI,
        STAGEEND,
        END,
    }

    public abstract class EventHandler
    {
        public string eventDescription;
    }
    public class UI_MoveAI_Event : EventHandler
    {
        public UI_MoveAI_Event(UIBackGround script, float dest) 
        { 
            this.script = script; this.destination = dest; 
        }
        public UIBackGround script;
        private float destination;
        public enum WindowType : int { NONE = -1, STORE = 0, EQUIPMENT, WORLD, TALENT, OPTION, }
        public WindowType WindowName
        {//21.8 | 10.9 | 0 | -10.9 | -21.8
            get
            {
                switch (destination)
                {
                    case 21.8f:
                        return WindowType.STORE;
                    case 10.9f:
                        return WindowType.EQUIPMENT;
                    case 0f:
                        return WindowType.WORLD;
                    case -10.9f:
                        return WindowType.TALENT;
                    case -21.8f:
                        return WindowType.OPTION;
                    default:
                        return WindowType.NONE;
                }
            }
        }
    }
    public class StageEndEvent : EventHandler
    {
        public StageEndEvent(int pointOfCoin, int pointOfHeart, List<string> getTypeOfItem) {
            this.pointOfCoin = pointOfCoin;
            this.pointOfHeart = pointOfHeart;
            this.getTypeOfItem = new List<string>(getTypeOfItem);
        }
        public int pointOfCoin;
        public int pointOfHeart;
        public List<string> getTypeOfItem;
    }
    public class SearchPlayerEvent : EventHandler
    {
        public SearchPlayerEvent(Player _player) { this.player = _player; }
        public Player player;
    }
    public class PlayerDeadEvent : EventHandler
    {
        public PlayerDeadEvent() { }
    }
    public class HitEvent : EventHandler
    {
        public HitEvent(int value, Vector3 pos) { damage = value; position = pos; }
        public int damage;
        public Vector3 position;
    }
    public class SelectAbillityEvent : EventHandler
    {
        public SelectAbillityEvent(int spriteName) { this.spriteName = spriteName; }
        public int spriteName;
    }
    public class MonsterCountEvent : EventHandler
    {
        public MonsterCountEvent() { }
    }
    public class textEvent : EventHandler
    {
        public textEvent(string test) { this.text = test; }
        public string text;
    }
    public class DataSaveScriptableObejctEvent : EventHandler
    {
        public DataSaveScriptableObejctEvent() { }
    }
    public class DataLoadPrebEvent : EventHandler
    {
        public DataLoadPrebEvent() { }
    }
    public class DataLoadScriptableObejctEvent : EventHandler
    {
        public DataLoadScriptableObejctEvent() { }
    }
    public class DataLoadItemSlotIndicatorEvent : EventHandler
    {
        public DataLoadItemSlotIndicatorEvent() { }
    }
    public class LobbyEquipmentFloatingWindowEvent : EventHandler
    {
        public LobbyEquipmentFloatingWindowEvent(UI_Lobby_Inventory_ItemIcon _script)
        {
            this.script = _script;
        }
        public UI_Lobby_Inventory_ItemIcon script;
    }
}

