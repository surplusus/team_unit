﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventSystem;

public class EventManagerTEST : MonoBehaviour
{
    private void Awake()
    {

        EventManager.AddListener(EVENT_TYPE.TEST, TestEvent);
    }
    void TestEvent(EventHandler e)
    {
        var evt = e as textEvent;
        print(evt.text);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            EventManager.InvokeEvent(EVENT_TYPE.TEST, new textEvent("잘되는가?"));
    }
}
