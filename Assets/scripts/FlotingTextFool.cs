﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventSystem;

public class FlotingTextFool : MonoBehaviour
{
    List<FlotingText> flotingTexts = new List<FlotingText>();
    int foolCursor = 0;

    private void Awake()
    {
        EventManager.AddListener(EVENT_TYPE.HIT, StartTextCoroutine);
    }
    private void OnDisable()
    {
        EventManager.RemoveListener(EVENT_TYPE.HIT, StartTextCoroutine);
    }
    void Start()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            flotingTexts.Add(transform.GetChild(i).GetComponent<FlotingText>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void StartTextCoroutine(EventHandler e)
    {
        var evt = e as EventSystem.HitEvent;

        flotingTexts[foolCursor].gameObject.SetActive(true);
        flotingTexts[foolCursor].StartHitText(evt.damage, evt.position);

        foolCursor++;
        if (foolCursor >= 50) foolCursor = 0;
    }
}
