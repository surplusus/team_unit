﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class PoolOfFoodController : MonoBehaviour
{
    static public PoolOfFoodController FoodCock = null;
    public enum TYPE : int
    {
        NONE=-1, HEART=0, COIN, ITEM,
    }
    [SerializeField] GameObject heartPreb;
    [SerializeField] GameObject coinPreb;
    [SerializeField] GameObject itemPreb;
    [SerializeField] float randomRange;
    [SerializeField] GameObject Debug;

    Stack<GameObject> poolOfHeart;
    Stack<GameObject> poolOfCoin;
    Stack<GameObject> poolOfItem;

    int pointOfHeart = 0;
    int pointOfCoin = 0;
    List<string> getTypeOfItem = new List<string>();
    private void Start()
    {
        FoodCock = this;
        StartCoroutine(WaitUntilDataSetUp());
    }
    IEnumerator WaitUntilDataSetUp()
    {
        yield return new WaitUntil(() => DataManager.IsAllItemSlotObjectsLoaded);
        poolOfHeart = new Stack<GameObject>(5);
        poolOfCoin = new Stack<GameObject>(25);
        poolOfItem = new Stack<GameObject>(5);
        AddGameObjectInPoolOf(TYPE.HEART, 5);
        AddGameObjectInPoolOf(TYPE.COIN, 25);
        AddGameObjectInPoolOf(TYPE.ITEM, 5);
    }
    private void OnDestroy()
    {
        FoodCock = null;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
            ShowFoods(Debug.transform.position, TYPE.HEART, 3);
        if (Input.GetKeyDown(KeyCode.H))
            ShowFoods(Debug.transform.position, TYPE.COIN, 5);
        if (Input.GetKeyDown(KeyCode.N))
            ShowFoods(Debug.transform.position, TYPE.ITEM, 3);

    }
    void AddGameObjectInPoolOf(TYPE type, int count)
    {
        GameObject pool = null;
        GameObject preb = null;
        Stack<GameObject> stack = null;
        System.Func<string, GameObject> selecPool = delegate (string n)
        {
            GameObject p = transform.Find(n)?.gameObject;
            if (p == null)
            {
                p = new GameObject(n);
                p.transform.SetParent(transform);
            }
            return p;
        };
        switch (type)
        {
            case TYPE.HEART:
                pool = selecPool("PoolOfHeart");
                preb = heartPreb;
                stack = poolOfHeart;
                break;
            case TYPE.COIN:
                pool = selecPool("PoolOfCoin");
                preb = coinPreb;
                stack = poolOfCoin;
                break;
            case TYPE.ITEM:
                pool = selecPool("PoolOfItem");
                preb = itemPreb;
                stack = poolOfItem;
                break;
            default:
                break;
        }
        for (int i = 0; i < count; ++i)
        {
            var inst = Instantiate<GameObject>(preb, pool.transform);
            inst.SetActive(false);
            stack.Push(inst);
        }
    }
    Stack<GameObject> SelectStackOf(TYPE type)
    {
        switch (type)
        {
            case TYPE.HEART:
                return poolOfHeart;
            case TYPE.COIN:
                return poolOfCoin;
            case TYPE.ITEM:
                return poolOfItem;
            default:
                break;
        }
        return null;
    }
    public List<GameObject> ShowFoods(Vector3 approxPos, TYPE type, int count)
    {
        List<GameObject> result = new List<GameObject>(count);
        System.Func<TYPE, int, IEnumerable<GameObject>> GetRangeOf = delegate (TYPE ty, int cnt) 
        {
            var stack = SelectStackOf(ty);
            if (stack.Count < cnt)
                AddGameObjectInPoolOf(ty, cnt - stack.Count);
            GameObject[] re = new GameObject[cnt];
            for (int i = 0; i < cnt; ++i)
            {
                re[i] = stack.Peek();
                stack.Pop();
            }
            return re;
        };

        result = GetRangeOf(type, count).ToList();
        for ( int i = 0; i < result.Count; ++i)
        {
            var dx = Random.Range(-randomRange + approxPos.x, +randomRange + approxPos.x);
            var dz = Random.Range(-randomRange + approxPos.z, +randomRange + approxPos.z);
            result[i].transform.position = new Vector3(dx, 0, dz);
            result[i].SetActive(true);
        }
        return result;
    }
    static public List<GameObject> ShowFood(Vector3 approxPos, TYPE type, int count, string pointsOrItemName)
    {
        switch (type)
        {
            case TYPE.HEART:
                FoodCock.pointOfHeart += int.Parse(pointsOrItemName);
                break;
            case TYPE.COIN:
                FoodCock.pointOfCoin += int.Parse(pointsOrItemName);
                break;
            case TYPE.ITEM:
                FoodCock.getTypeOfItem.Add(pointsOrItemName);
                break;
            default:
                break;
        }
        return FoodCock.ShowFoods(approxPos, type, count);
    }
    static public void TakeBackPoolingFood(FoodController food)
    {
        food.gameObject.SetActive(false);
        food.transform.position = Vector3.zero;
        if (food.name.Contains("Heart"))
            FoodCock.poolOfHeart.Push(food.gameObject);
        else if (food.name.Contains("Coin"))
            FoodCock.poolOfCoin.Push(food.gameObject);
        else if (food.name.Contains("Item"))
            FoodCock.poolOfItem.Push(food.gameObject);
    }
    static public void StageEnd()
    {
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.STAGEEND,
            new EventSystem.StageEndEvent(FoodCock.pointOfCoin, FoodCock.pointOfHeart, FoodCock.getTypeOfItem));

        FoodCock.pointOfHeart = 0;
        FoodCock.pointOfCoin = 0;
        FoodCock.getTypeOfItem.Clear();
    }
}
