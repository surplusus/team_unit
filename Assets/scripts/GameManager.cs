﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Cinemachine;

// sealed 한정사를 사용하여 상속이 불가하도록 조치
public sealed class GameManager : MonoBehaviour
{
    #region MakeSingleton
    private static GameManager Inst = null;
    public static GameManager Me    //  Inst의 프로퍼티(Me로 접근해 쓴다)
    {
        get
        {
            if (Inst == null)
                Inst = FindObjectOfType<GameManager>();
            return Inst;
        }
    }
    private void Awake()
    {
        if (Inst == null)
        {
            SetUp();
            Inst = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Inst != this)
            Destroy(gameObject);
    }
    #endregion
    #region Member
    [ReadOnly(false)] public InputManager inputManager;
    [ReadOnly(false)] public DataManager dataManager;
    [ReadOnly(false), SerializeField] UIManager UIManager;
    [ReadOnly(false), SerializeField] SoundManager MainSoundManager;
    [ReadOnly(false), SerializeField] Scene currentScene;
    [SerializeField] UnityEvent OnIntroScene = new UnityEvent();
    [SerializeField] UnityEvent OnLobbyScene = new UnityEvent();
    [SerializeField] UnityEvent OnInGameScene = new UnityEvent();
    public SoundManager MainSound { get => MainSoundManager; }
    public User USER { get; set; }
    event System.Action UpdateEvent;
    [SerializeField] GameObject prebDatamanager;
    [SerializeField] GameObject quitmsgPreb;
    Animation quitAnim;
    #endregion

    void Update()
    {
        CheckCheatCode();
        CheckQuitGame();
    }
    void SetUp()
    {   // 여기에 시작해서 죽으면 안되는 일을 쓴다 (Awake에서 불림)
        Screen.SetResolution(640, 960, true);
        CreateDataManager();
        UIManager = GetComponent<UIManager>();
        USER = gameObject.GetComponent<User>();
        inputManager = InputManager.Instantiate();
        MainSoundManager = GetComponent<SoundManager>();
        SceneManager.sceneLoaded += AutoSetUpWhenStartingScene;
        quitAnim = Instantiate(quitmsgPreb, transform, true).GetComponent<Animation>();
    }
    public void CreateDataManager()
    {
        var dataObj = Instantiate(prebDatamanager);
        dataObj.name = "DataManager";
        dataManager = DataManager.Me;
    }
    void AutoSetUpWhenStartingScene(Scene scene, LoadSceneMode mode)
    {
        string sceneName = GetCurrentSceneName();
        OperateFuncBySceneNameOf(sceneName);
    }
    void OperateFuncBySceneNameOf(string sceneName)
    {
        // 씬에서 각각 할일들 함수
        switch (sceneName)
        {
            case "intro":
                IntroFunc();
                break;
            case "lobby":
                LobbyFunc();
                break;
            case "stageLoading":
                StageLoadingFunc();
                break;
            case "game":
                InGameFunc();
                break;
            case "debug":
                LobbyFunc();
                break;
            default:
                break;
        }
    }
    static public string GetCurrentSceneName()
    {
        if (SceneManager.GetActiveScene().path.Contains("Guhyun"))
            return "debug";
        else if (SceneManager.GetActiveScene().path.Contains("KumHwan"))
            return "debug";
        else if (SceneManager.GetActiveScene().name.Equals("0MainLoadingScene"))
            return "intro";
        else if (SceneManager.GetActiveScene().name.Equals("1LobbyScene"))
            return "lobby";
        else if (SceneManager.GetActiveScene().name.Equals("2StageLoadingScene"))
            return "stageLoading";
        else if (SceneManager.GetActiveScene().name.Equals("3InGameScene"))
            return "game";
        return "";
    }
    void IntroFunc()
    {
        UIManager.Me.LoadMainLoadingWindow();
        GameManager.Me.MainSound.SoundPlay("Opening");
        OnIntroScene?.Invoke();
    }
    void LobbyFunc()
    {
        // 로비신에서 할일들
        MainSound.soundList.Find(x => x.Name.Equals("LobbyBGM")).IsLoop = true;
        GameManager.Me.MainSound.SoundPlay("LobbyBGM");
        // Inspecter에서 불러온 할일들
        OnLobbyScene?.Invoke();
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.DATA_LOADSLOT
            , new EventSystem.DataLoadItemSlotIndicatorEvent());
    }
    void InGameFunc()
    {
        // 인게임 시작하면 할일들
        DataManager.InstantiateRollScheduler();
        // Inspector에서 등록된 할일들
        OnInGameScene?.Invoke();
    }
    void StageLoadingFunc()
    {
        UIManager.Me.LoadStageLoadingWindow();

    }
    static public void MoveToSceneNameOf(string sceneName)
    {
        List<string> names = new List<string>(4) { "intro", "lobby", "stageloading", "game" };
        SceneManager.LoadScene(names.IndexOf(sceneName));
    }
    static public void MoveToInGameScene()
    {
        SceneManager.LoadScene("2StageLoadingScene");
        InputManager.Instantiate();
    }
    #region Cheat
    int CurrentSceneIndex = 0;
    string CheatCode = "";
    public event System.Action CheatEvent;
    bool CheckCheatCode()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            CheatCode += "B";
            print(CheatCode);
        }
        if (CheatCode != null && CheatCode == "BBB")
        {
            CheatEvent();
            CheatCode = "";
            return true;
        }

        return false;
    }
    #endregion
    float cntQuitmsg = 0;
    float quitTimer = 5f;
    void CheckQuitGame()
    {
        if (InputManager.IsQuitPushed())
        {
            if (cntQuitmsg > 0.01f && cntQuitmsg < quitTimer - 0.01f)
            {
                Application.Quit();
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
                Application.OpenURL("http://google.com");
#endif
            }
            cntQuitmsg = quitTimer;
            quitAnim.Play("QuitMassage_Start");
        }

        if (cntQuitmsg <= 0f)
            return;

        cntQuitmsg -= Time.deltaTime * 2f;
        
        if (cntQuitmsg <= 0f)
        {
            quitAnim.Play("QuitMassage_End");
            cntQuitmsg = 0f;
        }
    }
}
