﻿using UnityEngine;
using System;

#if UNITY_EDITOR
namespace UnityEditor
{
    //
    // 요약:
    //     Inspector에서 수정 할 수 없게 만든다
    //     [ReadOnly(false)]일 경우 RunTime이 아니더라도 수정할 수 없다
    //     (readonly 키워드 대체)
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute), true)]
    public class ReadOnlyAttributeDrawer : PropertyDrawer
    {
        protected virtual ReadOnlyAttribute Attribute => attribute as ReadOnlyAttribute;
        // Necessary since some properties tend to collapse smaller than their content
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        // Draw a disabled property field
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = !Application.isPlaying && Attribute.runtimeOnly;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }
}
#endif

[AttributeUsage(AttributeTargets.Field)]
public sealed class ReadOnlyAttribute : PropertyAttribute
{
    //
    // 요약:
    //     I wanna explain this Attribute. but, I can't
    public ReadOnlyAttribute(bool runtimeOnly = true)
    {
        this.runtimeOnly = runtimeOnly;
    }
    //
    // 요약:
    //     게임이 실행중일때만 수정 가능 (false일 경우 늘 수정 불가능)
    public bool runtimeOnly { get; private set; }
}