﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAbility
{
	DATA.AbilityType TypeName { get; }
}