﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterHpBar : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject rotate;
    [SerializeField] Image hpBar;
    Transform rotateTransform;
    IMonster monster;
    float maxHP = 0.0f;
    void Start()
    {
        rotateTransform = rotate.GetComponent<Transform>();

        monster = rotate.GetComponent<BlindMeleeMonster>();
        if (monster == null)
            monster = rotate.GetComponent<BlindRangeMonster>();
        if (monster == null)
            monster = rotate.GetComponent<OpticalMeleeMonster>();
        if (monster == null)
            monster = rotate.GetComponent<OpticalRangeMonster>();
        maxHP = monster.HP;

        hpBar.fillAmount = monster.HP / maxHP;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.transform.position = rotateTransform.position + new Vector3(0, 0, 1);//this.transform.position;
        hpBar.fillAmount = monster.HP / maxHP;
    }
}
