﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DATA;
using Item;
using System;

public class ShowInventory : MonoBehaviour
{
    public List<IItem> items;
    public DATA.Condition Condition { get => condition; set => condition = value; }
    public DATA.UserData UserData { get => userData; set => userData = value; }
    [ReadOnly(false), SerializeField] DATA.Condition condition = null;
    [ReadOnly(false), SerializeField] UserData userData;
    [ReadOnly(false), SerializeField] Weapon weapon;
    [ReadOnly(false), SerializeField] Armor armor;
    [ReadOnly(false), SerializeField] Accessory accessory1;
    [ReadOnly(false), SerializeField] Accessory accessory2;
    [ReadOnly(false), SerializeField] Accessory pet1;
    [ReadOnly(false), SerializeField] Accessory pet2;
    Player player;

    public void ShowEquipedItems()
    {
        player = gameObject.GetComponent<Player>();
        foreach (var item in userData.EquipedItems)
        {
            if (item.itemPreb.GO == null)
            {
                continue;
            }
            var obj = Instantiate<GameObject>(item.itemPreb.GO, gameObject.transform,false);
            switch (item.Name)
            {
                case "Weapon":
                    weapon = obj.GetComponent<Weapon>();
                    item.drivedClass = obj.GetComponent<Weapon>();
                    break;
                case "Armor":
                    armor = obj.GetComponent<Armor>();
                    item.drivedClass = obj.GetComponent<Armor>();
                    break;
                case "Ring1":
                    accessory1 = obj.GetComponent<Accessory>();
                    item.drivedClass = obj.GetComponent<Accessory>();
                    break;
                case "Ring2":
                    accessory2 = obj.GetComponent<Accessory>();
                    item.drivedClass = obj.GetComponent<Accessory>();
                    break;
                ///////// 펫 시스템이 아직 없어서 Accessory를 넣어놓음
                case "Pet1":
                    pet1 = obj.GetComponent<Accessory>();
                    item.drivedClass = obj.GetComponent<Accessory>();
                    break;
                case "Pet2":
                    pet2 = obj.GetComponent<Accessory>();
                    item.drivedClass = obj.GetComponent<Accessory>();
                    break;
            }
        }
        items = new List<IItem>(6) { weapon, armor, accessory1, accessory2, pet1, pet2};
    }
}
