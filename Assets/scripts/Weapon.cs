﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using DATA;
using Item;

[System.Serializable]
public class Weapon : MonoBehaviour, IItem
{
    public string Name { get => weaponName;}
    [ReadOnly(false), SerializeField] string weaponName;
    public string Explanation { get; set; } = "";
    public bool IsEquiped { get; set; } = false;
    public Player InGamePlayer { get; set; }
    public enum TYPE
    {
        SLING = 0, BOW = 1, BOOMERANG = 2,
    }
    [SerializeField] Weapon.TYPE weaponType;
    [ReadOnly(false), SerializeField] BulletBuilder bulletBuilder;
    public DATA.Condition Condition { get => condition; set => condition = value; }
    ItemType IItem.ItemType { get => DATA.ItemType.WEAPON; }

    public ItemPrebs Prebs => prebs;
    [SerializeField] ItemPrebs prebs;
    [SerializeField] DATA.Condition condition = null;
    private void Awake()
    {
        SetUpName();
        InGamePlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        InGamePlayer.SetWeaponTypeAnimation(weaponType);
        InGamePlayer.ShowWeaponOnPlayerHand(weaponType, Prebs.InGamePreb);
        SetUpBulletBuilder(Condition);
    }
    private void SetUpName()
    {
        switch (weaponType)
        {
            case TYPE.SLING:
                weaponName = "Sling";
                break;
            case TYPE.BOW:
                weaponName = "Bow";
                break;
            case TYPE.BOOMERANG:
                weaponName = "Boomerang";
                break;
            default:
                break;
        }
    }
    void SetUpBulletBuilder(Condition condition)
    {
        bulletBuilder = GetComponent<BulletBuilder>();
        bulletBuilder.Condition = condition;
        switch (weaponType)
        {
            case TYPE.SLING:
                bulletBuilder.CallFly = FlySling;
                break;
            case TYPE.BOW:
                bulletBuilder.CallFly = FlyArrow;
                break;
            case TYPE.BOOMERANG:
                bulletBuilder.CallFly = FlyBoomerang;
                break;
            default:
                break;
        }
    }
    public void Shoot()
    {
        bulletBuilder.Shoot();
    }
    void FlySling(GameObject obj)
    {
        var script = obj.GetComponent<Bullet>();
        obj.transform.Translate(script.startingForward * script.moveSpeed, Space.World);
        script.startingForward = transform.forward;

        print("Fly Sling");
    }

    void FlyArrow(GameObject obj)
    {
        var script = obj.GetComponent<Bullet>();
        script.rb.MovePosition(obj.transform.position + script.startingForward * script.moveSpeed * Time.deltaTime * 100);
        //obj.transform.Translate(script.startingForward * script.moveSpeed, Space.World);
        print("FlyArrow");
    }
    void FlyBoomerang(GameObject obj)
    {
        var script = obj.GetComponent<Bullet>();
        obj.transform.Translate(script.startingForward * script.moveSpeed*0.1f, Space.World);
        print("FlyBoomerang");
    }
    
    public void PutInto(DATA.Attack attackData)
    {

    }
    public void Introduce()
    {
        throw new System.NotImplementedException();
    }

    public void Equip()
    {
        throw new System.NotImplementedException();
    }
    public void Unequip()
    {
        throw new System.NotImplementedException();
    }
    public void LevelUp()
    {
        throw new System.NotImplementedException();
    }

    public void Buff(ref Status playerStatus)
    {
        throw new NotImplementedException();
    }
}
