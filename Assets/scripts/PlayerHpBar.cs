﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHpBar : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Image hpBar;
    [SerializeField] TextMeshProUGUI text;
    Player player;
    DATA.Status status;
    float maxHP = 0.0f;
    void Start()
    {
    }

    private void OnEnable()
    {
        EventSystem.EventManager.AddListener(EventSystem.EVENT_TYPE.SEARCHPLAYER, SetActiveHpBar);
    }
    private void OnDisable()
    {
        EventSystem.EventManager.RemoveListener(EventSystem.EVENT_TYPE.SEARCHPLAYER, SetActiveHpBar);
    }

    void SetActiveHpBar(EventSystem.EventHandler e)
    {
        var evt = e as EventSystem.SearchPlayerEvent;
        player = evt.player;
        status = player.Status;
        maxHP = status.HealthPoint;

        hpBar.fillAmount = status.HealthPoint / maxHP;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player != null)
        {
            int hp = status.HealthPoint;
            hpBar.fillAmount = hp / maxHP;
            text.text = hp.ToString();
        }
    }
}
