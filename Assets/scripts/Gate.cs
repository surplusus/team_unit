﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public bool IsOpen;
    MapGate gate;
    TextMesh StageNumText;
    RoleScheduler roleScheduler;
    BoxCollider upCollider;
    void Start()
    {
        gate = this.GetComponentInChildren<MapGate>();
        StageNumText = this.GetComponentInChildren<TextMesh>();
        roleScheduler = GameObject.FindGameObjectWithTag("RoleScheduler").GetComponent<RoleScheduler>();

        upCollider = this.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<BoxCollider>();
        if (StageNumText != null)
            StageNumText.text = roleScheduler.StageNum.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (roleScheduler.MonsterNum <= 0)
        {
            if(!IsOpen)
                PoolOfFoodController.StageEnd();

            upCollider.isTrigger = true;
            IsOpen = true;
            gate.GateOpen(IsOpen);
        }
    }
}