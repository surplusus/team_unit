﻿using DATA;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAnimEventController : MonoBehaviour
{
    static UnityEvent OnShootAnimEvent;
    static public System.Action OnDieAnimEvent;
    static UnityEvent OnRunAnimEvent;
    static UnityEvent OnDeadAnimEvent;
    static Player player;
    static SoundManager soundManager;
    static ShowInventory inventory;
    
    private void Awake()
    {
        OnShootAnimEvent = new UnityEvent();
        inventory = gameObject.GetComponentInChildren<ShowInventory>() ?? gameObject.GetComponent<ShowInventory>();
        player = gameObject.GetComponentInChildren<Player>() ?? gameObject.GetComponent<Player>();
        soundManager = gameObject.GetComponentInChildren<SoundManager>() ?? gameObject.GetComponent<SoundManager>();
        OnShootAnimEvent?.AddListener(ShootWhenAttackEndAnimPlayed);
    }
    private void OnDestroy()
    {
        player = null;
        soundManager = null;
        inventory = null;
        OnShootAnimEvent = null;
    }
    public void OnShootEventOperate()
    {
        OnShootAnimEvent?.Invoke();
    }
    public void OnDieEventOperate()
    {
        OnDieAnimEvent?.Invoke();
    }
    void ShootWhenAttackEndAnimPlayed()
    {
        var w = inventory.items[(int)ItemType.WEAPON] as Weapon;
        w?.Shoot();
    }
    public void MakeSoundWhenAttackEndAnimPlayed(string weaponName)
    {
        soundManager.SoundPlay(weaponName);
    }
    public void MakeSoundWhenRunAnimPlayed(bool isOn)
    {
        if (isOn)
            soundManager.SoundPlay("Run");
    }
    public void MakeSoundWhenDeadAnimPlayed()
    {
        soundManager.SoundPlay("Dead");
    }
}
