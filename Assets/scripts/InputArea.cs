﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputArea : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    RectTransform Inputfield;
    RawImage InputAreaRawImage;
    Vector2 pointOfScreenOut = new Vector2(-50f, 0f);
    [SerializeField] RectTransform TouchPoint;
    [SerializeField] RectTransform DeltaPoint;
    [SerializeField] Vector2 delta;

    private void Start()
    {
        Inputfield = GetComponent<RectTransform>();
        InputAreaRawImage = GetComponent<RawImage>();
        GameManager.Me.CheatEvent += ToggleRawImageAlpha;

    }
    void OnEnable()
    {
        OnPointerUp(null);
        InputManager.ResetDelta();
    }
    private void Update()
    {
        delta = InputManager.DeltaPos;
    }
    void TouchPointMove(PointerEventData eventData, string state)
    {
        switch (state)
        {
            case "Down":
                //TouchPoint.anchorMin = new Vector2(0.5f, 0.5f);
                //TouchPoint.anchorMax = new Vector2(0.5f, 0.5f);
                TouchPoint.anchoredPosition = InputManager.FirstTouchPos;
                break;
            case "Drag":
                break;
            case "Up":
                TouchPoint.anchorMin = new Vector2(0f, 0f);
                TouchPoint.anchorMax = new Vector2(0f, 0f);
                TouchPoint.anchoredPosition = pointOfScreenOut;
                break;
            default:
                print("잘못된 state");
                break;
        }
    }
    void DeltaPointMove(PointerEventData eventData, string state)
    {
        switch (state)
        {
            case "Down":
                break;
            case "Drag":
                DeltaPoint.anchoredPosition = InputManager.DeltaPos;
                break;
            case "Up":
                DeltaPoint.anchoredPosition = Vector2.zero;
                break;
            default:
                print("잘못된 state");
                break;
        }
    }
    public void OnDrag(PointerEventData eventData)
    {
        InputManager.OnDrag(eventData);
        TouchPointMove(eventData, "Drag");
        DeltaPointMove(eventData, "Drag");
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        InputManager.OnPointerDown(eventData);
        TouchPointMove(eventData, "Down");
        DeltaPointMove(eventData, "Down");
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        InputManager.OnPointerUp(eventData);
        TouchPointMove(eventData, "Up");
        DeltaPointMove(eventData, "Up");
    }
    public void ToggleRawImageAlpha()
    {
        if (InputAreaRawImage.color.a > 0.1f)
            InputAreaRawImage.color = new Color(1,1,1,0);
        else
            InputAreaRawImage.color = new Color(1, 1, 1, 0.4f);
    }
}
