﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_AttackBoost : IAbility
{
    private DATA.AbilityType typeName;
    public DATA.AbilityType TypeName { get => typeName; }
    public S_AttackBoost()
    {
        typeName = DATA.AbilityType.S_AttackBoost;
    }
    public GameObject prefab;
}
