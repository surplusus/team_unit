﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DATA;
using UnityEngine.Events;
using EventSystem;

public class BulletBuilder : MonoBehaviour
{
    public DATA.Condition Condition { get => originCondition; set => originCondition = value; }
    [SerializeField] DATA.Condition originCondition;
    public DATA.Attack AttackData { get=> originAttackData; set => originAttackData = value; }
    [SerializeField] DATA.Attack originAttackData;
    [SerializeField] GameObject bulletPreb;
    [ReadOnly(false), SerializeField] Player player = null;
    [ReadOnly(false), SerializeField] GameObject monster = null;
    public UnityAction<GameObject> CallFly;

    private void Awake()
    {
        WhoAmI();
    }
    void WhoAmI()
    {   // BulletBuilder를 누가 소유하고있는지 확인(player or monster)
        var pObj = transform.parent.tag.Equals("Player");
        var mObj = transform.parent.tag.Equals("Enemy");
        if (pObj)
        {
            player = transform.parent.GetComponent<Player>();
        }
        else if (mObj)
        {
            monster = transform.parent.gameObject;
        }
    }
    private void InitiateAttackDataValue(Attack attackData)
    {
        attackData.Strangth = 10;
        attackData.CriticalRate = 30; // 퍼센트
        attackData.CriticalDamage = 1.2f;
        attackData.DamagePerSecond = 10;
    }
    public void Shoot()
    {   // Weapon과 Monster에서 불린다
        //if (player != null)
        //{
        //    var bulletPoint = player.BulletPoint.position;
        //    var playerRot = player.transform.rotation;
        //    Clone(bulletPoint, playerRot);
        //}

        if (originCondition.IsForwardArrow) // 전방
        {
            Vector3 pos = player.BulletPoint.position;
            pos -= transform.right * 0.5f;
            Clone(bulletPreb, pos, transform.rotation);
            pos += transform.right;
            Clone(bulletPreb, pos, transform.rotation);
        }
        else
        {
            Clone(bulletPreb, player.BulletPoint.position, transform.rotation);
        }

        if(originCondition.IsRearArrow) // 후방
        {
            Clone(bulletPreb, player.BulletPoint.position, transform.rotation * Quaternion.AngleAxis(180,Vector3.up));
        }
    }
    public void Shoot(Quaternion rot)
    {
        Clone(bulletPreb, transform.position, rot);
    }
    public void Clone(Vector3 pos, Quaternion rot)
    {
        var obj = GameObject.Instantiate(bulletPreb, pos, rot);
        var script = obj.GetComponent<Bullet>();
        if (this.CallFly == null)
            script.CallFly = DefaultFly;
        else
            script.CallFly = this.CallFly;

        if (player != null)
            script.type = Bullet.TYPE.PLAYER;
        if (monster != null)
            script.type = Bullet.TYPE.ENEMY;

        script.AttackData = new DATA.Attack(originAttackData);
        script.Condition = originCondition;
    }
    void DefaultFly(GameObject obj)
    {       
        var script = obj.GetComponent<Bullet>();
        script.rb.MovePosition(script.transform.position + script.transform.forward * script.moveSpeed * 0.01f);
    }
    public void Clone(GameObject bullet, Vector3 pos, Quaternion rot)
    {
        
        var obj = GameObject.Instantiate(bullet,pos,rot);
        var script = obj.GetComponent<Bullet>();

        if (player != null)
            script.type = Bullet.TYPE.PLAYER;
        if (monster != null)
            script.type = Bullet.TYPE.ENEMY;

        script.AttackData = new DATA.Attack(originAttackData);
        script.Condition = originCondition;
        //if (this.CallFly == null)
        //    //script.CallFly = delegate(GameObject o)
        //    //{
        //    //    var sc = o.GetComponent<Bullet>();
        //    //    o.transform.Translate(sc.startingForward * sc.moveSpeed, Space.World);
        //    //    script.startingForward = transform.forward;
        //    //};
        //    script.CallFly = DefaultFly;
        //else
        //    script.CallFly = this.CallFly;
    }
}
