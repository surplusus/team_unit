﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using EventSystem;

public class SearchForPlayer : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera followCam;
    private void OnEnable()
    {
        EventManager.AddListener(EVENT_TYPE.SEARCHPLAYER, TrySearchForPlayer);
    }
    private void OnDestroy()
    {
        EventManager.RemoveListener(EVENT_TYPE.SEARCHPLAYER, TrySearchForPlayer);
    }
    void TrySearchForPlayer(EventHandler e)
    {
        var evt = e as SearchPlayerEvent;
        followCam.Follow = evt.player.transform;
    }
}
