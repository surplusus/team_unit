﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bullet : MonoBehaviour
{
    public enum TYPE
    {
        PLAYER = 0,
        ENEMY
    }

    public DATA.Attack AttackData { private get; set; }
    public DATA.Condition Condition { private get; set; }
    [SerializeField] SoundManager soundManager;
    // Event Handler 
    public UnityAction<GameObject> CallFly;

    [HideInInspector] public float moveSpeed = 1.0f;
    [HideInInspector] public int ricochetCount = 0;
    [HideInInspector] public int reflectionCount = 0;
    [HideInInspector] public int BoomerangeCount = 0;
    [HideInInspector] public float pushingForce = 0;
    [HideInInspector] public Vector3 startingPos;
    [HideInInspector] public Quaternion startingRot;
    [HideInInspector] public Vector3 startingForward;

    public Rigidbody rb;
    public TYPE type;
    public bool IsDead {get;set;} = false;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        soundManager = GetComponent<SoundManager>();
        startingPos = transform.position;
        startingRot = transform.rotation;
        startingForward = transform.forward;
    }
    private void Start()
    {
        rb.AddForce(this.transform.forward * 10.0f,ForceMode.VelocityChange);
    }
    private void Update()
    {
        if (IsDead)
            Destroy(gameObject);
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
    public float Damage()
    {
        float result = 0f;
        result += AttackData.Strangth;
        if (Random.Range(0.0f, 1.0f) < AttackData.CriticalRate)
            result *= AttackData.CriticalDamage;
        //AttackData.DamagePerSecond // 이건 뭐였지?
        return result;
    }
    public void Fly()
    {
        CallFly?.Invoke(gameObject);
    }
    IEnumerator DelayDestroyAfterHit()
    {
        Destroy(gameObject.GetComponent<SphereCollider>());
        var children = new List<Transform>(transform.GetComponentsInChildren<Transform>());
        children.Remove(transform);
        children.ForEach(x => x.gameObject.SetActive(false));

        if (type.Equals(TYPE.ENEMY))
        {
            IsDead = true;
        }
        else if (type.Equals(TYPE.PLAYER))
        {
            yield return new WaitForSecondsRealtime(0.5f);
            IsDead = true;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (type.Equals(TYPE.PLAYER))
        {
            if (collision.gameObject.tag.Contains("Wall"))
            {
                soundManager.SoundPlay("WallHit");
                if (Condition.IsWallReflection)
                    if (reflectionCount < 2)
                    {
                        reflectionCount++;
                        return;
                    }

                StartCoroutine(DelayDestroyAfterHit());
            }
            else if (collision.gameObject.tag.Equals("Enemy"))
            {
                gameObject.GetComponent<SphereCollider>().isTrigger = true;
                collision.gameObject.GetComponent<IMonster>().Hit(Damage());
                StartCoroutine(DelayDestroyAfterHit());
            }
        }
        else if (type == TYPE.ENEMY)
        {
            if (collision.gameObject.tag.Equals("Player"))
            {
                collision.gameObject.GetComponent<Player>().Hit(Damage());
                //IsDead = true;
            }
            else if (collision.gameObject.tag.Equals("Enemy"))
                return;
            IsDead = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        gameObject.GetComponent<SphereCollider>().isTrigger = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (type.Equals(TYPE.PLAYER))
        {
            if (other.tag.Contains("Wall"))
            {
                StartCoroutine(DelayDestroyAfterHit());
            }
            else if (other.tag.Equals("Enemy"))
            {
                //Debug.Log(other.gameObject.name + "  : " + Damage().ToString());
                other.gameObject.GetComponent<IMonster>().Hit(Damage());
                StartCoroutine(DelayDestroyAfterHit());
            }
        }
        else if (type.Equals(TYPE.ENEMY))
        {
            if (other.tag.Equals("Player"))
            {
                other.gameObject.GetComponent<Player>().Hit(Damage());
                IsDead = true;
            }
            if (other.tag.Contains("Wall"))
            {
                IsDead = true;
            }
        }
    }
}
