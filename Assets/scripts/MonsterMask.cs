﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMask : MonoBehaviour
{
    // Start is called before the first frame update
    IMonster monster;
    OpticalMeleeMonster opticalMeleeMonster;
    CapsuleCollider capsuleCollider;
    public GameObject rotate;
    void Start()
    {
        capsuleCollider = GetComponent<CapsuleCollider>();

        opticalMeleeMonster = rotate.GetComponent<OpticalMeleeMonster>();
        monster = opticalMeleeMonster;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = rotate.transform.position;

        if(opticalMeleeMonster.IsAttack)
        {
            if(capsuleCollider.enabled)
                capsuleCollider.isTrigger = false;
        }
        else
        {
            if (!capsuleCollider.enabled)
                capsuleCollider.isTrigger = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if(other.tag.Equals("Player"))
        {
            other.GetComponent<Player>().Hit(monster.Damage);
        }
    }
}
