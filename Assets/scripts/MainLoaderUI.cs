﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainLoaderUI : MonoBehaviour
{
    [SerializeField] bool IsDebug = true;

    [SerializeField] Image progressBar;
    [SerializeField] Text text;

    AsyncOperation async_operation;
    void Start()
    {
        IsDebug = UIManager.Me.IsDebug;
        StartCoroutine(LoadingNextScene("1LobbyScene"));
    }
    IEnumerator LoadingNextScene(string sceneName)
    {
        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();

        async_operation = SceneManager.LoadSceneAsync(sceneName);
        async_operation.allowSceneActivation = false;
        while (async_operation.progress >= 0.9f)
        {
            // async_operation.isDone
            yield return true;
        }
        //async_operation.allowSceneActivation = true;
    }
    void Update()
    {
        float DebugEndTime = 1.0f;
        Loading(IsDebug, DebugEndTime);
    }
    float debugTime = 0.0f;
    void Loading(bool isDebug, float overTime)
    {
        if (isDebug)
        {
            debugTime += Time.deltaTime;

            float progress = debugTime / overTime;
            progressBar.fillAmount = progress;
            int per = (int)(progress * 100);
            //Debug.Log("progress :" + progress);
            //Debug.Log("per :" + per);

            text.text = per.ToString() + "%";

            if (debugTime >= overTime)
            {
                async_operation.allowSceneActivation = true;
            }
        }
        else
        {
            float progress = async_operation.progress;
            progressBar.fillAmount = progress;
            int per = (int)progress * 100;
            text.text = string.Format($"{per} %");

            if (async_operation.progress >= 0.9f)
            {
                async_operation.allowSceneActivation = true;
                progressBar.fillAmount = 1;
                text.text = "100%";
            }
        }
    }
}
