﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using UnityEngine.AI;
public class OpticalRangeMonster : MonoBehaviour, IMonster
{
    NavMeshAgent navMeshAgent;
    public GameObject player;
    SkinnedMeshRenderer skinnedMeshRenderer;

    float idleTime = 0.0f;
    float hitTime = 0.0f;
    float attackTime = 0.0f;
    float deadTime = 0.0f;
    float turnDelay = 0.0f;
    float collisionTime = 1.0f;

    public bool IsDie = false;
    public bool IsHit = false;
    public bool IsAttack = false;
    public bool IsShoot = false;
    public bool IsMove = false;
    Color colorOri;

    private int hp;
    private int exp;
    private float damage;
    private float moveSpeed;
    public float range;
    BulletBuilder bulletBuilder;
    [SerializeField] GameObject bullet;
    Animator animator;

    void Awake()
    {
        navMeshAgent = this.gameObject.GetComponent<NavMeshAgent>();
        bulletBuilder = this.gameObject.GetComponent<BulletBuilder>();
        animator = this.gameObject.GetComponentInChildren<Animator>();
        skinnedMeshRenderer = this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        hp = 350;
        exp = 7;
        damage = 22.0f;
        moveSpeed = 2;
        range = 10;
        colorOri = this.gameObject.transform.GetComponentInChildren<SkinnedMeshRenderer>().material.color;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (collisionTime < 1.0f)
            collisionTime += Time.deltaTime;

        animator.SetFloat("Move", navMeshAgent.velocity.magnitude);
    }


    int IMonster.HP
    {
        get { return hp; }
        set { hp = value; }
    }

    int IMonster.EXP
    {
        get { return exp; }
        set { exp = value; }
    }
    float IMonster.Damage
    {
        get { return damage; }
        set { damage = value; }
    }
    float IMonster.MoveSpeed
    {
        get { return moveSpeed; }
        set { moveSpeed = value; }
    }
    public void Idle()
    {
        if(IsDie)
        {
            Die();
        }
        else if (player != null)
        {
            if (Vector3.Distance(player.transform.position, this.transform.position) < range)
            {
                navMeshAgent.destination = this.transform.position;

                if (idleTime > 3.0f)
                {
                    IsAttack = true;
                    IsShoot = true;
                    animator.SetBool("IsShoot", IsShoot);
                    idleTime = 0.0f;
                }
                else
                {
                    idleTime += Time.deltaTime;
                }
            }
        }
        else
        {
            if(!IsMove)
                IsMove = true;
        }
    }
    public void Move()
    {
        if(player != null)
            navMeshAgent.destination = player.transform.position;
        IsMove = false;
    }
    public void Attack()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("AttackEnd"))
        {
            if (IsShoot)
                Shoot();

            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
            {
                IsAttack = false;
            }
        }
    }
    public void Die()
    {
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.MONSTERCOUNT, new EventSystem.MonsterCountEvent());
        animator.SetTrigger("Die");
        IsDie = true;
        gameObject.layer = LayerMask.NameToLayer("Die");
        StartCoroutine("FallDie");
    }
    IEnumerator FallDie()
    {
        int heart = Random.Range(0, 2);
        PoolOfFoodController.ShowFood(this.transform.position, PoolOfFoodController.TYPE.COIN, exp, exp.ToString());
        PoolOfFoodController.ShowFood(this.transform.position, PoolOfFoodController.TYPE.HEART, heart, heart.ToString());

        float t = 0.0f;
        float angle = 0.0f;
        while (t < 2.0f)
        {
            Vector3 pos = transform.position;
            angle += Time.deltaTime * 30;
            transform.Rotate(new Vector3(angle, 0, 0));
            //Debug.Log("Rotation : " + transform.rotation);
            pos.y -= Time.deltaTime;
            transform.position = pos;

            t += Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }

        Destroy(this.transform.parent.gameObject);
    }

    public void Hit(float damage)
    {
        hp -= (int)damage;
        Vector3 pos = transform.position;
        pos.x += Random.Range(-100, 100) * 0.01f;
        pos.y += Random.Range(-100, 100) * 0.01f;
        pos.z += Random.Range(-100, 100) * 0.01f;
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.HIT,
            new EventSystem.HitEvent((int)damage, pos));

        if (hp <= 0)
        {
            if(!IsDie)
                Die();
        }
        else
        {
            if (!IsAttack)
                animator.SetTrigger("Hit");
        }
        StartCoroutine("HitFlash");
    }

    IEnumerator HitFlash()
    {
        float c = 0.8f;
        while (hitTime < 0.2f)
        {
            skinnedMeshRenderer.material.SetColor("_Emission", new Color(c, c, c, 1));
            yield return new WaitForSeconds(0.01f);
            hitTime += 0.01f;
            c -= 0.03f;
        }

        c = 0;
        skinnedMeshRenderer.material.SetColor("_Emission", new Color(c, c, c, 1));
        hitTime = 0.0f;
    }

    void Shoot()
    {

        this.transform.LookAt(player.transform);

        Quaternion rot1 = this.transform.rotation;
        Quaternion rot2 = this.transform.rotation;
        Quaternion rot3 = this.transform.rotation;
        rot2 *= Quaternion.Euler(0.0f, -45.0f, 0.0f);
        rot3 *= Quaternion.Euler(0.0f, +45.0f, 0.0f);

        bulletBuilder.Clone(bullet, this.transform.position, rot1);
        bulletBuilder.Clone(bullet, this.transform.position, rot2);
        bulletBuilder.Clone(bullet, this.transform.position, rot3);

        attackTime = 0.0f;
        IsShoot = false;
        animator.SetBool("IsShoot", IsShoot);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (collisionTime >= 1.0f)
            {
                collision.gameObject.GetComponent<Player>().Hit(damage);
                collisionTime = 0.0f;
            }
        }
    }
}
