﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;

public class OpticalMeleeMonster : MonoBehaviour, IMonster
{
    public GameObject player;
    new Rigidbody rigidbody;
    Animator animator;
    SkinnedMeshRenderer skinnedMeshRenderer;
    CapsuleCollider capsuleCollider;


    public bool IsDie = false;
    public bool IsHit = false;
    public bool IsAttack = false;
    public bool IsMove = false;

    private int hp;
    private int exp;
    private float damage;
    private float moveSpeed;
    float hitTime = 0.0f;
    float idleTime = 0.0f;
    float chargeTime = 0.0f;
    float collisionTime = 1.0f;

    Color colorOri;
    // Start is called before the first frame update
    void Start()
    {
        hp = 150;
        exp = 6;
        damage = 44.0f;
        moveSpeed = 0.01f;
        IsMove = true;
        colorOri = this.gameObject.transform.GetComponentInChildren<SkinnedMeshRenderer>().material.color;
        player = GameObject.FindGameObjectWithTag("Player");
        rigidbody = gameObject.GetComponent<Rigidbody>();
        skinnedMeshRenderer = this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        animator = GetComponentInChildren<Animator>();
        capsuleCollider = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (collisionTime < 1.0f)
            collisionTime += Time.deltaTime;
    }

    int IMonster.HP
    {
        get { return hp; }
        set { hp = value; }
    }

    int IMonster.EXP
    {
        get { return exp; }
        set { exp = value; }
    }

    float IMonster.Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    float IMonster.MoveSpeed
    {
        get { return moveSpeed; }
        set { moveSpeed = value; }
    }
    public void Idle()
    {
        if (idleTime > 3.0f)
        {
            IsAttack = true;
            capsuleCollider.isTrigger = true;
            IsMove = false;
            idleTime = 0.0f;
        }
        else
        {
            if (!IsAttack)
            {
                IsMove = true;
            }
            idleTime += Time.deltaTime;
        }
    }
    public void Move()
    {
        if (player == null) return;

        if (Vector3.Distance(this.transform.position, player.transform.position) > 1.0f)
        {
            Transform look = player.transform;
            Vector3 lookPos = look.position;
            lookPos.y = this.transform.position.y;
            look.position = lookPos;
            this.transform.LookAt(look);
        }
        rigidbody.MovePosition(this.transform.position + this.transform.forward * moveSpeed);
        IsMove = false;
    }
    public void Attack()
    {
        if (chargeTime < 1.0f)
        {
            Charge();
            chargeTime += Time.deltaTime;
        }
        else
        {
            IsAttack = false;
            capsuleCollider.isTrigger = false;
            chargeTime = 0.0f;
        }
    }
    public void Die()
    {
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.MONSTERCOUNT, new EventSystem.MonsterCountEvent());
        animator.SetTrigger("Die");
        IsDie = true;
        gameObject.layer = LayerMask.NameToLayer("Die");

        StartCoroutine("FallDie");
    }
    IEnumerator FallDie()
    {
        PoolOfFoodController.ShowFood(this.transform.position, PoolOfFoodController.TYPE.COIN, exp, exp.ToString());

        float t = 0.0f;

        while (t < 2.0f)
        {
            Vector3 pos = transform.position;
            pos.y -= Time.deltaTime;
            transform.position = pos;

            t += Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }

        Destroy(this.transform.parent.gameObject);
    }

    public void Hit(float damage)
    {
        hp -= (int)damage;
        Vector3 pos = transform.position;
        pos.x += Random.Range(-100, 100) * 0.01f;
        pos.y += Random.Range(-100, 100) * 0.01f;
        pos.z += Random.Range(-100, 100) * 0.01f;
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.HIT,
            new EventSystem.HitEvent((int)damage, pos));

        if (hp <= 0)
        {
            if (!IsDie)
                Die();
        }
        else
        { 
            if (!IsAttack)
                animator.SetTrigger("Hit");
        }
        StartCoroutine("HitFlash");
    }

    IEnumerator HitFlash()
    {
        float c = 0.8f;
        while (hitTime < 0.2f)
        {
            skinnedMeshRenderer.material.SetColor("_Emission", new Color(c, c, c, 1));
            yield return new WaitForSeconds(0.01f);
            hitTime += 0.01f;
            c -= 0.03f;
        }

        c = 0;
        skinnedMeshRenderer.material.SetColor("_Emission", new Color(c, c, c, 1));
        hitTime = 0.0f;
    }
    void Charge()
    {
        rigidbody.MovePosition(this.transform.position + this.transform.forward * moveSpeed * 20);
        IsMove = false;
    }
    private void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject.tag.Equals("Player"))
        {
            if (collisionTime >= 1.0f)
            {
                collision.gameObject.GetComponent<Player>().Hit(damage);
                collisionTime = 0.0f;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Contains("Wall_"))
        {
            IsAttack = false;
            capsuleCollider.isTrigger = false;
            chargeTime = 0.0f;
        }
        else if(other.tag.Equals("Player"))
        {
            other.gameObject.GetComponent<Player>().Hit(damage);
        }
    }
}
