﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AnimEvent
{
    [System.Serializable]
    public class AnimEventListener : UnityEvent<string, int>
    {
    }
    [System.Serializable]
    public class AnimTimer
    {
        public string ClipName;
        public int Frame;
        public AudioClip AudioClip;
    }
}
public class AnimEventSetter : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] Animation animation;
    [SerializeField] List<AnimEvent.AnimTimer> animTimer;
    AnimationEvent animationEvent;
    AnimationClip[] animationClips = null;
    AudioSource audioSource = null;
    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        if (audioSource == null)
            audioSource = gameObject.AddComponent<AudioSource>();
        for (int i = 0; i < animTimer.Count; ++i)
        {
            SetUpEvent(animTimer[i].ClipName, animTimer[i].Frame, i);
        }
    }
    public void SetUpEvent(string sName, int nFrame, int index)
    {
        if (animator != null)
            animationClips = GetAllClips(animator);
        else if (animation != null)
            animationClips = GetAllClips(animation);
        AnimationClip clip = SearchAnimClip(sName);
        AddAnimationEvent(clip, nFrame, index);
    }
    void AddAnimationEvent(AnimationClip clip, int frame, int index)
    {
        AnimationEvent evt = new AnimationEvent();
        evt.functionName = "DefaultAnimEventFunc";
        evt.intParameter = index;
        evt.time = frame / 30f;
        clip.AddEvent(evt);
    }
    void DefaultAnimEventFunc(int index)
    {
        var clip = animTimer[index].AudioClip;
        if (clip != null)
        {
            audioSource.PlayOneShot(clip);
        }
        else
        {
            print("AnimTimer에 AudioClip이 없습니다");
            return;
        }
    }
    AnimationClip SearchAnimClip(string name)
    {
        List<AnimationClip> list = new List<AnimationClip>();
        list.AddRange(animationClips);
        var clip = list.Find(x => (x.name == name));
        return clip;
    }
    AnimationClip[] GetAllClips(Animator anim)
    {
        AnimationClip[] result = null;
        result = anim.runtimeAnimatorController.animationClips;
        return result;
    }
    AnimationClip[] GetAllClips(Animation anim)
    {
        List<AnimationClip> result = new List<AnimationClip>();
        foreach (AnimationState state in anim)
        {
            result.Add(state.clip);
        }
        return result.ToArray();
    }
}
