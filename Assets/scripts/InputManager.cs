﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;
using Cinemachine;
using UnityEngine.AddressableAssets;

[System.Serializable]
public class InputManager
{
    #region 매니저라면 응당있어야 할 것들
    private static InputManager inst = null;
    public static InputManager Instantiate()
    {
        inst = new InputManager();
        return inst;
    }
    public static void Release()
    {
        inst.InputArea.ReleaseAsset();
        inst.InputArea = null;
        inst = null;
    }
    #endregion

    static public Vector2 FirstTouchPos = Vector2.zero;
    static public Vector2 DeltaPos = Vector2.zero;
    static public float LIMITDELTAPOSVALUE = 35f;

    [ReadOnly(false), SerializeField] GameObject inputArea;
    [ReadOnly(false), SerializeField] InputArea dragArea;
    public AssetReference InputArea;
    static public CinemachineVirtualCamera virtualCamera = null;
    static public void CreateInputArea()
    {
        if (inst == null)
            Instantiate();
        if (inst.inputArea == null)
        {
            var obj = GameObject.Find("InputArea");
            if (obj == null)
            {
                Addressables.InstantiateAsync((object)"InputArea").Completed += ob =>
                {
                    obj = ob.Result;
                    inst.inputArea = obj;
                    inst.dragArea = obj.GetComponentInChildren<InputArea>();
                };
                return;
            }
            inst.inputArea = obj;
            inst.dragArea = obj.GetComponentInChildren<InputArea>();
        }
    }
    #region Keys
    List<KeyCode> KeyUP = new List<KeyCode> { KeyCode.W, KeyCode.UpArrow, };
    List<KeyCode> KeyDOWN = new List<KeyCode> { KeyCode.S,KeyCode.DownArrow, };
    List<KeyCode> KeyLEFT = new List<KeyCode> { KeyCode.A, KeyCode.LeftArrow, };
    List<KeyCode> KeyRIGHT = new List<KeyCode> { KeyCode.D, KeyCode.RightArrow, };
    List<KeyCode> KeyQUIT = new List<KeyCode> { KeyCode.Escape, };
    #endregion
    private bool IsButtonPushed(string keyName)
    {
        List<KeyCode> keys = null;
        if (keyName == "UP") keys = KeyUP;
        else if (keyName == "DOWN") keys = KeyDOWN;
        else if (keyName == "LEFT") keys = KeyLEFT;
        else if (keyName == "RIGHT") keys = KeyRIGHT;
        else if (keyName == "QUIT") keys = KeyQUIT;

        for (int i = 0; i < keys.Count; ++i)
            if (Input.GetKeyDown(keys[i]))
                return true;

        return false;
    }
    public static bool IsUpPushed() { return inst.IsButtonPushed("UP"); }
    public static bool IsDownPushed() { return inst.IsButtonPushed("DOWN"); }
    public static bool IsLeftPushed() { return inst.IsButtonPushed("LEFT"); }
    public static bool IsRightPushed() { return inst.IsButtonPushed("RIGHT"); }
    public static bool IsQuitPushed() { return inst.IsButtonPushed("QUIT"); }
    public static bool IsAnyPushed()
    {
        return IsUpPushed() || IsDownPushed() || IsLeftPushed() || IsRightPushed() || IsPushed;
    }
    static public bool IsPushed { get; private set; } = false;
    static public bool IsUp { get; private set; } = false;
    static public void OnDrag(PointerEventData eventData)
    {
        System.Func<Vector2, float, float, Vector2> Clamp = delegate (Vector2 v, float min, float max)
        {
            if (v.x < min) v.x = min;
            if (v.x > max) v.x = max;
            if (v.y < min) v.y = min;
            if (v.y > max) v.y = max;
            return v;
        };
        Vector2 vec = Vector2.zero;
        float CorrectiveValue = 10f;
        vec = eventData.delta * Time.deltaTime * CorrectiveValue;
        vec += InputManager.DeltaPos;
        float limit = InputManager.LIMITDELTAPOSVALUE;
        InputManager.DeltaPos = Clamp(vec, -limit, limit);
        InputManager.IsUp = false;
    }
    static public void OnPointerDown(PointerEventData eventData)
    {
        InputManager.DeltaPos = Vector2.zero;
        InputManager.FirstTouchPos = eventData.pressPosition;
        InputManager.IsPushed = true;
        InputManager.IsUp = false;
    }
    static public void OnPointerUp(PointerEventData eventData)
    {
        InputManager.DeltaPos = Vector2.zero;
        InputManager.FirstTouchPos = Vector2.zero;
        InputManager.IsPushed = false;
        InputManager.IsUp = true;
    }
    static public void ResetDelta()
    {
        InputManager.DeltaPos = Vector2.zero;
    }
}