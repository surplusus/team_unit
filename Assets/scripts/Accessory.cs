﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Item;
using DATA;

[System.Serializable]
public class Accessory :MonoBehaviour, IItem
{
    [ReadOnly(false), SerializeField] GameObject Player = null;
    public Accessory(Player player)
    {
        InGamePlayer = player;
    }
    public enum TYPE
    {
        RING = 0, NECKLACE = 1,
    }
    public TYPE Type { get; set; } = TYPE.RING;
    public string Name { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public string Explanation { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public bool IsEquiped { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public Player InGamePlayer { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    ItemType IItem.ItemType { get => DATA.ItemType.ACCESSORY; }

    public ItemPrebs Prebs => prebs;
    [SerializeField] ItemPrebs prebs;

    public void Equip()
    {
        throw new System.NotImplementedException();
    }

    public void Introduce()
    {
        throw new System.NotImplementedException();
    }

    public void LevelUp()
    {
        throw new System.NotImplementedException();
    }

    public void Unequip()
    {
        throw new System.NotImplementedException();
    }

    public void Buff(ref Status playerStatus)
    {
        
    }
}
