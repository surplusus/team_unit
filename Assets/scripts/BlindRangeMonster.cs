﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;

public class BlindRangeMonster : MonoBehaviour, IMonster
{
    Animator animator;
    SkinnedMeshRenderer skinnedMeshRenderer;

    public bool IsDie = false;
    public bool IsHit = false;
    public bool IsAttack = false;
    public bool IsMove = false;
    float idleTime = 0.0f;
    float hitTime = 0.0f;
    float deadTime = 0.0f;
    float dofTime = 0.0f;
    float collisionTime = 1.0f;

    private int hp;
    private int exp;
    private float damage;
    private float moveSpeed;

    BulletBuilder bulletBuilder;
    [SerializeField] GameObject bullet;

    Vector3 destination;
    Vector3 direction;
    void Awake()
    {
        bulletBuilder = this.gameObject.GetComponent<BulletBuilder>();
        animator = this.gameObject.GetComponentInChildren<Animator>();
        skinnedMeshRenderer = this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        hp = 250;
        exp = 5;
        damage = 33.0f;
        moveSpeed = 2;
        idleTime = Random.Range(1.5f, 2.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (collisionTime < 1.0f)
            collisionTime += Time.deltaTime;
    }


    int IMonster.HP
    {
        get { return hp; }
        set { hp = value; }
    }

    int IMonster.EXP
    {
        get { return exp; }
        set { exp = value; }
    }
    float IMonster.Damage
    {
        get { return damage; }
        set { damage = value; }
    }
    float IMonster.MoveSpeed
    {
        get { return moveSpeed; }
        set { moveSpeed = value; }
    }
    public void Idle()
    {
        if(idleTime > 3.0f)
        {
            do
            {
                destination = new Vector3(this.transform.position.x + Random.Range(-30, 30)* 0.1f, 0, this.transform.position.z + Random.Range(-30, 30) * 0.1f);
                Debug.Log(destination);
                Vector3 RayPos = destination;
                RaycastHit rayHit;
                RayPos.y = 100.0f;
                if (Physics.Raycast(RayPos, new Vector3(0, -1, 0), out rayHit))
                {
                    if (rayHit.collider.gameObject.name == "floor")
                    {
                        float d = Vector3.Distance(this.transform.position, destination);

                        moveSpeed = d / 2;
                        IsMove = true;
                        animator.SetTrigger("Move");
                        direction = destination - this.transform.position;
                    }
                }
            } while (!IsMove);

            idleTime = 0.0f;
        }
        else
        {
            idleTime += Time.deltaTime;
        }
    }
    public void Move()
    {
        if (dofTime > 2)
        {
            Vector3 pos = this.transform.position;
            pos.y = 0;
            this.transform.position = pos;

            this.transform.localScale = new Vector3(1, 1, 1);

            dofTime = 0.0f;
            IsMove = false;
            IsAttack = true;
        }
        else
        {
            if (dofTime > 0.8f)
            {
                this.transform.Translate(-this.transform.up * Time.deltaTime * 2.5f);
                Vector3 scale = this.transform.localScale;
                this.transform.localScale -= (scale * Time.deltaTime) / 2;
            }
            else if (dofTime > 0.5f)
            {
            }
            else
            {
                this.transform.Translate(this.transform.up * Time.deltaTime * 6);
                Vector3 scale = this.transform.localScale;
                this.transform.localScale += scale * Time.deltaTime;
            }

            this.transform.Translate(direction.normalized * moveSpeed * Time.deltaTime);
            dofTime += Time.deltaTime;
        }
    }
    public void Attack() 
    {
        Shoot();
    }
    public void Die()
    {
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.MONSTERCOUNT, new EventSystem.MonsterCountEvent());
        animator.SetTrigger("Die");
        IsDie = true;
        gameObject.layer = LayerMask.NameToLayer("Die");
        StartCoroutine("FallDie");
    }
    IEnumerator FallDie()
    {
        int heart = Random.Range(0, 2);
        PoolOfFoodController.ShowFood(this.transform.position, PoolOfFoodController.TYPE.COIN, exp, exp.ToString());
        PoolOfFoodController.ShowFood(this.transform.position, PoolOfFoodController.TYPE.HEART, heart, heart.ToString());

        float t = 0.0f;

        while (t < 2.0f)
        {
            Vector3 pos = transform.position;
            pos.y -= Time.deltaTime;
            transform.position = pos;

            t += Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }

        Destroy(this.transform.parent.gameObject);
    }

    public void Hit(float damage)
    {
        hp -= (int)damage;
        Vector3 pos = transform.position;
        pos.x += Random.Range(-100, 100) * 0.01f;
        pos.y += Random.Range(-100, 100) * 0.01f;
        pos.z += Random.Range(-100, 100) * 0.01f;
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.HIT,
            new EventSystem.HitEvent((int)damage, pos));

        if (hp <= 0)
        {
            if (!IsDie)
                Die();
        }
        else
        {
            if (!IsAttack)
                animator.SetTrigger("Hit");
        }
        StartCoroutine("HitFlash");
    }

    IEnumerator HitFlash()
    {
        float c = 0.8f;
        while (hitTime < 0.2f)
        {
            skinnedMeshRenderer.material.SetColor("_Emission", new Color(c, c, c, 1));
            yield return new WaitForSeconds(0.01f);
            hitTime += 0.01f;
            c -= 0.03f;
        }

        c = 0;
        skinnedMeshRenderer.material.SetColor("_Emission", new Color(c, c, c, 1));
        hitTime = 0.0f;
    }

    void Shoot()
    {
        Debug.Log("Shoot");
        Quaternion rot1 = this.transform.rotation;
        Quaternion rot2 = this.transform.rotation;
        Quaternion rot3 = this.transform.rotation;
        Quaternion rot4 = this.transform.rotation;

        rot2 *= Quaternion.Euler(0.0f, -90.0f, 0.0f);
        rot3 *= Quaternion.Euler(0.0f, -180.0f, 0.0f);
        rot4 *= Quaternion.Euler(0.0f, -270.0f, 0.0f);

        bulletBuilder.Clone(bullet, this.transform.position, rot1);
        bulletBuilder.Clone(bullet, this.transform.position, rot2);
        bulletBuilder.Clone(bullet, this.transform.position, rot3);
        bulletBuilder.Clone(bullet, this.transform.position, rot4);
        IsAttack = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(destination, 0.3f);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (collisionTime >= 1.0f)
            {
                collision.gameObject.GetComponent<Player>().Hit(damage);
                collisionTime = 0.0f;
            }
        }
    }
}
