﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Linq;

public class SoundManager : MonoBehaviour
{
    public AudioSource audioSource;
    public List<Sound> soundList = new List<Sound>();

    private void Awake()
    {
        if (audioSource == null)
        {
            audioSource = transform.GetChild(0).GetComponent<AudioSource>() ?? null;
            if (audioSource == null)
                audioSource = GetComponent<AudioSource>() ?? gameObject.AddComponent<AudioSource>();
        }
        foreach (var sound in soundList)
        {
            sound.SetSource(audioSource);
        }
    }
    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.G))
        {
            //AllSoundStop();
            print("sound Stop");
            SetAllSoundsVolume(0);
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            print("sound Play");
            SetAllSoundsVolume(1);
        }
    }
    public void SoundPlay(string name)
    {
        Sound sound = soundList.Find(x => x.Name.Equals(name));
        sound?.Play();
    }
    public void SoundStop(string name)
    {
        Sound sound = soundList.Find(x => x.Name.Equals(name));
        sound?.Stop();
    }
    static public void AllSoundStop()
    {
        var list = GameObject.FindObjectsOfType<SoundManager>();
        foreach (var sound in list)
        {
            sound.soundList?.ForEach(x => x.Stop());
        }
    }
    static public void AllSoundPlay()
    {
        var list = GameObject.FindObjectsOfType<SoundManager>();
        foreach (var sound in list)
        {
            sound.soundList?.ForEach(x => x.Play());
        }
    }
    static public void SetAllSoundsVolume(float value)
    {
        var sources = GameObject.FindObjectsOfType<SoundManager>();
        foreach (var source in sources)
        {
            source.audioSource.volume = value;
        }
    }
}
