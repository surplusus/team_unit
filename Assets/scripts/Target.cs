﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    Player player;
    Transform child;
    void Start()
    {
        child = transform.GetChild(0);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player.TargetMonster == null)
        {
            if (child.gameObject.activeSelf)
                child.gameObject.SetActive(false);
        }
        else
        {
            if (!child.gameObject.activeSelf)
            {
                child.gameObject.SetActive(true);
            }

            Vector3 pos = player.TargetMonster.transform.position;
            pos.y = 0;
            this.transform.position = pos;
        }
    }
}
