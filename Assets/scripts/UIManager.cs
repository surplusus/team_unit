﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;

public class UIManager : MonoBehaviour
{
    #region 매니저라면 응당있어야 할 것들
    public static UIManager Me = null;
    public static void Instantiate()
    {
        //Me.SetUp();
    }
    public static void Release()
    {
        Me = null;
    }
    #endregion
    public UnityEvent RoleSchedulerEvent;
    public bool IsDebug => isDebug;
    [SerializeField] bool isDebug;
    [SerializeField] GameObject MainLoader = null;
    [SerializeField] GameObject StageLoader = null;
    private void Awake()
    {
        UIManager.Me = this;
        if (MainLoader == null)
        {
            //MainLoader = AssetDatabase.LoadAssetAtPath("Assets/UnitPrefab/MainLoader.prefab", typeof(GameObject)) as GameObject;
            MainLoader = MainLoader ?? DataManager.GetGameObjectIndexing("MainLoader");
        }
        if (StageLoader == null)
        {
            //StageLoader = AssetDatabase.LoadAssetAtPath("Assets/UnitPrefab/StageLoader.prefab", typeof(GameObject)) as GameObject;
            StageLoader = StageLoader ?? DataManager.GetGameObjectIndexing("StageLoader");
        }
    }
    private void OnDestroy()
    {
        UIManager.Release();
    }
    public void LoadMainLoadingWindow()
    {
        Instantiate(MainLoader, Vector3.zero, Quaternion.identity);
    }
    public void LoadStageLoadingWindow()
    {
        Instantiate(StageLoader, Vector3.zero, Quaternion.identity);
    }
}