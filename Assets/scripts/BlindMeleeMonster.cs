﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using UnityEngine.AI;

public class BlindMeleeMonster : MonoBehaviour, IMonster
{
    GameObject Prefab;
    NavMeshAgent navMeshAgent;
    Animator animator;
    BehaviorTree behaviorTree;
    SkinnedMeshRenderer skinnedMeshRenderer;

    public bool IsDie = false;
    public bool IsHit = false;
    public bool IsAttack = false;
    public bool IsMove = false;
    float idleTime = 0.0f;
    float hitTime = 0.0f;
    float deadTime = 0.0f;
    float moveTime = 0.0f;
    float collisionTime = 1.0f;
    float idleTimeMax = 0.0f;


    private int hp;
    private int exp;
    private float damage;
    private float moveSpeed;

    Color colorOri;
    Vector3 destination;
    
    void Awake()
    {
        behaviorTree = this.gameObject.GetComponent<BehaviorTree>();
        animator = this.gameObject.GetComponentInChildren<Animator>();
        navMeshAgent = this.gameObject.GetComponent<NavMeshAgent>();
    }
    // Start is called before the first frame update
    void Start()
    {
        hp = 300;
        exp = 4;
        damage = 33;
        moveSpeed = 2;
        idleTime = Random.Range(1.5f, 2.0f);
        colorOri = this.gameObject.transform.GetComponentInChildren<SkinnedMeshRenderer>().material.color;
        skinnedMeshRenderer = this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        idleTimeMax = Random.Range(1, 3);
    }

    // Update is called once per frame
    void Update()
    {
        if (collisionTime < 1.0f)
            collisionTime += Time.deltaTime;

        animator.SetFloat("Move", navMeshAgent.velocity.magnitude);
    }

    int IMonster.HP
    {
        get { return hp; }
        set { hp = value; }
    }

    int IMonster.EXP
    {
        get { return exp; }
        set { exp = value; }
    }

    float IMonster.Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    float IMonster.MoveSpeed
    {
        get { return moveSpeed; }
        set { moveSpeed = value; }
    }

    public void Idle()
    {
        if(idleTime > idleTimeMax)
        {
            IsMove = true;
            destination = new Vector3(transform.position.x + Random.Range(-3, 3), 0, transform.position.z + Random.Range(-3, 3));
            navMeshAgent.destination = destination;
            idleTime = 0.0f;
            idleTimeMax = Random.Range(1, 3);
        }
        else
        {
            idleTime += Time.deltaTime;
        }
    }
    public void Move()
    {
        if (moveTime > 3.0f)
        {
            moveTime = 0.0f;
            IsMove = false;
            navMeshAgent.destination = this.transform.position;
        }
        else
        {
            moveTime += Time.deltaTime;
        }
    }
    public void Attack(){}
    public void Die()
    {
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.MONSTERCOUNT, new EventSystem.MonsterCountEvent());
        animator.SetTrigger("Die");
        IsDie = true;
        gameObject.layer = LayerMask.NameToLayer("Die");
        StartCoroutine("FallDie");
    }
    IEnumerator FallDie()
    {
        PoolOfFoodController.ShowFood(this.transform.position, PoolOfFoodController.TYPE.COIN, exp, exp.ToString());

        float t = 0.0f;

        while (t < 2.0f)
        {
            Vector3 pos = transform.position;
            pos.y -= Time.deltaTime;
            transform.position = pos;

            t += Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }

        Destroy(this.transform.parent.gameObject);
    }
    public void Hit(float damage)
    {
        hp -= (int)damage;
        Vector3 pos = transform.position;
        pos.x += Random.Range(-100, 100) * 0.01f;
        pos.y += Random.Range(-100, 100) * 0.01f;
        pos.z += Random.Range(-100, 100) * 0.01f;
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.HIT,
            new EventSystem.HitEvent((int)damage, pos));

        if (hp <= 0)
        {
            if (!IsDie)
                Die();
        }
        else
        {
            if (!IsAttack)
                animator.SetTrigger("Hit");
        }
        StartCoroutine("HitFlash");
    }

    IEnumerator HitFlash()
    {
        float c = 0.8f;
        while (hitTime < 0.2f)
        {
            skinnedMeshRenderer.material.SetColor("_Emission", new Color(c, c, c, 1));
            yield return new WaitForSeconds(0.01f);
            hitTime += 0.01f;
            c -= 0.03f;
        }

        c = 0;
        skinnedMeshRenderer.material.SetColor("_Emission", new Color(c, c, c, 1));
        hitTime = 0.0f;
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (collisionTime >= 1.0f)
            {
                collision.gameObject.GetComponent<Player>().Hit(damage);
                collisionTime = 0.0f;
            }
        }
    }
}
