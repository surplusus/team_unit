﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DATA;
using System.Text;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;
using System;
using System.Linq;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Threading.Tasks;

public class DataManager : MonoBehaviour
{
    #region MakeSingleton & 초기화
    static private DataManager me = null;
    static public DataManager Me
    {
        get
        {
            if (me == null)
            {
                me = FindObjectOfType<DataManager>();
                DontDestroyOnLoad(me.gameObject);
                me.Initiate();
            }
            return me;
        }
    }
    void Initiate()
    {
        AssetLabel = new AssetLabelReference();
        myGameObjectIndexing = new Dictionary<string, GameObjectIndexing>(8);
        indexOfGameObject = new Dictionary<string, int>(8);
        AllLoadedAssets = new List<GameObject>(8);
        ItemAssets = new List<GameObject>(4);
        Locations = new List<IResourceLocation>(8);
        AssetLabel.labelString = "Prefab";
        LoadAddressableNamedOf(AssetLabel.labelString);
        InitAsset("Item", ItemAssets);
        IEnumerator coroutine = InitScriptableObjects();
        StartCoroutine(coroutine);
        CreateItemSlotIndicator();
    }
    #endregion
    #region Member
    public List<GameObject> AllLoadedAssets;
    public List<GameObject> ItemAssets;
    public List<GameObject> ItemsInIndicator;
    public SOs scriptableObjects;
    Dictionary<string, GameObjectIndexing> myGameObjectIndexing;
    Dictionary<string, int> indexOfGameObject;
    List<IResourceLocation> Locations;
    static bool isAllAssetsLoaded = false;
    static bool isAllScriptableObejctsLoaded = false;
    static bool isAllItemSlotObjectsLoaded = false;
    [Serializable] public class SOs
    {
        public Attack attack;
        public Condition condition;
        public Status playerStatus;
        public TalentData talent;
        public UserData userData;
        public BulletPrebPathList bulletPrebPath;
    }
    [SerializeField] static AssetLabelReference AssetLabel;
    [SerializeField] GameObject prebRollScheduler;
    // Property
    public UI_ItemIndicator ItemIndicatorScript { get; private set; }
    static public bool IsAllAssetsLoaded
    {
        get
        {
            if (me == null)
                return false;
            var cntLoc = Me?.Locations?.Count ?? -1;
            var cntAsset = Me?.AllLoadedAssets?.Count ?? -1;
            if (cntLoc > 0 && cntLoc == cntAsset)
                isAllAssetsLoaded = true;

            return isAllAssetsLoaded;
        }
    }
    static public bool IsAllItemSlotObjectsLoaded
    {
        get
        {
            if (!IsAllAssetsLoaded)
                isAllItemSlotObjectsLoaded = false;
            return isAllItemSlotObjectsLoaded;
        }
        private set => isAllItemSlotObjectsLoaded = value;
    }
    static public bool IsAllScriptableObejctsLoaded
    {
        get
        {
            if (!IsAllAssetsLoaded)
                isAllScriptableObejctsLoaded = false;
            return isAllScriptableObejctsLoaded;
        }
        private set => isAllScriptableObejctsLoaded = value;
    }
    #endregion

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveAllSOdata();
        }
    }
    private void OnDisable()
    {
        SaveData<Attack>(ref scriptableObjects.attack);
        SaveData<Status>(ref scriptableObjects.playerStatus);
        SaveData<TalentData>(ref scriptableObjects.talent);
        SaveData(ref scriptableObjects.userData);
        SaveData<BulletPrebPathList>(ref scriptableObjects.bulletPrebPath);
    }
    IEnumerator InitScriptableObjects()
    {
        yield return new WaitUntil(() => IsAllAssetsLoaded);
        LoadSOFromJsonValue<Attack>(ref scriptableObjects.attack);
        LoadSOFromJsonValue<Status>(ref scriptableObjects.playerStatus);
        LoadSOFromJsonValue<TalentData>(ref scriptableObjects.talent);
        LoadSOFromJsonValue<UserData>(ref scriptableObjects.userData);
        LoadSOFromJsonValue<BulletPrebPathList>(ref scriptableObjects.bulletPrebPath);
        IsAllScriptableObejctsLoaded = true;
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.DATA_LOADSO
            , new EventSystem.DataLoadScriptableObejctEvent());
    }
    async void CreateItemSlotIndicator()
    {
        var obj = await CreateAddressableLoader.InitAsset("ItemIndicator");
        var indicator = Instantiate<GameObject>(obj[0], transform);
        ItemIndicatorScript = indicator.GetComponentInChildren<UI_ItemIndicator>();
        ItemsInIndicator = ItemsInIndicator ?? new List<GameObject>(10);
        indicator.GetComponent<Canvas>().enabled = false;
        indicator.transform.GetChild(0).gameObject.SetActive(false);
        indicator = indicator.transform.GetChild(1).gameObject;
        int size = indicator.transform.childCount;
        for (int i = 0; i < size; ++i)
        {
            ItemsInIndicator.Add(indicator.transform.GetChild(i).gameObject);
        }
        IsAllItemSlotObjectsLoaded = true;
        //EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.DATA_LOADSLOT
        //    , new EventSystem.DataLoadItemSlotIndicatorEvent());
        scriptableObjects.userData.LoadedSpriteNames = new List<string>(ItemIndicatorScript.SpriteNames);
    }
    static public void InitAsset(string label, List<GameObject> list)
    {
        Addressables.LoadResourceLocationsAsync(label).Completed += loc =>
        {
            if (loc.Status == AsyncOperationStatus.Failed)
                Debug.Log($"{label} : 로드에 실패했습니다");

            var locations = new List<IResourceLocation>(loc.Result);
            foreach (var item in locations)
            {
                Addressables.LoadAssetAsync<GameObject>(item).Completed += o =>
                {
                    list.Add(o.Result);
                };
            }
        };
    }
    private void LoadSOFromJsonValue<T>(ref T data) where T : class, IData
    {
        if (data != null)
        {
            var so = data as T;
            LoadData<T>(ref so);
        }
    }
    #region LoadAddressables
    private static void LoadAddressableNamedOf(string label)
    {
        Addressables.LoadResourceLocationsAsync(label).Completed += OnLoaded;
    }
    private static void OnLoaded(AsyncOperationHandle<IList<IResourceLocation>> obj)
    {
        if (obj.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log("Failed to load addressableObject, retrying in 1 second...");
            return;
        }
        Me.Locations = new List<IResourceLocation>(obj.Result);
        for (int i = 0; i < Me.Locations.Count; ++i)
        {
            var loc = Me.Locations[i];
            Addressables.LoadAssetAsync<GameObject>(loc).Completed += o =>
            {
                Me.AllLoadedAssets.Add(o.Result);
                string path = loc.InternalId;
                string sName = loc.PrimaryKey;
                string key = loc.GetHashCode().ToString();
                GameObject go = o.Result;
                if (Me.indexOfGameObject == null)
                    Me.indexOfGameObject = new Dictionary<string, int>();
                int tryValue = 0;
                if (Me.indexOfGameObject.TryGetValue(sName, out tryValue))
                    Me.indexOfGameObject.Add(sName, Me.indexOfGameObject.Count);
                GameObjectIndexing tryGOI = null;
                if (Me.myGameObjectIndexing.TryGetValue(sName, out tryGOI))
                    Me.myGameObjectIndexing.Add(sName, new GameObjectIndexing(sName, key, path, go));
            };
        }
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.DATA_LOADPREB
            , new EventSystem.DataLoadPrebEvent());
    }
    #endregion
    #region Save
    public void SaveAllSOdata()
    {
        SaveData<Attack>(ref scriptableObjects.attack);
        SaveData<Condition>(ref scriptableObjects.condition);
        SaveData<Status>(ref scriptableObjects.playerStatus);
        SaveData<TalentData>(ref scriptableObjects.talent);
        SaveData(ref scriptableObjects.userData);
        SaveData(ref scriptableObjects.bulletPrebPath);
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.DATA_SAVESO
            , new EventSystem.DataSaveScriptableObejctEvent());
    }
    public static void SaveData(ref UserData data)
    {
        if (Me?.ItemsInIndicator == null)
        {
            Debug.LogError("Error : ItemsInIndicator가 없습니다");
            return;
        }
        var indicatorScript = Me.ItemIndicatorScript;
        Me.scriptableObjects.userData.LoadedSpriteNames = new List<string>(indicatorScript.SpriteNames);
        string path = string.Format($"{Application.dataPath}/Data/UserData.json");
        string jsonData = JsonUtility.ToJson(data, true);
        File.WriteAllText(path, jsonData);
    }
    public static void SaveData<T>(ref T json) where T : class, IData
    {
        if (json == null)
        {
            Debug.LogError("Error : json파일이 없습니다");
            return;
        }
        var typeName = json.GetType().Name;
        string path = string.Format($"{Application.dataPath}/Data/{typeName}.json");
        string jsonData = JsonUtility.ToJson(json, true);
        File.WriteAllText(path, jsonData);
    }
    #endregion
    #region Load
    public static void LoadData<T>(ref T json) where T : class, IData
    {
        var typeName = json.GetType().Name;
        string path = string.Format($"{Application.dataPath}/Data/{typeName}.json");
        if (!File.Exists(path))
        {
            Debug.LogError($"파일이 존재하지 않습니다 : {json.GetType().Name}");
            return;
        }
        using (FileStream stream = new FileStream($"{path}", FileMode.Open))
        {
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            string jsonData = Encoding.UTF8.GetString(bytes);
            JsonUtility.FromJsonOverwrite(jsonData, json);
        }
    }
    public static void LoadData(ref UserData data)
    {
        string path = string.Format($"{Application.dataPath}/Data/UserData.json");
        FileStream stream = new FileStream($"{path}", FileMode.Open);
        byte[] bData = new byte[stream.Length];
        stream.Read(bData, 0, bData.Length);
        stream.Close();
        string jsonData = Encoding.UTF8.GetString(bData);
        JsonUtility.FromJsonOverwrite(jsonData, data);
    }
    #endregion
    public static void LoadData(ref BulletPrebPathList data)
    {
        string path = string.Format($"{Application.dataPath}/Data/BulletPrebPath.json");
        FileStream stream = new FileStream($"{path}", FileMode.Open);
        byte[] bytes = new byte[stream.Length];
        stream.Read(bytes, 0, bytes.Length);
        stream.Close();
        string jsonData = Encoding.UTF8.GetString(bytes);
        JsonUtility.FromJsonOverwrite(jsonData, data);
    }
    public static void SaveData(ref BulletPrebPathList data)
    {
        var o1 = data.SlingAsset.GO;
        var o2 = data.BowAsset.GO;
        var o3 = data.BoomerangAsset.GO;
        string path = string.Format($"{Application.dataPath}/Data/BulletPrebPathList.json");
        string jsonData = JsonUtility.ToJson(data, true);
        File.WriteAllText(path, jsonData);
    }
    static public GameObject GetGameObjectIndexing(string key)
    {
        if (!Me.indexOfGameObject.ContainsKey(key) 
            || Me?.indexOfGameObject?[key] == null)
            return null;

        int idx = Me.indexOfGameObject[key];
        return Me.AllLoadedAssets[idx];
    }
    static public Sprite GetSpriteFromItemIndicator(string spriteName)
    {
        {
        //if (!Me.indexOfGameObject.ContainsKey("ItemIndicator") || Me?.indexOfGameObject?["ItemIndicator"] == null)
        //    return null;
        //if (Me.ItemsInIndicator == null)
        //{
        //    Me.ItemsInIndicator = new List<GameObject>(10);
        //    var indicator = Me.transform.GetChild(0);
        //    if (indicator == null)
        //    {
        //        indicator = GetGameObjectIndexing("ItemIndicator").transform;
        //        indicator.SetParent(Me.transform, false);
        //    }
        //    var size = indicator.GetSiblingIndex();
        //    for (int i = 0; i < size; ++i)
        //        Me.ItemsInIndicator.Add(indicator.GetChild(i).gameObject);
        //}
        //return Me.ItemsInIndicator.Find(x => x.name == spriteName);
        }
        return Me.ItemIndicatorScript.GetSpriteBy(spriteName);
    }
    static public void InstantiateGameObjectIndexing(string key, ref GameObject obj)
    {
        GameObject result = null;
        DataManager.Me.StartCoroutine(Me.WaitUntilDataReady(result, key));
        obj = result;
    }
    IEnumerator WaitUntilDataReady(GameObject o, string key)
    {
        yield return new WaitUntil(() => DataManager.IsAllAssetsLoaded);
        o = Instantiate<GameObject>(GetGameObjectIndexing(key));
    }
    static public void InstantiateRollScheduler()
    {
        Instantiate<GameObject>(Me.prebRollScheduler);
    }
}

public static class CreateAddressableLoader
{
    public static async Task InitAsset<T>(string assetNameOrLabel, List<T> createObjs) where T : UnityEngine.Object
    {
        var locations = await Addressables.LoadResourceLocationsAsync(assetNameOrLabel).Task;
        foreach (var location in locations)
        {
            createObjs.Add(await Addressables.InstantiateAsync(location).Task as T);
        }
    }
    public static async Task<List<GameObject>> InitAsset(string assetLabel)
    {
        var locations = await Addressables.LoadResourceLocationsAsync(assetLabel).Task;
        if (locations.Count == 0)
        {
            Debug.Log($"Failed to read AddressableAsset : Label Of {assetLabel}");
            return null;
        }
        List<GameObject> re = new List<GameObject>();
        Action<GameObject> callback = o =>
        {
            re.Add(o);
        };
        var list = await Addressables.LoadAssetsAsync<GameObject>(locations, callback).Task;
        return re;
    }
    public static async Task<List<GameObject>> InitAsset(List<string> assetNames)
    {
        string[] sList = assetNames.ToArray();
        List<GameObject> re = new List<GameObject>();
        Action<GameObject> callback = o => { };
        var list = await Addressables.LoadAssetsAsync<GameObject>(sList, callback, Addressables.MergeMode.Union).Task;
        if (list.Count == 0)
        {
            Debug.Log($"Failed to read AddressableAsset : List Of {assetNames.Count}");
            return null;
        }
        return list as List<GameObject>;
    }
}