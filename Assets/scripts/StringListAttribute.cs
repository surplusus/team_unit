﻿using UnityEngine;
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
namespace UnityEditor
{
    [CustomPropertyDrawer(typeof(StringListAttribute))]
    public class StringListAttributeDrawer : PropertyDrawer
    {
        List<GUIContent> m_StringList;
        int m_DefaultIndex = -1;
        SoundManager manager;
        protected virtual StringListAttribute Attribute => attribute as StringListAttribute;
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var type = property.propertyType.GetType();
            if (!type.Equals(typeof(string)))
                return;

            if (m_DefaultIndex == -1)
            {
                SetUp(property);
            }

            int oldIndex = m_DefaultIndex;
            m_DefaultIndex = EditorGUI.Popup(position, label, m_DefaultIndex, m_StringList.ToArray());

            if (oldIndex != m_DefaultIndex)
                property.stringValue = m_StringList[m_DefaultIndex].text;
        }
        void SetUp(SerializedProperty property)
        {
            manager = Attribute.Manager;
            List<string> lst = new List<string>(property.arraySize);
            m_StringList = new List<GUIContent>(property.arraySize);
            for (int i = 0; i < property.arraySize; ++i)
            {
                m_StringList.Add(new GUIContent(property.GetArrayElementAtIndex(i).stringValue));
            }
            m_DefaultIndex = 0;
        }
    }
}
#endif

[AttributeUsage(AttributeTargets.Field)]
public sealed class StringListAttribute : PropertyAttribute
{
    public StringListAttribute(SoundManager mgr)
    {
        this.Manager = mgr;
    }
    public SoundManager Manager { get; private set; }
}