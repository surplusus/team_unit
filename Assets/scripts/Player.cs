﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DATA;
using UnityEngine.Events;
using System;

public class Player : MonoBehaviour
{
    [ReadOnly(false),SerializeField] UserData userData ;  // User가 Datamanager에서 로드가 끝난뒤 넣어준다
    [ReadOnly(false),SerializeField] Status status;      // User가 Datamanager에서 로드가 끝난뒤 넣어준다
    [ReadOnly(false),SerializeField] TalentData talent;   // User가 Datamanager에서 로드가 끝난뒤 넣어준다
    [SerializeField] Condition condition;  // Datamanager에서 불러들인다 새로 만든다
    public UserData UserData { get { return userData; } set { userData = value; } }
    public Status Status { get { return status;} set { status = value; } }
    public TalentData Talent { set { talent = value; } }
    [SerializeField] float MoveSpeed = 60f;
    [SerializeField] bool IsDead = false;
    [SerializeField] Animator anim;
    [SerializeField] List<GameObject> Enemys;
    [SerializeField] ShowInventory inventory;
    public Transform LeftArm;
    public Transform RightArm;
    public Transform BulletPoint;
    List<IAbility> abilities = null;
    new Rigidbody rigidbody;
    public GameObject TargetMonster { get => targetMonster; set => targetMonster = value; }
    [ReadOnly(false), SerializeField] GameObject targetMonster;
    GameObject[] MonstersInStage;

    private void Awake()
    {
        inventory = GetComponent<ShowInventory>();
        rigidbody = GetComponent<Rigidbody>();
        MonstersInStage = GameObject.FindGameObjectsWithTag("Enemy");
        abilities = new List<IAbility>(9);
        StartCoroutine(InitiateShowInventory());
    }

    void Update()
    {
        if (IsDead)
        {
            anim.SetFloat("Velocity", 0f);
            anim.SetBool("IsOnTarget", false);
            anim.SetBool("IsDead", IsDead);
            //if(anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
            //{
            //    EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.DEAD, new EventSystem.PlayerDeadEvent());
            //}
            return;
        }
        if (status.HealthPoint <= 0f && !IsDead)
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.isKinematic = true;
            IsDead = true;
        }

        if (TargetMonster != null)
            anim.SetBool("IsOnTarget", true);
        else
            anim.SetBool("IsOnTarget", false);
        
        if (!InputManager.IsPushed)
        {           // 걷지않는 동안
            NotMove();
            // <<<
            float delayTime = 0.1f;
            RestartToLockOn(delayTime);
            float AttackSpeed = 1000.0f / status.Dexterity * Time.deltaTime;
            Attack(AttackSpeed);
        }
        else
        {
            LockOnDelay = 0f;
            Move();
        }
        TurnAround();

    }
    IEnumerator InitiateShowInventory()
    {
        yield return new WaitUntil(() => DataManager.IsAllScriptableObejctsLoaded);
        inventory.Condition = DataManager.Me.scriptableObjects.condition;
        inventory.UserData = DataManager.Me.scriptableObjects.userData;
        inventory.ShowEquipedItems();
    }
    public void ShowWeaponOnPlayerHand(Weapon.TYPE type, GameObject inGamePreb)
    {
        switch (type)
        {
            case Weapon.TYPE.SLING:
                Instantiate(inGamePreb, RightArm, false);
                break;
            case Weapon.TYPE.BOW:
                Instantiate(inGamePreb, LeftArm, false);
                break;
            case Weapon.TYPE.BOOMERANG:
                Instantiate(inGamePreb, RightArm, false);
                break;
            default:
                break;
        }
    }
    float LockOnDelay = 0f;
    void RestartToLockOn(float delaytime)
    {
        if (LockOnDelay < delaytime)
        {
            LockOnDelay += Time.deltaTime;
            return;
        }
        float radius = 20f;
        LayerMask checkingLayer = LayerMask.GetMask("Enemy") | LayerMask.GetMask("Fly");
        Collider[] targetColliers = Physics.OverlapSphere(transform.position, radius, checkingLayer);
        bool isHit = targetColliers.Length == 0 ? false : true;
        if (isHit)
        {
            float closestDist = 999f;
            for (int i = 0; i < targetColliers.Length; ++i)
            {
                GameObject obj = targetColliers[i].gameObject;
                if (!Enemys.Contains(obj))
                    Enemys.Add(targetColliers[i].gameObject);
                float dist = Mathf.Abs((obj.transform.position - transform.position).sqrMagnitude);
                if (dist <= closestDist)
                {
                    closestDist = dist;
                    targetMonster = obj;
                }
            }
        }
        else
        {
            targetMonster = null;
        }
    }
    void SelectEnemyLockedOn()
    {
        foreach (var item in MonstersInStage)
        {
            item.GetComponent<MeshRenderer>().material.color = Color.white;
        }
        if (TargetMonster != null)
            TargetMonster.GetComponent<MeshRenderer>().material.color = Color.red;
    }
    void Move()
    {
        Vector3 vLook = InputManager.DeltaPos;
        Vector3 forward = new Vector3(vLook.x * MoveSpeed * 0.1f, 0, vLook.y * MoveSpeed * 0.1f);

        rigidbody.velocity = transform.forward * MoveSpeed * 3.0f;
        anim.SetFloat("Velocity", rigidbody.velocity.magnitude);
    }
    void NotMove()
    {
        rigidbody.velocity = Vector3.zero;
        anim.SetFloat("Velocity", rigidbody.velocity.magnitude);
    }
    float AttackTimeAccumulation = 0f;
    void Attack(float attackTime)
    {
        // 타겟이 없거나 Player가 움직이고 있으면 리턴
        if (TargetMonster == null || rigidbody.velocity.sqrMagnitude > 0.01f)
        {
            AttackTimeAccumulation = 0.0f;
            return;
        }
        // 공속 시간이 안되면 그냥 리턴
        if (AttackTimeAccumulation < attackTime)
        {
            AttackTimeAccumulation += Time.deltaTime;
            return;
        }

        anim.SetTrigger("OnAttack");
        AttackTimeAccumulation = 0.0f;
    }
    void TurnAround()
    {
        Vector3 vLook = new Vector3(InputManager.DeltaPos.x, 0, InputManager.DeltaPos.y);
        float slerpvalue = 0.1f;
        // 타켓 온일 때
        if (TargetMonster != null && !InputManager.IsPushed)
        {
            vLook = TargetMonster.transform.position - transform.position;
            Vector3.Normalize(vLook);
            slerpvalue = 0.7f;
        }
        // 입력이 없을 때 
        if (InputManager.IsPushed)
        {
            slerpvalue = 0.4f;
        }

        Quaternion rotToTarget = vLook.sqrMagnitude > 0.001f ? Quaternion.LookRotation(vLook) : transform.rotation;
        Quaternion ang = Quaternion.Slerp(transform.rotation, rotToTarget, slerpvalue);
        transform.rotation = ang;
    }
    void EquipAbility(DATA.AbilityType type)
    {
        switch (type)
        {
            case DATA.AbilityType.S_AttackBoost:
                abilities.Add(new S_AttackBoost());
                break;
        }
        UpdateAttackData();
    }
    void UpdateAttackData()
    {

    }
    public void SetWeaponTypeAnimation(Weapon.TYPE weaponType)
    {
        anim.SetInteger("WeaponType", (int)weaponType);
    }
    public void Hit(float damage)
    {
        status.HealthPoint -= (int)damage;
        if (status.HealthPoint < 0) status.HealthPoint = 0;

        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.HIT, new EventSystem.HitEvent((int)damage, transform.position));
    }
}
