﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using DATA;

public class UI_Lobby_MouseClickLayer : MonoBehaviour, IPointerClickHandler
{
    public UnityEvent OnClick = new UnityEvent();
    public void OnPointerClick(PointerEventData eventData)
    {
        OnClick?.Invoke();
    }
}
