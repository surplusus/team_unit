﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
public class UI_Lobby_TalentUpgradedWindow : MonoBehaviour
{
    [SerializeField] GameObject talentWindow;
    [SerializeField] GameObject menuBarWindow;
    [ReadOnly(false), SerializeField] GameObject selectedButton;
    [SerializeField] GameObject mouseClickLayer;
    [SerializeField] DATA.TalentData data;
    [SerializeField] GameObject me;
    [SerializeField] GameObject[] medalMaskPrebs;
    new string name;
    string explain;
    GameObject lockobj;
    private void Start()
    {
        var talentScript = talentWindow.GetComponent<UI_Lobby_RandomFocus>();
        talentScript.AfterLevelUpEvent.AddListener(SetUpSelectedButton);
        me.SetActive(false);
    }
    void SetUpSelectedButton(GameObject button, int index)
    {
        selectedButton = button;
        talentWindow.SetActive(false);  // 재능창 끄고
        menuBarWindow.SetActive(false); // 메뉴바 끄고
        gameObject.SetActive(true);     // 업그레이드 화면 켠다

        var medalMask = transform.Find("MedalMask");
        var medal = Instantiate(medalMaskPrebs[index], medalMask, false);

        name = medal.transform.Find("Name").GetComponent<Text>().text;
        explain = data.ShowExplanation(index);

        var myName = transform.Find("NameMask").GetComponent<TextMeshProUGUI>();
        myName.text = name;
        var myExpplain = transform.Find("ExplanMask").GetComponent<TextMeshProUGUI>();
        myExpplain.text = explain;

        data.Talents[index].Level += 1;
    }
}
