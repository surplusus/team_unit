﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UI_Store_BoxController : MonoBehaviour
{
    [SerializeField] GameObject backEffect;
    [SerializeField] GameObject openEffect;
    [SerializeField] GameObject basicBox;
    [SerializeField] GameObject rareBox;
    [SerializeField] GameObject popUpWindow;
    List<AnimationState> animStates = new List<AnimationState>(4);
    IEnumerator PlayBox(GameObject box)
    {
        Animation anim = box.GetComponent<Animation>();
        foreach (AnimationState ani in anim)
            animStates.Add(ani);

        var query = from it in animStates
                    select new { it.name, it.clip, it.length };
        foreach (var item in query)
        {
            print(item);
        }

        anim.Play("boxappear");  // boxappear
        int count = 3;
        
        yield return new WaitForSeconds(anim["boxappear"].clip.length);
        anim.Play("stand");   // stand
        
        yield return new WaitForSeconds(anim["stand"].clip.length * count);
        anim.Play("open");   // open
        openEffect.SetActive(true);
        backEffect.SetActive(true);
        
        yield return new WaitForSeconds(2f);
        anim.Play("boxdisappear");   // boxdisappear
        yield return new WaitForSeconds(anim["boxdisappear"].clip.length);
        gameObject.SetActive(false);
        popUpWindow.SetActive(true);
    }
    public void OpenBasicBox()
    {
        basicBox.SetActive(true);
        rareBox.SetActive(false);
        popUpWindow.SetActive(false);
        StartCoroutine(PlayBox(basicBox));
    }
    public void OpenRareBox()
    {
        basicBox.SetActive(false);
        rareBox.SetActive(true);
        popUpWindow.SetActive(false);
        StartCoroutine(PlayBox(rareBox));
    }
}
