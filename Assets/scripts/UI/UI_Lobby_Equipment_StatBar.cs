﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DATA;
using System;

public class UI_Lobby_Equipment_StatBar : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI attackTMP;
    [SerializeField] TextMeshProUGUI HPTMP;
    [SerializeField] UI_Lobby_Equipment_EquipedSlot weapon;
    [SerializeField] UI_Lobby_Equipment_EquipedSlot armor;
    [SerializeField] UI_Lobby_Equipment_EquipedSlot ring1;
    [SerializeField] UI_Lobby_Equipment_EquipedSlot ring2;
    [SerializeField] UI_Lobby_Equipment_EquipedSlot pet1;
    [SerializeField] UI_Lobby_Equipment_EquipedSlot pet2;
    [ReadOnly(false), SerializeField] Status playerStatus;
    List<UI_Lobby_Equipment_EquipedSlot> slots;
    public bool IsSlotChanged { set => isDirty = true; }
    bool isDirty = false;
    private void Awake()
    {
        StartCoroutine(SetUp());
        slots = new List<UI_Lobby_Equipment_EquipedSlot>(6) { weapon, armor, ring1, ring2, pet1, pet2, };
    }
    IEnumerator SetUp()
    {
        yield return new WaitUntil(() => DataManager.IsAllScriptableObejctsLoaded);
        playerStatus = DataManager.Me.scriptableObjects.playerStatus;
        isDirty = true;
    }
    string CalcHP()
    {
        var value = playerStatus.MaxHealthPoint;
        for (int i = 0; i < slots.Count; ++i)
            value += slots[i].GetBuffHP();
        return value.ToString();
    }
    string CalcAttack()
    {
        var value = playerStatus.Strangth;
        for (int i = 0; i < slots.Count; ++i)
            value += slots[i].GetBuffAttack();
        return value.ToString();
    }
    private void Update()
    {
        var prevHP = int.Parse(HPTMP.text);
        var prevAttack = int.Parse(attackTMP.text);
        if (isDirty)
        {
            var curHP = int.Parse(CalcHP());
            var curAttack = int.Parse(CalcAttack());
            bool isCompleted1 = false, isCompleted2 = false;
            Mathf.Lerp(prevHP, curHP, 0.9f);
            Mathf.Lerp(prevAttack, curAttack, 0.9f);
            if (Mathf.Abs(prevHP - curHP) < 0.1f)
            {
                prevHP = curHP;
                isCompleted1 = true;
            }
            if (Mathf.Abs(prevAttack - curAttack) < 0.1f)
            {
                prevAttack = curAttack;
                isCompleted2 = true;
            }
            HPTMP.text = prevHP.ToString();
            attackTMP.text = prevAttack.ToString();
            if (isCompleted1 && isCompleted2)
                isDirty = false;
        }

    }
}
