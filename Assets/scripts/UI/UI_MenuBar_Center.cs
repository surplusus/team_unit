﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_MenuBar_Center : MonoBehaviour
{
    public GameObject MyWorld;
    public GameObject MyNonWorld;
    private void Awake()
    {
        // world
        MyWorld = transform.GetChild(0).gameObject;
        // not world
        MyNonWorld = transform.GetChild(1).gameObject;
    }
    public void SayIAmWorld()
    {
        MyWorld.SetActive(true);
        MyNonWorld.SetActive(false);
    }
    public void SayIAmNotWorld()
    {
        MyWorld.SetActive(false);
        MyNonWorld.SetActive(true);
    }
    public void SayIAmNothing()
    {
        MyWorld.SetActive(false);
        MyNonWorld.SetActive(false);
    }
}
