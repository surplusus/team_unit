﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using EventSystem;
using System.Linq;
using System;

public class UI_Lobby_Equipment_Inventory : MonoBehaviour
{
    [ReadOnly(false), SerializeField] Transform itemGrid;
    [ReadOnly(false), SerializeField] Transform ingredientGrid;
    [SerializeField] GameObject basicItemSlot;
    [SerializeField] GameObject rareItemSlot;
    [SerializeField] GameObject ingredientSlot;
    [SerializeField] Animator EquipmentAnim;
    [SerializeField] UI_Lobby_Equipment_FloatingWindow floatingItemWindow;
    [SerializeField] UI_Lobby_Equipment_FloatingIngredientWindow floatingIngredientWindow;
    [SerializeField] DATA.UserData userdata;
    [ReadOnly(false), SerializeField] List<Sprite> sprites = new List<Sprite>();
    [ReadOnly(false), SerializeField] List<string> spriteNames = new List<string>();
    UI_Lobby_Inventory_ItemIcon.TYPE type;
    static public Func<string, Sprite> GetSpriteByItemNameOf;

    private void Awake()
    {
        itemGrid = transform.GetChild(0);
        ingredientGrid = transform.GetChild(2);
        DataManager.LoadData(ref userdata);
        GetSpriteByItemNameOf = FuncGetSpriteOfItemNameOf;
    }
    private void OnEnable()
    {
        EventManager.AddListener(EVENT_TYPE.DATA_LOADSLOT, SetUpInventoryItems);
        EventManager.AddListener(EVENT_TYPE.EPUIPMENT_OPENFLOATINGWINDOW, OpenFloatingWindow);
    }
    private void OnDisable()
    {
        EventManager.RemoveListener(EVENT_TYPE.DATA_LOADSLOT, SetUpInventoryItems);
        EventManager.RemoveListener(EVENT_TYPE.EPUIPMENT_OPENFLOATINGWINDOW, OpenFloatingWindow);
    }
    void SetUpInventoryItems(EventSystem.EventHandler e)
    {
        if (DataManager.Me.ItemsInIndicator.Count == 0)
        {
            InvokeRepeating("ResetInventorySlots", 0.5f, 0.1f);
            return;
        }
        ResetInventorySlots();
    }
    void OpenFloatingWindow(EventSystem.EventHandler e)
    {
        var evt = e as EventSystem.LobbyEquipmentFloatingWindowEvent;
        var script = evt.script;
        if (script.name.Contains("Ingredient") || script.name.Contains("Sapphire"))
            FloatIngredientWindow(script);
        else
            FloatingItemWindow(script);
    }

    private void FloatingItemWindow(UI_Lobby_Inventory_ItemIcon script)
    {
        EquipmentAnim.Play("OpenItem");
        var formetter = new DATA.FloatingItemWindowFormetter(floatingItemWindow, script, userdata);
    }

    private void FloatIngredientWindow(UI_Lobby_Inventory_ItemIcon script)
    {
        EquipmentAnim.Play("OpenIngredient");
        var formetter = new DATA.FloatingIngredientWindowFormetter(floatingIngredientWindow, script, userdata);
    }

    void ResetInventorySlots()
    {
        if (DataManager.Me.ItemsInIndicator.Count == 0)
            return;
        CancelInvoke("ResetInventorySlots");
        foreach (var obj in DataManager.Me.ItemsInIndicator)
        {
            sprites.Add(obj.GetComponent<Image>().sprite);
            spriteNames.Add(obj.name);
        }
        var inventoryList = DataManager.Me.scriptableObjects.userData.Inventory;
        for (int i = 0; i < inventoryList.Count; ++i)
        {
            var name = inventoryList[i].Name;
            var idx = spriteNames.FindIndex(0,(x)=> x==inventoryList[i].Name);

            bool isIngredient = false;
            GameObject preb = FindSlotType(name, out isIngredient);
            GameObject inst = null;
            UI_Lobby_Inventory_ItemIcon.TYPE type = FindType(inventoryList[i].Name);
            Sprite typeSprite = null;
            if (isIngredient)
                inst = Instantiate<GameObject>(preb, ingredientGrid);
            else
            {
                inst = Instantiate<GameObject>(preb, itemGrid);
                typeSprite = FindTypeSprite(name, type);
            }
                
            var sprite = sprites[idx];
            var script = inst.GetComponent<UI_Lobby_Inventory_ItemIcon>();
            script.InitValues(sprite, typeSprite, type, inventoryList[i].Count, inventoryList[i].Level);
            script.ItemName = name;
        }
    }
    UI_Lobby_Inventory_ItemIcon.TYPE FindType(string name)
    {
        if (name.Contains("Ingredient")) return UI_Lobby_Inventory_ItemIcon.TYPE.INGREDIENT;
        if (name.Contains("Sapphire")) return UI_Lobby_Inventory_ItemIcon.TYPE.SAPPHIRE;
        if (name.Contains("Weapon")) return UI_Lobby_Inventory_ItemIcon.TYPE.WEAPON;
        if (name.Contains("Armor")) return UI_Lobby_Inventory_ItemIcon.TYPE.ARMOR;
        if (name.Contains("Ring")) return UI_Lobby_Inventory_ItemIcon.TYPE.RING;
        if (name.Contains("Pet")) return UI_Lobby_Inventory_ItemIcon.TYPE.PET;
        return UI_Lobby_Inventory_ItemIcon.TYPE.NONE;
    }
    GameObject FindSlotType(string name, out bool isIngredient)
    {
        isIngredient = false;
        if (name.Contains("Sapphire") || name.Contains("Ingredient"))
        {
            isIngredient = true;
            return ingredientSlot;
        }
        if (name.Contains("Basic"))
            return basicItemSlot;
        if (name.Contains("Rare"))
            return rareItemSlot;
        return null;
    }
    Sprite FindTypeSprite(string name, UI_Lobby_Inventory_ItemIcon.TYPE type)
    {
        string value = "";
        switch (type)
        {
            case UI_Lobby_Inventory_ItemIcon.TYPE.WEAPON:
                value = "Weapon";
                break;
            case UI_Lobby_Inventory_ItemIcon.TYPE.ARMOR:
                value = "Armor";
                break;
            case UI_Lobby_Inventory_ItemIcon.TYPE.RING:
                value = "Ring";
                break;
            case UI_Lobby_Inventory_ItemIcon.TYPE.PET:
                value = "Pet";
                break;
            default:
                break;
        }
        var re = spriteNames.Find(x => { return x.Contains("Type") && x.Contains(value); });
        if (re == null)
            return null;
        return sprites[spriteNames.IndexOf(re)];
    }
    Sprite FuncGetSpriteOfItemNameOf(string itemName)
    {
        int index = spriteNames.FindIndex(x => x == itemName);
        if (index >= 0 && index < spriteNames.Count)
            return sprites[index];
        return null; 
    }
}

namespace DATA
{
    public class FloatingItemWindowFormetter
    {
        public FloatingItemWindowFormetter(UI_Lobby_Equipment_FloatingWindow floatingWindow, UI_Lobby_Inventory_ItemIcon itemInfo, DATA.UserData userData)
        {
            this.floating = floatingWindow;
            this.item = itemInfo;
            this.userData = userData;
            SetItUp();
        }
        UI_Lobby_Equipment_FloatingWindow floating;
        UI_Lobby_Inventory_ItemIcon item;
        DATA.UserData userData;

        void SetItUp()
        {
            SetItemType();
            SetTitle();
            CopyToSelectedItem();
            SetLevel();
            SetIngredientImage();
            SetDescription();
            SetStatDescription();
            SetUpgradeDescription();
        }

        private void CopyToSelectedItem()
        {
            var basic = floating.SelectedItem.transform.GetChild(0);
            var rare = floating.SelectedItem.transform.GetChild(1);
            Transform itemObj = null;
            if (item.name.Contains("Rare"))
            {
                itemObj = rare;
                rare.gameObject.SetActive(true);
                basic.gameObject.SetActive(false);
            }
            else
            {
                itemObj = basic;
                basic.gameObject.SetActive(true);
                rare.gameObject.SetActive(false);
            }
            itemObj.GetChild(0).GetComponent<Image>().sprite = item.itemImage;
            itemObj.GetChild(1).GetComponent<Image>().sprite = item.typeImage;
        }

        private void SetStatDescription()
        {
            int l = item.Level;
            switch (item.Type)
            {
                case UI_Lobby_Inventory_ItemIcon.TYPE.NONE:
                case UI_Lobby_Inventory_ItemIcon.TYPE.SAPPHIRE:
                case UI_Lobby_Inventory_ItemIcon.TYPE.INGREDIENT:
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.WEAPON:
                    string t1 = string.Format($"공격 + 10 ( + {l}) \n공격 + {(int)l*0.1f}%");
                    floating.ItemStatDescription.text = t1;
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.ARMOR:
                    string t2 = string.Format($"최대 HP + 150 \n회피 + {(int)((l+2)*0.5)}%");
                    floating.ItemDescription.text = t2;
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.RING:
                    string t3 = string.Format($"원거리 유닛에 대한 데미지 + 20 ( + {(int)l*0.4f})");
                    floating.ItemDescription.text = t3;
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.PET:
                    string t4 = string.Format($"공격 + 60 ( + {l})");
                    floating.ItemDescription.text = t4;
                    break;
                default:
                    break;
            }
        }

        private void SetItemType()
        {
            floating.ItemType = item.Type;
        }

        private void SetUpgradeDescription()
        {
            List<int> levelDesign = new List<int>(9) { 2, 3, 4, 5, 6, 7, 8, 9, 10, };
            Func<int,int> calcLevelDesign = delegate (int level)
            {
                if (level < 10)
                    return levelDesign[level - 1];
                return 12;
            };
            var weaponScroll = userData.Inventory.Find(x => x.Name == "Ingredient_Scroll_Weapon")?.Count ?? 0;
            var armorScroll = userData.Inventory.Find(x => x.Name == "Ingredient_Scroll_Armor")?.Count ?? 0;
            var ringScroll = userData.Inventory.Find(x => x.Name == "Ingredient_Scroll_Ring")?.Count ?? 0;
            var petScroll = userData.Inventory.Find(x => x.Name == "Ingredient_Scroll_Pet")?.Count ?? 0;
            switch (item.Type)
            {
                case UI_Lobby_Inventory_ItemIcon.TYPE.NONE:
                case UI_Lobby_Inventory_ItemIcon.TYPE.SAPPHIRE:
                case UI_Lobby_Inventory_ItemIcon.TYPE.INGREDIENT:
                    floating.ItemUpgradeDescription.text = "업그레이드 할 수 없습니다";
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.WEAPON:
                    floating.ItemUpgradeDescription.text = string.Format($"{weaponScroll}/{calcLevelDesign(item.Level)}");
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.ARMOR:
                    floating.ItemUpgradeDescription.text = string.Format($"{armorScroll}/{calcLevelDesign(item.Level)}");
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.RING:
                    floating.ItemUpgradeDescription.text = string.Format($"{ringScroll}/{calcLevelDesign(item.Level)}");
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.PET:
                    floating.ItemUpgradeDescription.text = string.Format($"{petScroll}/{calcLevelDesign(item.Level)}");
                    break;
                default:
                    break;
            }
        }

        private void SetIngredientImage()
        {
            var ingredirentImg = floating.transform.Find("UpgradeIngredient").GetChild(0).GetComponent<Image>();
            switch (item.Type)
            {
                case UI_Lobby_Inventory_ItemIcon.TYPE.NONE:
                case UI_Lobby_Inventory_ItemIcon.TYPE.SAPPHIRE:
                case UI_Lobby_Inventory_ItemIcon.TYPE.INGREDIENT:
                    Debug.Log("Item 종류만 Ingredient가 바뀝니다");
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.WEAPON:
                    ingredirentImg.sprite = DataManager.GetSpriteFromItemIndicator("Type_Icon_Weapon");
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.ARMOR:
                    ingredirentImg.sprite = DataManager.GetSpriteFromItemIndicator("Type_Icon_Armor");
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.RING:
                    ingredirentImg.sprite = DataManager.GetSpriteFromItemIndicator("Type_Icon_Ring");
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.PET:
                    ingredirentImg.sprite = DataManager.GetSpriteFromItemIndicator("Type_Icon_Pet");
                    break;
                default:
                    break;
            }
        }

        private void SetLevel()
        {
            floating.ItemLevel.text = string.Format($"레벨: {item.Level}/30");
        }

        private void SetDescription()
        {
            List<string> desc = new List<string>(4) {
                "엄청난 에너지가 담겨있어, 적이 알아채기도 전에 적을 처치합니다",
                "아주 멋진 방어구, 원거리 발사체로 인한 데미지를 줄여줍니다",
                "링의 힘을 얻게 됩니다",
                "광역 데미지를 가하는 폭탄을 계속 던집니다",
            };
            switch (item.Type)
            {
                case UI_Lobby_Inventory_ItemIcon.TYPE.NONE:
                case UI_Lobby_Inventory_ItemIcon.TYPE.SAPPHIRE:
                case UI_Lobby_Inventory_ItemIcon.TYPE.INGREDIENT:
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.WEAPON:
                    floating.ItemDescription.text = desc[0];
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.ARMOR:
                    floating.ItemDescription.text = desc[1];
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.RING:
                    floating.ItemDescription.text = desc[2];
                    break;
                case UI_Lobby_Inventory_ItemIcon.TYPE.PET:
                    floating.ItemDescription.text = desc[3];
                    break;
                default:
                    break;
            }
        }

        void SetTitle()
        {
            List<string> sTitles = new List<string> {"없음", "용감한 활", "부메랑", "팬텀 망토", "황금 흉갑", "손재주의 조끼", "보이드 로브", "뱀 반지", "곰 반지", "호랑이 반지", "레이저 박쥐", "살아있는 폭탄", "요정" };
            List<string> sComp = new List<string> { "없음", "Bow", "Boomerang", "Basic_Plate", "Rare_Plate", "Basic_Vest", "Rare_Vest", "Snake", "Bear", "Tiger", "Bet", "Ghost", "Fairy" };
            
            if (item.ItemName.Contains("Rare"))
            {
                floating.ItemNameTitle.color = new Color(0.7650781f, 0f, 1, 1);
                floating.ItemNameSubtitle.color = new Color(0.7650781f, 0f, 1, 1);
                floating.ItemNameSubtitle.text = "레어";
            }
            else
            {
                floating.ItemNameTitle.color = new Color(1f, 1f, 1, 1);
                floating.ItemNameSubtitle.color = new Color(1f, 1f, 1, 1);
                floating.ItemNameSubtitle.text = "일반";
            }

            for(int i = 0; i < sComp.Count; ++i)
            {
                if (item.ItemName.Contains(sComp[i]))
                {
                    floating.ItemNameTitle.text = sTitles[i];
                    return;
                }
            }
            Debug.Log("Error : Item Title Name");
        }

    }
    public class FloatingIngredientWindowFormetter : IData
    {
        public FloatingIngredientWindowFormetter(UI_Lobby_Equipment_FloatingIngredientWindow floatingWindow, UI_Lobby_Inventory_ItemIcon itemInfo, DATA.UserData userData)
        {
            this.floating = floatingWindow;
            this.item = itemInfo;
            this.userData = userData;
            SetItUp();
        }
        UI_Lobby_Equipment_FloatingIngredientWindow floating;
        UI_Lobby_Inventory_ItemIcon item;
        DATA.UserData userData;
        private void SetItUp()
        {
            SetTitleAndDescription();
            SetImage();
            SetCount();
        }

        private void SetCount()
        {
            floating.ItemCount.text = string.Format($"수량: {item.Count}");
        }

        private void SetImage()
        {
            //Sprite sp = UI_Lobby_Equipment_Inventory.GetSpriteByItemNameOf(item.ItemName);
            Sprite sp = DataManager.Me.ItemIndicatorScript.GetSpriteBy(item.ItemName);
            floating.ItemImage = sp;
        }

        private void SetTitleAndDescription()
        {
            string[] comp = new string[4]{ "Weapon", "Armor", "Ring", "Pet"};
            string[] title = new string[4] { "무기", "방어", "반지", "펫"};
            if (item.ItemName.Contains("Sapphire"))
            {
                floating.ItemNameTitle.text = string.Format($"사파이어");
                floating.ItemDescription.text = string.Format($"영웅을 업그레이드할 때 사용하는 신비한 에너지가 들어있습니다");
                return;
            }
            for (int i = 0; i < comp.Length; ++i)
            {
                if (item.ItemName.Contains(comp[i]))
                {
                    floating.ItemNameTitle.text = string.Format($"{title[i]} 스크롤");
                    floating.ItemDescription.text = string.Format($"{title[i]}를 업그레이드할 때 사용하는 스크롤");
                    return;
                }
            }
            Debug.Log("Ingredient Title 입력에 실패했습니다");
        }
    }
}