﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using EventSystem;
using UnityEngine.UI;

public class UI_Lobby_Equipment_DragWindow : MonoBehaviour
{
    [ReadOnly(false), SerializeField] UIBackGround UIBackGround;
    [SerializeField] RectTransform rollableWindow;
    [ReadOnly(false), SerializeField] ScrollRect scrollRect;
    Vector3 originPosOfWindow;
    float velocity = 0;
    bool isFocusedOnEquipmentWindow = false;
    bool isScrolling = false;
    private void Awake()
    {
        scrollRect = gameObject.GetComponent<ScrollRect>();
    }
    private void OnEnable()
    {
        EventSystem.EventManager.AddListener(EventSystem.EVENT_TYPE.UI_MOVEAI, UIbackground_MoveAI);
        originPosOfWindow = rollableWindow.localPosition;
    }
    private void OnDisable()
    {
        EventSystem.EventManager.RemoveListener(EventSystem.EVENT_TYPE.UI_MOVEAI, UIbackground_MoveAI);
    }
    private void Update()
    {
        if (isFocusedOnEquipmentWindow && isScrolling)
        {
            UIBackGround.enabled = false;
        }
        else
        {
            UIBackGround.enabled = true;
        }
    }
    public void LockWindow(Vector2 scrollDelta)
    {
        if (scrollDelta.x == 0.0f && scrollDelta.y == 1.0f)
            isScrolling = false;
        else if (scrollDelta.x == 1.0f && scrollDelta.y == 1.0f)
            isScrolling = false;
        else
            isScrolling = true;
    }
    void UIbackground_MoveAI(EventSystem.EventHandler e)
    {
        var evt = e as EventSystem.UI_MoveAI_Event;
        UIBackGround = evt.script;
        if (evt.WindowName.Equals(UI_MoveAI_Event.WindowType.EQUIPMENT))
        {
            isFocusedOnEquipmentWindow = true;
            //UIBackGround.MoveAIOn(10.9f);
        }
        else
        {
            isFocusedOnEquipmentWindow = false;
            UIBackGround.enabled = true;
            UIBackGround.dragged = false;
            isScrolling = false;
            SetOriginPosition();
        }
    }
    public void SetOriginPosition()
    {
        rollableWindow.localPosition = originPosOfWindow;
    }
}
