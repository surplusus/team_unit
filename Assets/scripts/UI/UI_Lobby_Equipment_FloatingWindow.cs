﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_Lobby_Equipment_FloatingWindow : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI itemNameTitle; 
    [SerializeField] TextMeshProUGUI itemNameSubtitle; 
    [SerializeField] Text itemDescription; 
    [SerializeField] TextMeshProUGUI itemLevel; 
    [SerializeField] TextMeshProUGUI itemStatDescription; 
    [SerializeField] TextMeshProUGUI itemUpgradeDescription;
    [ReadOnly(false), SerializeField] UI_Lobby_Inventory_ItemIcon.TYPE itemType;
    [SerializeField] GameObject selectedItem;
    List<Sprite> upgradeIngredients = new List<Sprite>(4);
    public UI_Lobby_Inventory_ItemIcon.TYPE ItemType { set => itemType = value; get => itemType; }
    public TextMeshProUGUI ItemNameTitle { get => itemNameTitle; set => itemNameTitle = value; }
    public TextMeshProUGUI ItemNameSubtitle { get => itemNameSubtitle; set => itemNameSubtitle = value; }
    public Text ItemDescription { get => itemDescription; }
    public TextMeshProUGUI ItemLevel { get => itemLevel; }
    public TextMeshProUGUI ItemStatDescription { get => itemStatDescription; }
    public TextMeshProUGUI ItemUpgradeDescription { get => itemUpgradeDescription; }
    public GameObject SelectedItem { get => selectedItem; set => selectedItem = value; }
}
