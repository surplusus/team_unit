﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UI_Lobby_Equipment_FloatingIngredientWindow : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI itemNameTitle;
    [SerializeField] Text itemDescription;
    [SerializeField] TextMeshProUGUI itemCount;
    [ReadOnly(false), SerializeField] UI_Lobby_Inventory_ItemIcon.TYPE itemType;
    [SerializeField] Image itemImage;
    [ReadOnly(false), SerializeField] Sprite itemSprite;
    List<Sprite> upgradeIngredients = new List<Sprite>(4);
    public UI_Lobby_Inventory_ItemIcon.TYPE ItemType { set => itemType = value; }
    public Sprite ItemImage { 
        set
        {
            itemSprite = value;
            itemImage.sprite = itemSprite;
        }
    }
    public TextMeshProUGUI ItemNameTitle { get => itemNameTitle; set => itemNameTitle = value; }
    public Text ItemDescription { get => itemDescription; }
    public TextMeshProUGUI ItemCount { get => itemCount; set => itemCount = value; }
}
