﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;
using UnityEditor;

public class SceneLinker : MonoBehaviour
{
    [SerializeField] Scene sceneTEST;
    [SerializeField] AssetReference sceneAsset;
    [SerializeField] Scene sceneLobby;
    [SerializeField] Object[] scenes;
    void Start()
    {
        //LoadSceneAdditive();
        LoadChildScenes();
    }
    void LoadSceneAdditive()
    {
        SceneManager.LoadScene("1LobbyScene", LoadSceneMode.Additive);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Scene2"));

    }
    void LoadChildScenes()
    {
        SceneManager.LoadSceneAsync(1).completed += SceneLinker_completed;
        var b = sceneAsset.LoadSceneAsync(LoadSceneMode.Additive);
        
    }

    private void SceneLinker_completed(AsyncOperation obj)
    {
        throw new System.NotImplementedException();
    }
}