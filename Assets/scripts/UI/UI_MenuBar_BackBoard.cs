﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UI_MenuBar_BackBoard : MonoBehaviour
{
    public List<GameObject> BackButtons;
    public GameObject SelectedButton;
    [SerializeField] UI_MenuBar_Center LeftCenter;
    [SerializeField] UI_MenuBar_Center RightCenter;
    [SerializeField] UIBackGround UIBackGround;
    List<bool> IsButtonActive;
    float[] posSelected = new float[5] {-212, -106,0,106,212, };
    Vector2 firstPos;
    bool IsDirty = true;
    int selectedIndex = 2;
    private void Start()
    {
        firstPos = SelectedButton.GetComponent<RectTransform>().position;
        ToggleActive(selectedIndex);
        LeftCenter = BackButtons[2].GetComponent<UI_MenuBar_Center>();
        RightCenter = BackButtons[3].GetComponent<UI_MenuBar_Center>();
    }
    private void Update()
    {
        if (IsDirty)
        {
            MoveSelectButton(selectedIndex);
        }
    }
    public void SelectMenuBarOf(int index)
    {
        IsDirty = true;
        MoveSelectButton(index);
    }
    void ToggleActive(int index)
    {   // 0 부터 시작
        IsButtonActive = Enumerable.Repeat<bool>(true, 6).ToList<bool>();
        IsButtonActive[index%6] = false;
        IsButtonActive[(index+1)%6] = false;
        float[] mapPos = new float[5] { 21.8f, 10.9f, 0f, -10.9f, -21.8f };
        for (int i = 0; i< 6; ++i)
        {
            BackButtons[i].SetActive(IsButtonActive[i]);
        }
        UIBackGround.MoveAIOn(mapPos[index]);
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.UI_MOVEAI, new EventSystem.UI_MoveAI_Event(UIBackGround, mapPos[index]));
    }
    void MoveSelectButton(int index)
    {
        RectTransform selected = SelectedButton.GetComponent<RectTransform>();
        Vector2 origin = selected.anchoredPosition;
        Vector2 target = new Vector2(posSelected[index], 0);
        selected.anchoredPosition = Vector2.Lerp(origin, target, 0.2f);
        if (Vector3.Distance(selected.position, target) < 0.1f)
        {
            selected.anchoredPosition = target;
            IsDirty = false;
        }
    }
    void PushLeftSideButton()
    {
        if (selectedIndex==0)
        {
            LeftCenter.SayIAmNotWorld();
            RightCenter.SayIAmWorld();
        }
        if (selectedIndex==1)
        {
            LeftCenter.SayIAmNothing();
            RightCenter.SayIAmWorld();
        }
    }
    void PushRightSideButton()
    {
        if (selectedIndex == 3)
        {
            LeftCenter.SayIAmWorld();
            RightCenter.SayIAmNothing();
        }
        if (selectedIndex == 4)
        {
            LeftCenter.SayIAmWorld();
            RightCenter.SayIAmNotWorld();
        }
    }
    public void SelectStoreButton()
    {
        if (selectedIndex.Equals(0))
            return;
        selectedIndex = 0;
        ToggleActive(0);
        // selectedButton에서 child를 바꿔주는 함수
        SelectedButton.GetComponent<UI_MenuBar_Selected>().SetChildAs(0);
        // Center Icon을 바꿔준다
        PushLeftSideButton();
        isStartFromEndSide = true;
        IsDirty = true;
    }
    public void SelectInventoryButton()
    {
        if (selectedIndex.Equals(1))
            return;
        selectedIndex = 1;
        ToggleActive(1);
        SelectedButton.GetComponent<UI_MenuBar_Selected>().SetChildAs(1);
        PushLeftSideButton();
        IsDirty = true;
    }
    bool isStartFromEndSide = false;
    public void SelectWorldButton(string LorR)
    {
        var invenCenterButtonActive = LeftCenter.MyNonWorld.activeSelf;
        var talentCenterButtonActive = RightCenter.MyNonWorld.activeSelf;
        if (LorR.Equals("Left") && invenCenterButtonActive)
        {
            SelectInventoryButton();
        }
        else if (LorR.Equals("Right") && talentCenterButtonActive)
        {
            SelectTalentButton();
        }
        else
        {
            selectedIndex = 2;
            ToggleActive(2);
            SelectedButton.GetComponent<UI_MenuBar_Selected>().SetChildAs(2);
        }

        IsDirty = true;
    }
    public void SelectTalentButton()
    {
        if (selectedIndex.Equals(3))
            return;
        selectedIndex = 3;
        ToggleActive(3);
        SelectedButton.GetComponent<UI_MenuBar_Selected>().SetChildAs(3);
        PushRightSideButton();
        IsDirty = true;
    }
    public void SelectOptionButton()
    {
        if (selectedIndex.Equals(4))
            return;
        selectedIndex = 4;
        ToggleActive(4);
        SelectedButton.GetComponent<UI_MenuBar_Selected>().SetChildAs(4);
        PushRightSideButton();
        isStartFromEndSide = true;
        IsDirty = true;
    }

}
