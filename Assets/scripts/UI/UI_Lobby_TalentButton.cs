﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UI_Lobby_TalentButton : MonoBehaviour
{
    [SerializeField] int index;
    [SerializeField] DATA.TalentData talentData;
    [SerializeField] GameObject seal;
    [SerializeField] Text levelValueText;
    [SerializeField] Transform explanation;
    TextMeshProUGUI expText;
    GameObject buttonNameText;
    void Start()
    {
        SetUpTextValues();
        {   //  TalentData에서 설명을 뽑아서 Explanation안에 넣어줌
            expText = explanation.Find("Explanation").GetComponent<TextMeshProUGUI>();
            expText.text = talentData.ShowExplanation(index);   //  아래 함수랑 다른 거임
            TurnSetActiveFalse();
        }
    }
    private void OnEnable()
    {
        SetUpTextValues();
    }
    void SetUpTextValues()
    {
        buttonNameText = transform.Find("Name").gameObject;
        var talentLevel = talentData.Talents[index].Level;
        if (talentLevel.Equals(0))
        {
            seal.SetActive(true);
            buttonNameText.SetActive(false);
        }
        else
        {
            seal.SetActive(false);
            buttonNameText.SetActive(true);
        }
        levelValueText.text = talentLevel.ToString();
    }
    public void ShowExplanation()
    {   // talent 버튼을 누르면 Explanation을 띄우기 위해서 OnClick 이벤트에서 쓰임
        explanation.gameObject.SetActive(true);
        Invoke("TurnSetActiveFalse", 0.5f);
    }
    void TurnSetActiveFalse()
    {
        explanation.gameObject.SetActive(false);
    }
}
