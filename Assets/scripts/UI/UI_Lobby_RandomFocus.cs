﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class TalentLevelUpEvent : UnityEvent<GameObject, int> { }

public class UI_Lobby_RandomFocus : MonoBehaviour
{
    public TalentLevelUpEvent AfterLevelUpEvent { get; set; } = new TalentLevelUpEvent();
    [HideInInspector] public int SelectedTalent;
    [SerializeField] List<RectTransform> buttons;
    [SerializeField] GameObject randomFocus;
    [SerializeField] DATA.TalentData data;
    RectTransform tr_RandomFocus;
    Animation anim_RandomFocus;
    Vector3 originalPosOfRandomFocus;
    public bool IsPlayingTalentLevelUp { get; private set; }
    private void Awake()
    {
        tr_RandomFocus = randomFocus.GetComponent<RectTransform>();
        anim_RandomFocus = randomFocus.GetComponent<Animation>();
    }
    private void Start()
    {
        originalPosOfRandomFocus = tr_RandomFocus.position;
        randomFocus.SetActive(false);
    }
    public void RunLevelUp()
    {
        if (IsPlayingTalentLevelUp)
            return;

        float interval = 0.1f;
        StartCoroutine(MoveRandomFocus(interval));
    }
    void CoverOverButtonOf(int index)
    {
        tr_RandomFocus.position = buttons[index].position;
    }
    void PlayAnimOnButtonOf(int index)
    {
        tr_RandomFocus.position = buttons[index].position;
        anim_RandomFocus.Play();
    }
    IEnumerator MoveRandomFocus(float interval)
    {
        IsPlayingTalentLevelUp = true;
        randomFocus.SetActive(true);
        int cnt = 15;
        while (cnt-- > 0)
        {
            yield return new WaitForSeconds(interval);
            int index = Random.Range(0, 8);
            CoverOverButtonOf(index);
        }

        if (data.Talents[8].Level.Equals(0))
        {
            SelectedTalent = 8;
        }
        else
        {
            SelectedTalent = Random.Range(0, 7);
        }
        PlayAnimOnButtonOf(SelectedTalent); // random select 애니메니션 재생
        yield return new WaitForSeconds(3f);
        AfterLevelUpEvent?.Invoke(buttons[SelectedTalent].gameObject, SelectedTalent); // TalentUpgrade 캔버스 활성화
        IsPlayingTalentLevelUp = false;
        ResetRandomFocusIcon();
    }
    void ResetRandomFocusIcon()
    {
        tr_RandomFocus.position = originalPosOfRandomFocus;
    }
}
