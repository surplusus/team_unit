﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using EventSystem;

public class UI_Lobby_Equipment_EquipedSlot : MonoBehaviour

{
	public UnityEvent OnClickEvent;
	[SerializeField] GameObject tapEquipBtn;
	[SerializeField] GameObject slotOfSelectItem;
	[ReadOnly(false), SerializeField] GameObject selectedItem;
	[ReadOnly(false), SerializeField] bool isEquiped = false;
	[ReadOnly(false), SerializeField] bool isTypeMatched = false;
	public enum Type : int
	{
		NONE=-1, WEAPON, ARMOR, RING1,RING2,PET1,PET2,
	}
	public Type SlotType { get => type; }
	[SerializeField] Type type = Type.NONE;

	private void OnEnable()
	{
		EventManager.AddListener(EVENT_TYPE.EPUIPMENT_OPENFLOATINGWINDOW, SaveSelectedItem);
	}
	void SaveSelectedItem(EventHandler e)
	{
		var evt = e as LobbyEquipmentFloatingWindowEvent;
		var item = evt.script;
		selectedItem = item.gameObject;
		isTypeMatched = CompareItemType(item);
	}
	public int GetBuffHP()
	{
		return 10;
	}
	public int GetBuffAttack()
	{
		return 100;
	}
	public void ShowEquipedItemSlot()
	{
		if (isTypeMatched)
			tapEquipBtn.SetActive(true);
	}
	public void EquipItem()
	{
		if (slotOfSelectItem.transform.childCount != 0)
			Destroy(slotOfSelectItem.transform.GetChild(0).gameObject);
		var obj = Instantiate<GameObject>(selectedItem, slotOfSelectItem.transform);
		obj.transform.localPosition = Vector3.zero;
		isEquiped = true;
	}
	bool CompareItemType(UI_Lobby_Inventory_ItemIcon item)
	{
		UI_Lobby_Inventory_ItemIcon.TYPE comp = UI_Lobby_Inventory_ItemIcon.TYPE.NONE;
		switch (type)
		{
			case Type.NONE:
				comp = UI_Lobby_Inventory_ItemIcon.TYPE.NONE;
				break;
			case Type.WEAPON:
				comp = UI_Lobby_Inventory_ItemIcon.TYPE.WEAPON;
				break;
			case Type.ARMOR:
				comp = UI_Lobby_Inventory_ItemIcon.TYPE.ARMOR;
				break;
			case Type.RING1:
				comp = UI_Lobby_Inventory_ItemIcon.TYPE.RING;
				break;
			case Type.RING2:
				comp = UI_Lobby_Inventory_ItemIcon.TYPE.RING;
				break;
			case Type.PET1:
				comp = UI_Lobby_Inventory_ItemIcon.TYPE.PET;
				break;
			case Type.PET2:
				comp = UI_Lobby_Inventory_ItemIcon.TYPE.PET;
				break;
		}
		if (comp.Equals(item.Type))
			return true;
		return false;
	}
}
