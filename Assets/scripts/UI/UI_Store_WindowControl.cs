﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UI_Store_WindowControl : MonoBehaviour
{
    public UnityEvent BasicBoxButtonEvent = new UnityEvent();
    public UnityEvent RareBoxButtonEvent = new UnityEvent();

    public void BasicBoxButtonOnClick()
    {
        BasicBoxButtonEvent?.Invoke();
    }
    public void RareBoxButtonOnClick()
    {
        RareBoxButtonEvent?.Invoke();
    }
}
