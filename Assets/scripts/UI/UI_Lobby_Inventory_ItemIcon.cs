﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Lobby_Inventory_ItemIcon : MonoBehaviour
{
	public enum TYPE
	{
		NONE=0,SAPPHIRE, INGREDIENT, WEAPON, ARMOR, RING, PET,
	}
	public string ItemName;
	public TYPE Type = TYPE.SAPPHIRE;
	public int Level = 1;
	public int Count = 1;
	public Sprite itemImage;
	public Sprite typeImage;
	Image child0 = null;
	Image child1 = null;
	Text child2 = null;
	private void Awake()
	{
		transform.GetChild(0).GetChild(0).TryGetComponent<Image>(out child0);
		transform.GetChild(0).GetChild(1).TryGetComponent<Image>(out child1);
		transform.GetChild(0).GetChild(2).TryGetComponent<Text>(out child2);
	}
	public void InitValues(Sprite item, Sprite type, TYPE eType, int count, int level)
	{
		itemImage = item;
		typeImage = type;
		this.Count = count;
		this.Level = level;
		Type = eType;
		switch (Type)
		{
			case TYPE.SAPPHIRE:
			case TYPE.INGREDIENT:
				child1.sprite = itemImage;
				child2.text = string.Format($"x{Count}");
				break;
			case TYPE.WEAPON:
			case TYPE.ARMOR:
			case TYPE.RING:
			case TYPE.PET:
				child0.sprite = itemImage;
				child1.sprite = typeImage;
				child2.text = string.Format($"Lv.{Level}");
				break;
		}
	}
	public void PlayFloatingWindowOnClickButton()
	{
		EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.EPUIPMENT_OPENFLOATINGWINDOW
			, new EventSystem.LobbyEquipmentFloatingWindowEvent(this));
	}
}
