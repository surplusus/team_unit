﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DATA;
using EventSystem;

public class LobbyTopMenu : MonoBehaviour
{
    [SerializeField] GameObject slot;
    [SerializeField] DATA.UserData userData;
    private void OnEnable()
    {
        EventManager.AddListener(EVENT_TYPE.DATA_SAVESO, delegate (EventHandler e) { CoinFunc(); });
        GameManager.Me.CheatEvent += MenuBarCheat;
    }
    bool isCheatActivated = false;
    private void MenuBarCheat()
    {
        if (!isCheatActivated)
        {
            userData.Coin += 5000;
            userData.Gem += 10;
            userData.Exp.ExpPoint += 10;
            userData.ActionPoint.PlusRemainder(25);
            UpdateTexts();
            isCheatActivated = true;
        }
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(EVENT_TYPE.DATA_SAVESO, delegate (EventHandler e) { CoinFunc(); });
    }
    private void Start()
    {
        if (slot.name == "ActionPoint")
        {
            ActionPoint point = userData.ActionPoint;
            point.SetStandartTime();
            //point.Remainder = 0;
            InvokeRepeating("ActionPointFunc", 1.0f, 1.0f);
        }
        UpdateTexts();
    }
    void UpdateTexts()
    {
        switch (slot.name)
        {
            case "Level":
                LevelFunc();
                break;
            case "ActionPoint":
                ActionPointFunc();
                break;
            case "Coin":
                CoinFunc();
                break;
            case "Gem":
                GemFunc();
                break;
        }
    }
    void LevelFunc()
    {
        var bar = slot.transform.Find("Bar");
        var numShadow = slot.transform.Find("Image").Find("TextShadow");
        var num = slot.transform.Find("Image").Find("Text");
        bar.GetComponent<Image>().fillAmount = userData.Exp.GetExpRate();
        numShadow.GetComponent<Text>().text = userData.Level.ToString();
        num.GetComponent<Text>().text = userData.Level.ToString();
    }
    void ActionPointFunc()
    {
        var bar = slot.transform.Find("Bar");
        var numShadow = slot.transform.Find("TextShadow");
        var num = slot.transform.Find("Text");
        ActionPoint point = userData.ActionPoint;
        point.EarnPoint();
        bar.GetComponent<Image>().fillAmount = userData.ActionPoint.ActionPointRate();
        num.GetComponent<Text>().text = string.Format($"{point.Stamina} / {point.Limit}");
        numShadow.GetComponent<Text>().text = string.Format($"{point.Stamina} / {point.Limit}");
    }
    void CoinFunc()
    {
        var num = slot.transform.Find("Text");
        num.GetComponent<Text>().text = userData.Coin.ToString();
    }
    void GemFunc()
    {
        var num = slot.transform.Find("Text");
        num.GetComponent<Text>().text = userData.Gem.ToString();
    }
    
}
