﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_MenuBar_Selected : MonoBehaviour
{
    [SerializeField] List<GameObject> MyChildren;
    void Start()
    {
        for (int i = 0; i < 5; ++i)
            MyChildren.Add(transform.GetChild(i).gameObject);

        SetChildAs(2);
    }
    public void SetChildAs(int chlidTransformIndex)
    {
        for (int i = 0; i < 5; ++i)
            MyChildren[i].SetActive(false);
        MyChildren[chlidTransformIndex].SetActive(true);
    }
}
