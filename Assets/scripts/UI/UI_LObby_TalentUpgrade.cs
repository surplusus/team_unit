﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_Lobby_TalentUpgrade : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI upgradeText;
    [SerializeField] TextMeshProUGUI upgradeCoastText;
    [SerializeField] DATA.TalentData data;
    [SerializeField] Animation randomFocusAnim;
    [ReadOnly(false), SerializeField] int totalCount;
    [ReadOnly(false), SerializeField] int coast; 
    List<int> levelDesign = new List<int>{300,400,600,800,1000,1200,1400,1700,2000,2500};
    private void OnEnable()
    {
        InitiateTalentTexts();
    }
    public void InitiateTalentTexts()
    {
        upgradeText.text = ShowTatalLevelText();
        upgradeCoastText.text = GetTextByLevelDisign();
    }
    string ShowTatalLevelText()
    {
        totalCount = 0;
        for (int i = 0; i < 9; ++i)
        {
            totalCount += data.Talents[i].Level;
        }
        return string.Format($"{totalCount} 회 업그레이드");
    }
    string GetTextByLevelDisign()
    {
        coast = totalCount < 10 ? levelDesign[totalCount] : levelDesign[9] + (totalCount - 10) * 500;
        return string.Format($"x{coast}");
    }
    public void SpendCoinForUpgradeTalent()
    {
        DataManager.Me.scriptableObjects.userData.Coin -= coast;
        DataManager.Me.SaveAllSOdata();
    }
}
