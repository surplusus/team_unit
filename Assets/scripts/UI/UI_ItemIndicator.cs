﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ItemIndicator : MonoBehaviour, DATA.IData
{
    [SerializeField] List<Sprite> sprites;
    [SerializeField] List<string> spriteNames; 
    public List<Sprite> Sprites { get => sprites; }
    public List<string> SpriteNames { get => spriteNames; }
    Dictionary<string, int> spriteIdxChart;
    List<Image> childImages;
    private void Awake()
    {
        var imgs = transform.GetComponentsInChildren<Image>();
        childImages = new List<Image>(imgs);
        sprites = new List<Sprite>();
        spriteNames = new List<string>();
        spriteIdxChart = new Dictionary<string, int>();
        for (int i = 0; i < childImages.Count; ++i)
        {
            sprites.Add(childImages[i].sprite);
            string name = childImages[i].name;
            spriteNames.Add(name);
            spriteIdxChart.Add(name, i);
        }
        var data = this;
        DataManager.SaveData<UI_ItemIndicator>(ref data);
    }
    public Sprite GetSpriteBy(string name)
    {
        int index = 0;
        if (spriteIdxChart.TryGetValue(name, out index))
            return sprites[index];
        print("그런 이름의 sprite가 없습니다");
        return null;
    }
    public Sprite GetSpriteBy(int index)
    {
        if (index >= 0 && index < sprites.Count && !sprites.Count.Equals(0))
            return sprites[index];
        print("그런 index의 sprite가 없습니다");
        return null;
    }
}
