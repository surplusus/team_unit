﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DATA;

namespace Item
{
    public interface IItem
    {
        string Name { get;}
        DATA.ItemType ItemType { get; }
        ItemPrebs Prebs { get; }
        string Explanation { get; set; }
        bool IsEquiped { get; set; }
        Player InGamePlayer { get; set; }
        void Introduce();
        void Buff(ref Status playerStatus);
        void Equip();
        void Unequip();
        void LevelUp();
    }

}
