﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using Cinemachine;
using EventSystem;
using UnityEngine.UI;

public class RoleScheduler : MonoBehaviour
{
    #region PublicMembers
    public Player Player { set => player = value; }
    public GameObject gameResult;
    public int StageNum = -1;
    public int ChapterNum = 1;
    public int MonsterNum = 0;
    public int CoinNum = 0;
    #endregion
    [ReadOnly(false), SerializeField] Player player;
    [SerializeField] List<Transform> maps = new List<Transform>();
    [SerializeField, ReadOnly(false)] PoolOfFoodController cock;
    [SerializeField] DATA.Condition originCondition;
    [ReadOnly(false), SerializeField] User user;
    List<string> getTypeOfItem = new List<string>();

    private void OnEnable()
    {
        EventManager.AddListener(EVENT_TYPE.MONSTERCOUNT, MonsterCountDown);
        EventManager.AddListener(EVENT_TYPE.SEARCHPLAYER, SetPlayer);
        EventManager.AddListener(EVENT_TYPE.STAGEEND, CalculateGetTypeOfItem);
        PlayerAnimEventController.OnDieAnimEvent += ()=>Invoke("GameResult", 2.0f);
    }
    private void OnDisable()
    {
        EventManager.RemoveListener(EVENT_TYPE.MONSTERCOUNT, MonsterCountDown);
        EventManager.RemoveListener(EVENT_TYPE.SEARCHPLAYER, SetPlayer);
        EventManager.RemoveListener(EVENT_TYPE.STAGEEND, CalculateGetTypeOfItem);
        PlayerAnimEventController.OnDieAnimEvent = null;
    }
    private void Awake()
    {
        //cock = GameObject.FindObjectOfType<PoolOfFoodController>();
        gameResult = GameObject.FindGameObjectWithTag("GameResult");
        gameResult.SetActive(false);
    }
    private void Start()
    {
        user = GameManager.Me.USER;
        cock = GameObject.FindObjectOfType<PoolOfFoodController>();
        List<int> list = new List<int>();
        Transform allMap = GameObject.FindGameObjectWithTag("Maps").transform;
        maps.Clear();
        for (int i = 0; i < allMap.childCount; i++)
            maps.Add(allMap.GetChild(i));

        StageNum = -1;
        StageRun();
        ResetCondition();
    }
    #region Pacade
    public void StageRun()
    {
        StageNum++;
        if (GameObject.Find("InputArea") == null)
            InputManager.CreateInputArea();

        if (StageNum == 0)
            MakePlayer();
        else if (StageNum > 20)
            GameResult();
        else
            ResetPlayerPosition();

        MakeMap(StageNum);
    }
    #endregion
    void MakeMap(int index)
    {
        if (index > 0)
            maps[index - 1].gameObject.SetActive(false);

        maps[index].gameObject.SetActive(true);
        SetMonsterNum(index);
    }
    void MakePlayer()
    {
        // 앞서 player가 있으면 지운다
        var nativeMap = GameObject.FindWithTag("Player");
        if (nativeMap != null)
            Destroy(nativeMap);

        player = user.CreatePlayerByUserData(new Vector3(0, 0, 0));
        PlayerAnimEventController.OnDieAnimEvent += () => Destroy(player.transform.parent.gameObject);
    }
    void ResetCondition()
    {
        originCondition.IsPiercing = false;
        originCondition.IsIce = false;
        originCondition.IsFire = false;
        originCondition.IsElectric = false;
        originCondition.IsPoison = false;
        originCondition.IsWallReflection = false;
        originCondition.IsForwardArrow = false;
        originCondition.IsRearArrow = false;
        originCondition.ReflectionRate = 0;
        originCondition.RicochetRate = 0;
        originCondition.BoomerangRate = 0;
}
    void ResetPlayerPosition()
    {
        player.transform.position = new Vector3(0, 0, 0);
    }
    void SetMonsterNum(int index)
    {
        MonsterNum = maps[index].GetChild(0).transform.childCount;
    }
    void MonsterCountDown(EventHandler e)
    {
        MonsterNum--;
    }
    void SetPlayer(EventHandler e)
    {
        var evt = e as EventSystem.SearchPlayerEvent;
        Player = evt.player;
    }
    void CalculateGetTypeOfItem(EventHandler e)
    {
        var evt = e as StageEndEvent;
        if (player == null) return;

        player.Status.Exp.ExpPoint += evt.pointOfCoin;
        CoinNum += evt.pointOfCoin;
        //Debug.Log("ExpPoint : " + player.Status.Exp.ExpPoint);
        //Debug.Log("evtPointOfCoin : " + evt.pointOfCoin);

        player.Status.HealthPoint += evt.pointOfHeart * 50;
        if (player.Status.HealthPoint > player.Status.MaxHealthPoint) player.Status.HealthPoint = player.Status.MaxHealthPoint;

        for (int i = 0; i < evt.getTypeOfItem.Count; i++)
        {
            getTypeOfItem.Add(evt.getTypeOfItem[i]);
        }
        Invoke("SkillSelect", 1.0f);
    }
    public void SkillSelect()
    {
        if (player.Status.Exp.LevelUp())
            GameObject.FindGameObjectWithTag("Canvas").GetComponent<UISkillUpCtrl>().EnableUISkillUp();
    }
    public void GameResult()
    {   // OnEnable에서 PlayerAnimEventController.OnDieAnimEvent 에 걸려있음
        gameResult.SetActive(true);
        UIGameResult result = gameResult.GetComponent<UIGameResult>();
        if (StageNum <= 20)
        {
            result.stageNum.text = StageNum.ToString();
            result.chapterNum.text = string.Format($"챕터 {ChapterNum}");
            result.coinNum.text = string.Format($"x{CoinNum -1}");
        }
        else
        {
            result.TurnGreen();
            result.stageNum.text = string.Format("끝");
            result.chapterNum.text = string.Format($"챕터 {ChapterNum}");
            result.coinNum.text = string.Format($"x{CoinNum -1}");
        }
        Time.timeScale = 0;
    }
}
