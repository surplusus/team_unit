﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FlotingText : MonoBehaviour
{
    Text damageText;
    Vector3 pos;
    new Camera camera;
    void Start()
    {
        damageText = this.GetComponent<Text>();
        this.gameObject.SetActive(false);
        camera = GameObject.FindGameObjectWithTag("SubCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartHitText(int damage, Vector3 position)
    {
        pos = position;
        damageText.text = string.Format($"-{damage}");
        StartCoroutine("HitText");
    }

    IEnumerator HitText()
    {
        float maxTime = 0.5f;
        float curTime = 0.0f;
        float size = 1.0f;
        while (curTime < maxTime)
        {
            this.transform.position = camera.WorldToScreenPoint(pos);
            this.transform.localScale = new Vector3(size, size, size);
            yield return new WaitForSeconds(0.01f);
            curTime += 0.01f;

            if (curTime < maxTime/3)
            {
                size += 0.03f;
                pos.y += 0.05f;
            }
            else if (curTime > maxTime - maxTime/3)
            {
                size -= 0.05f;
                pos.y -= 0.03f;
            }
        }
        this.gameObject.SetActive(false);
    }
}
