﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abillity : MonoBehaviour
{
    // Start is called before the first frame update
    BulletBuilder bulletBuilder;

    public bool IsPiercing;
    public bool IsIce;
    public bool IsFire;
    public bool IsElectric;
    public bool IsPoison;

    public bool IsWallReflection;
    public bool IsForwardArrow;
    public bool IsRearArrow;

    public float ReflectionRate;
    public float RicochetRate;
    public float BoomerangRate;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnEnable()
    {
        EventSystem.EventManager.AddListener(EventSystem.EVENT_TYPE.SELECTABILLITY, HashingAbillity);
    }

    void HashingAbillity(EventSystem.EventHandler e)
    {
        if(bulletBuilder == null) bulletBuilder = GameObject.FindGameObjectWithTag("Weapon").GetComponent<BulletBuilder>();

        var evt = e as EventSystem.SelectAbillityEvent;

        switch(evt.spriteName)
        {
            case 1000001:   // 전방화살+1
                Debug.Log("전방화살");
                bulletBuilder.Condition.IsForwardArrow = true;
                IsForwardArrow = true;
                break;
            case 1000003:   // 벽반사
                Debug.Log("벽반사");
                bulletBuilder.Condition.IsWallReflection = true;
                break;
            case 1000004:   // 후방화살+1
                Debug.Log("후방화살");
                bulletBuilder.Condition.IsRearArrow = true;
                break;
        }
    }
    void Affect(Player player)
    {

    }
    void Affect(IMonster monster)
    {

    }

    void Affect(BulletBuilder bulletBuilder)
    {
    }
}
