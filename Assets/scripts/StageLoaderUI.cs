﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class StageLoaderUI : MonoBehaviour
{
    [SerializeField] bool IsDebug;

    [SerializeField] Image progressBar;
    [SerializeField] Text text;

    AsyncOperation async_operation;
    void Start()
    {
        IsDebug = UIManager.Me.IsDebug;
        StartCoroutine(LoadingNextScene("3InGameScene"));
    }
    IEnumerator LoadingNextScene(string sceneName)
    {
        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();

        async_operation = SceneManager.LoadSceneAsync(sceneName);
        async_operation.allowSceneActivation = false;
        while (async_operation.progress >= 0.9f)
        {
            // async_operation.isDone
            yield return true;
        }
        //async_operation.allowSceneActivation = true;
    }
    void FixedUpdate()
    {
        float DebugEndTime = 3.0f;
        int MaxDotNumber = 5;
        Loading(IsDebug, DebugEndTime, MaxDotNumber);
    }
    float debugTime = 0.0f;
    string str = "세계 진입 중";
    void Loading(bool isDebug, float overTime, int maxDotNumber)
    {
        if (isDebug)
        {
            UpdatePrograssBar(overTime, maxDotNumber);
        }
        else
        {
            float progress = async_operation.progress;
            int per = (int)progress * 100;
            text.text = string.Format($"{per} %");

            if (async_operation.progress >= 0.9f)
            {
                async_operation.allowSceneActivation = true;
                progressBar.fillAmount = 1;
                text.text = "100%";
            }
        }
    }
    void UpdatePrograssBar(float overTime, int maxDotNumber)
    {
        debugTime += Time.deltaTime;
        int count = 1;
        float progress = debugTime / overTime;
        int per = (int)(progress * 100);
        string str = "세계 진입 중";

        count = (per / 10) % maxDotNumber;
        while (count-- > 0)
            str += ".";

        text.text = string.Format($"{str}");
        progressBar.fillAmount = (float)(per % 15) / 10f;
        if (debugTime >= overTime)
        {
            async_operation.allowSceneActivation = true;
        }
    }
}
