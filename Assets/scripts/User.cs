﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DATA;
using UnityEngine.AddressableAssets;
using System;

public class User : MonoBehaviour
{
    #region SetUp 함수에서 DataManager가 넣어주는 ScriptableObejct
    [SerializeField] GameObject playerPreb; 
    [ReadOnly(false), SerializeField] UserData userDataSO = null;
    [ReadOnly(false), SerializeField] TalentData talentSO = null;
    [ReadOnly(false), SerializeField] Status statusSO = null;
    public UserData UserData { set => userDataSO = value; get { return userDataSO; } }
    public TalentData Talent { set => talentSO = value; get { return talentSO; } }
    public Status Status { set => statusSO = value; get { return statusSO; } }
    #endregion
    bool IsSOSetUp = false;
    Player playerComponent = null;
    private void Awake()
    {
        StartCoroutine(SetUpSOs());
    }
    IEnumerator SetUpSOs()
    {
        yield return new WaitUntil(() => DataManager.IsAllAssetsLoaded);
        UserData = DataManager.Me.scriptableObjects.userData;
        Talent = DataManager.Me.scriptableObjects.talent;
        Status = DataManager.Me.scriptableObjects.playerStatus;
        IsSOSetUp = true;
    }
    void PutDataIntoPlayer(ref Player player)
    {
        player.UserData = this.userDataSO;
        player.Talent = this.talentSO;
        //player.Status = new Status(this.statusSO);
        var newStatus = new Status(this.statusSO);
        CalcPlayerStatusValue(ref newStatus);
        player.Status = newStatus;
    }

    private void CalcPlayerStatusValue(ref Status status)
    {
        
    }

    public Player CreatePlayerByUserData(Vector3 startPos)
    {
        StartCoroutine(CreatePlayer(startPos));
        return playerComponent;
    }
    IEnumerator CreatePlayer(Vector3 startPos)
    {
        yield return new WaitUntil(() => IsSOSetUp);
        var p = Instantiate<GameObject>(playerPreb, startPos, Quaternion.identity);
        playerComponent = p.GetComponentInChildren<Player>();
        PutDataIntoPlayer(ref playerComponent);
        EventSystem.EventManager.InvokeEvent(EventSystem.EVENT_TYPE.SEARCHPLAYER, new EventSystem.SearchPlayerEvent(playerComponent));
    }
}
