﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteBG : MonoBehaviour
{
    RoleScheduler roleScheduler;
    new Camera camera;
    private void Start()
    {
        
        camera = GameObject.FindGameObjectWithTag("SubCamera").GetComponent<Camera>();
    }

    public void StartNextStageWhite()
    {
        if(roleScheduler == null)
            roleScheduler = GameObject.FindGameObjectWithTag("RoleScheduler").GetComponent<RoleScheduler>();

        StartCoroutine("NextStageWhite");
    }

    public void StartReturnRobby()
    {
        StartCoroutine(ReturnRobby());
    }
    public IEnumerator NextStageWhite()
    {
        int size = 1;
        bool IsSW = false;
        while (size >= 1)
        {
            this.transform.localScale = new Vector3(size, size, size);

            if (IsSW)
                size -= 2;
            else
                size++;

            if (size > 70)
            {
                IsSW = true;
                roleScheduler.StageRun();
                this.transform.position = camera.WorldToScreenPoint(camera.transform.position);
            }

            yield return new WaitForSeconds(0.01f);
        }
        this.gameObject.SetActive(false);
    }

    public IEnumerator ReturnRobby()
    {
        int size = 1;

        while (size <= 70)
        {
            this.transform.localScale = new Vector3(size, size, size);
            size += 2;

            yield return new WaitForSeconds(0.01f);
        }

        GameManager.MoveToSceneNameOf("lobby");
    }
}
