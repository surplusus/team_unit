﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace EventSystem
{
    public sealed class EventManager : MonoBehaviour
    {
        #region Singleton
        static private EventManager me = null;
        static bool IsInstanceDeleted = false;
        static public EventManager Me
        {
            get
            {
                if (me == null)
                {
                    me = FindObjectOfType<EventManager>();
                    if (me == null)
                    {
                        GameObject newEventMgr = new GameObject("EventManager");
                        me = newEventMgr.AddComponent<EventManager>();
                        DontDestroyOnLoad(newEventMgr);
                    }
                }
                return me;
            }
        }
        private void OnEnable()
        {
            IsInstanceDeleted = false;
        }
        private void OnDisable()
        {
            listeners.Clear();
            listeners = null;
            me = null;
        }
        private void OnDestroy()
        {
            IsInstanceDeleted = true;
        }
        #endregion
        public delegate void OnEvent(EventHandler e);
        Dictionary<EVENT_TYPE, List<OnEvent>> listeners = new Dictionary<EVENT_TYPE, List<OnEvent>>();

        static public void AddListener(EVENT_TYPE type, OnEvent eventFunc)
        {
            Me.listeners = Me.listeners ?? new Dictionary<EVENT_TYPE, List<OnEvent>>();

            if (!Me.listeners.ContainsKey(type) || Me.listeners[type] == null)
                Me.listeners[type] = new List<OnEvent>();
            Me.listeners[type].Add(eventFunc);
        }
        static public void RemoveListener(EVENT_TYPE type, OnEvent eventFunc)
        {
            if (IsInstanceDeleted)
                return;
            if (Me.listeners == null || me == null) return;
            if (Me.listeners.ContainsKey(type) && Me.listeners[type] != null)
                Me.listeners[type].Remove(eventFunc);
        }
        static public void InvokeEvent(EVENT_TYPE type, EventHandler e)
        {
            if (IsInstanceDeleted)
                return;
            if (Me.listeners == null || me == null) return;
            bool isExist = Me?.listeners?.ContainsKey(type) ?? false;
            if (isExist)
            {
                for (int i = 0; i < Me.listeners[type].Count; ++i)
                {
                    Me.listeners[type][i](e);
                }
            }
        }
    }
}

