Shader "Custom/BossOutBright" {
	Properties {
		_MainColor ("【主颜色】Main Color", Vector) = (0.5,0.5,0.5,1)
		_TextureDiffuse ("【漫反射纹理】Texture Diffuse", 2D) = "white" {}
		_RimColor ("【边缘发光颜色】Rim Color", Vector) = (0.5,0.5,0.5,1)
		_RimPower ("【边缘发光强度】Rim Power", Range(0, 36)) = 0.1
		_RimIntensity ("【边缘发光强度系数】Rim Intensity", Range(0, 100)) = 3
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }
		Pass {
			Name "ForwardBase"
			Tags { "LIGHTMODE" = "FORWARDBASE" "RenderType" = "Opaque" }
			GpuProgramID 4588
			Program "vp" {
				SubProgram "gles hw_tier00 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec3 in_NORMAL0;
					attribute highp vec4 in_TEXCOORD0;
					varying highp vec4 vs_TEXCOORD0;
					varying highp vec3 vs_NORMAL0;
					varying highp vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    vs_TEXCOORD1 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
					    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
					    vs_TEXCOORD0 = in_TEXCOORD0;
					    vs_NORMAL0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
					    vs_NORMAL0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
					    vs_NORMAL0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec3 _WorldSpaceCameraPos;
					uniform 	vec4 _WorldSpaceLightPos0;
					uniform 	mediump vec4 glstate_lightmodel_ambient;
					uniform 	vec4 _LightColor0;
					uniform 	vec4 _MainColor;
					uniform 	vec4 _TextureDiffuse_ST;
					uniform 	vec4 _RimColor;
					uniform 	float _RimPower;
					uniform 	float _RimIntensity;
					uniform lowp sampler2D _TextureDiffuse;
					varying highp vec4 vs_TEXCOORD0;
					varying highp vec3 vs_NORMAL0;
					varying highp vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					vec3 u_xlat1;
					vec3 u_xlat2;
					lowp vec3 u_xlat10_2;
					mediump vec3 u_xlat16_3;
					float u_xlat12;
					void main()
					{
					    u_xlat0.xyz = (-vs_TEXCOORD1.xyz) + _WorldSpaceCameraPos.xyz;
					    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
					    u_xlat0.x = dot(vs_NORMAL0.xyz, u_xlat0.xyz);
					    u_xlat0.x = max(u_xlat0.x, 0.0);
					    u_xlat0.x = (-u_xlat0.x) + 1.0;
					    u_xlat0.x = log2(u_xlat0.x);
					    u_xlat0.x = u_xlat0.x * _RimPower;
					    u_xlat0.x = exp2(u_xlat0.x);
					    u_xlat0.xyz = u_xlat0.xxx * _RimColor.xyz;
					    u_xlat0.xyz = u_xlat0.xyz * vec3(vec3(_RimIntensity, _RimIntensity, _RimIntensity));
					    u_xlat12 = dot(vs_NORMAL0.xyz, vs_NORMAL0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat1.xyz = vec3(u_xlat12) * vs_NORMAL0.xyz;
					    u_xlat12 = dot(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat2.xyz = vec3(u_xlat12) * _WorldSpaceLightPos0.xyz;
					    u_xlat12 = dot(u_xlat1.xyz, u_xlat2.xyz);
					    u_xlat12 = max(u_xlat12, 0.0);
					    u_xlat16_3.xyz = glstate_lightmodel_ambient.xyz + glstate_lightmodel_ambient.xyz;
					    u_xlat1.xyz = vec3(u_xlat12) * _LightColor0.xyz + u_xlat16_3.xyz;
					    u_xlat2.xy = vs_TEXCOORD0.xy * _TextureDiffuse_ST.xy + _TextureDiffuse_ST.zw;
					    u_xlat10_2.xyz = texture2D(_TextureDiffuse, u_xlat2.xy).xyz;
					    u_xlat2.xyz = u_xlat10_2.xyz * _MainColor.xyz;
					    u_xlat0.xyz = u_xlat1.xyz * u_xlat2.xyz + u_xlat0.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    SV_Target0.w = 1.0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec3 in_NORMAL0;
					attribute highp vec4 in_TEXCOORD0;
					varying highp vec4 vs_TEXCOORD0;
					varying highp vec3 vs_NORMAL0;
					varying highp vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    vs_TEXCOORD1 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
					    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
					    vs_TEXCOORD0 = in_TEXCOORD0;
					    vs_NORMAL0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
					    vs_NORMAL0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
					    vs_NORMAL0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec3 _WorldSpaceCameraPos;
					uniform 	vec4 _WorldSpaceLightPos0;
					uniform 	mediump vec4 glstate_lightmodel_ambient;
					uniform 	vec4 _LightColor0;
					uniform 	vec4 _MainColor;
					uniform 	vec4 _TextureDiffuse_ST;
					uniform 	vec4 _RimColor;
					uniform 	float _RimPower;
					uniform 	float _RimIntensity;
					uniform lowp sampler2D _TextureDiffuse;
					varying highp vec4 vs_TEXCOORD0;
					varying highp vec3 vs_NORMAL0;
					varying highp vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					vec3 u_xlat1;
					vec3 u_xlat2;
					lowp vec3 u_xlat10_2;
					mediump vec3 u_xlat16_3;
					float u_xlat12;
					void main()
					{
					    u_xlat0.xyz = (-vs_TEXCOORD1.xyz) + _WorldSpaceCameraPos.xyz;
					    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
					    u_xlat0.x = dot(vs_NORMAL0.xyz, u_xlat0.xyz);
					    u_xlat0.x = max(u_xlat0.x, 0.0);
					    u_xlat0.x = (-u_xlat0.x) + 1.0;
					    u_xlat0.x = log2(u_xlat0.x);
					    u_xlat0.x = u_xlat0.x * _RimPower;
					    u_xlat0.x = exp2(u_xlat0.x);
					    u_xlat0.xyz = u_xlat0.xxx * _RimColor.xyz;
					    u_xlat0.xyz = u_xlat0.xyz * vec3(vec3(_RimIntensity, _RimIntensity, _RimIntensity));
					    u_xlat12 = dot(vs_NORMAL0.xyz, vs_NORMAL0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat1.xyz = vec3(u_xlat12) * vs_NORMAL0.xyz;
					    u_xlat12 = dot(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat2.xyz = vec3(u_xlat12) * _WorldSpaceLightPos0.xyz;
					    u_xlat12 = dot(u_xlat1.xyz, u_xlat2.xyz);
					    u_xlat12 = max(u_xlat12, 0.0);
					    u_xlat16_3.xyz = glstate_lightmodel_ambient.xyz + glstate_lightmodel_ambient.xyz;
					    u_xlat1.xyz = vec3(u_xlat12) * _LightColor0.xyz + u_xlat16_3.xyz;
					    u_xlat2.xy = vs_TEXCOORD0.xy * _TextureDiffuse_ST.xy + _TextureDiffuse_ST.zw;
					    u_xlat10_2.xyz = texture2D(_TextureDiffuse, u_xlat2.xy).xyz;
					    u_xlat2.xyz = u_xlat10_2.xyz * _MainColor.xyz;
					    u_xlat0.xyz = u_xlat1.xyz * u_xlat2.xyz + u_xlat0.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    SV_Target0.w = 1.0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec3 in_NORMAL0;
					attribute highp vec4 in_TEXCOORD0;
					varying highp vec4 vs_TEXCOORD0;
					varying highp vec3 vs_NORMAL0;
					varying highp vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    vs_TEXCOORD1 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
					    u_xlat0 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat0;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat0;
					    vs_TEXCOORD0 = in_TEXCOORD0;
					    vs_NORMAL0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
					    vs_NORMAL0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
					    vs_NORMAL0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec3 _WorldSpaceCameraPos;
					uniform 	vec4 _WorldSpaceLightPos0;
					uniform 	mediump vec4 glstate_lightmodel_ambient;
					uniform 	vec4 _LightColor0;
					uniform 	vec4 _MainColor;
					uniform 	vec4 _TextureDiffuse_ST;
					uniform 	vec4 _RimColor;
					uniform 	float _RimPower;
					uniform 	float _RimIntensity;
					uniform lowp sampler2D _TextureDiffuse;
					varying highp vec4 vs_TEXCOORD0;
					varying highp vec3 vs_NORMAL0;
					varying highp vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					vec3 u_xlat1;
					vec3 u_xlat2;
					lowp vec3 u_xlat10_2;
					mediump vec3 u_xlat16_3;
					float u_xlat12;
					void main()
					{
					    u_xlat0.xyz = (-vs_TEXCOORD1.xyz) + _WorldSpaceCameraPos.xyz;
					    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
					    u_xlat0.x = dot(vs_NORMAL0.xyz, u_xlat0.xyz);
					    u_xlat0.x = max(u_xlat0.x, 0.0);
					    u_xlat0.x = (-u_xlat0.x) + 1.0;
					    u_xlat0.x = log2(u_xlat0.x);
					    u_xlat0.x = u_xlat0.x * _RimPower;
					    u_xlat0.x = exp2(u_xlat0.x);
					    u_xlat0.xyz = u_xlat0.xxx * _RimColor.xyz;
					    u_xlat0.xyz = u_xlat0.xyz * vec3(vec3(_RimIntensity, _RimIntensity, _RimIntensity));
					    u_xlat12 = dot(vs_NORMAL0.xyz, vs_NORMAL0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat1.xyz = vec3(u_xlat12) * vs_NORMAL0.xyz;
					    u_xlat12 = dot(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz);
					    u_xlat12 = inversesqrt(u_xlat12);
					    u_xlat2.xyz = vec3(u_xlat12) * _WorldSpaceLightPos0.xyz;
					    u_xlat12 = dot(u_xlat1.xyz, u_xlat2.xyz);
					    u_xlat12 = max(u_xlat12, 0.0);
					    u_xlat16_3.xyz = glstate_lightmodel_ambient.xyz + glstate_lightmodel_ambient.xyz;
					    u_xlat1.xyz = vec3(u_xlat12) * _LightColor0.xyz + u_xlat16_3.xyz;
					    u_xlat2.xy = vs_TEXCOORD0.xy * _TextureDiffuse_ST.xy + _TextureDiffuse_ST.zw;
					    u_xlat10_2.xyz = texture2D(_TextureDiffuse, u_xlat2.xy).xyz;
					    u_xlat2.xyz = u_xlat10_2.xyz * _MainColor.xyz;
					    u_xlat0.xyz = u_xlat1.xyz * u_xlat2.xyz + u_xlat0.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    SV_Target0.w = 1.0;
					    return;
					}
					
					#endif"
				}
			}
			Program "fp" {
				SubProgram "gles hw_tier00 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					"!!!!GLES"
				}
			}
		}
	}
	Fallback "Diffuse"
}