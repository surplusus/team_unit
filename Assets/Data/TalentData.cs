﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DATA
{
    [CreateAssetMenu(fileName = "DATA_Talent", menuName = "DATA/Talent")]
    public class TalentData : ScriptableObject, IData
    {
        [Header("PlayerTalent")]
        public List<Talent> Talents;
        public string ShowExplanation(DATA.TalentName index)
        {
            string variable = Talents[(int)index].Veriable;
            string copy = Talents[(int)index].Explanation;
            string result = copy.Replace("##", variable);
            return result;
        }
        public string ShowExplanation(int index)
        {
            string variable = Talents[index].Veriable;
            string copy = Talents[index].Explanation;
            string result = copy.Replace("##", variable);
            return result;
        }
        public int ShowLevel(DATA.TalentName index)
        {
            return Talents[(int)index].Level;
        }
    }

    [System.Serializable]
    public class Talent
    {
        public DATA.TalentName Name;
        public int Level = 0;
        public string Explanation;
        public string Veriable;
        public Talent(DATA.TalentName name)
        {
            this.Name = name;
        }
    }
}