using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class LocalPathList : ScriptableObject
{
	public List<LocalPath> Addresses; // Replace 'EntityType' to an actual type that is serializable.
}

[System.Serializable]
public class LocalPath
{
	public int no;
	public string id;
	public string path;
}