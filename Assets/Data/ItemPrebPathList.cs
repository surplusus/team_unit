﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Item
{
    [CreateAssetMenu(fileName = "DATA_ItemPath", menuName = "DATA/ItemPath")]
    public class ItemPrebPathList : ScriptableObject
    {
        public List<ItemPrebs> ItemPrebs = new List<ItemPrebs>(3)
        {
            new ItemPrebs{Name="Sling"},
            new ItemPrebs{Name="Bow"},
            new ItemPrebs{Name="Boomerang"},
        };
    }
    [System.Serializable]
    public class ItemPrebs
    {
        public string Name;
        public GameObject InGamePreb;
        public GameObject SlotPreb;
    }
}
