using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class TalentDescriptionList : ScriptableObject
{
	public List<TalentDescription> Description;
}

[System.Serializable]
public class TalentDescription
{
	public DATA.TalentName talentName;
	public string description;
}