﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DATA
{
    [CreateAssetMenu(fileName = "DATA_AttackData", menuName = "DATA/AttackData")]
    public class Attack : ScriptableObject, IData
    {
        public float Strangth;   // strangth
        public float CriticalRate;    // Critical Rate
        public float CriticalDamage;    // Critical Damage
        public float DamagePerSecond;   // Damage per Second
        public Attack(Attack other)
        {
            this.Strangth = other.Strangth;
            this.CriticalRate = other.CriticalRate;
            this.CriticalDamage = other.CriticalDamage;
            this.DamagePerSecond = other.DamagePerSecond;
        }
    }
}