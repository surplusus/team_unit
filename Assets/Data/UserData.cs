﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Item;
using UnityEngine.AddressableAssets;
using System.Linq;

namespace DATA
{
    [CreateAssetMenu(fileName = "DATA_UserData", menuName = "DATA/UserData")]
    public class UserData : ScriptableObject, IData
    {
        public int Level { get => Exp.Level; set => Exp.Level = value; }
        public Experience Exp;
        public ActionPoint ActionPoint;
        public int Coin;
        public int Gem;
        public List<InventoryItem> Inventory;
        public List<string> LoadedSpriteNames;
        public List<EquipedItem> EquipedItems = new List<EquipedItem>(6) // ShowInventory가 쓴다
        {
            new EquipedItem("Weapon"), new EquipedItem("Armor"), new EquipedItem("Ring1"), new EquipedItem("Ring2"), new EquipedItem("Pet1"), new EquipedItem("Pet2"),
        };
        public List<GameObject> GetAllInventoryItemSlots()
        {
            List<GameObject> result = new List<GameObject>();
            var query = from obj in DataManager.Me.ItemsInIndicator
                        from inv in Inventory
                        where obj.name == inv.Name
                        select obj;
            foreach (var item in query)
                result.Add(item);
            return result;
        }
    }
    [System.Serializable]
    public class ActionPoint
    {
        public int Limit = 30;
        public int Remainder = 0;
        public float IncrementalTime = 1; // ActioinPoint가 증가하는 시간 간격
        private DateTime standardTime = DateTime.Now;
        public int Stamina { get; set; } = 0;
        public float ActionPointRate()
        {
            return Stamina / (float)Limit;
        }
        public float ElapsedTime()
        {   // 기준시간 대비 얼마나 시간이 지났나?
            TimeSpan delta = TimeSpan.FromTicks(DateTime.Now.Ticks - standardTime.Ticks);
            return (float)(new decimal(delta.TotalSeconds));
        }
        public void EarnPoint()
        {
            var earnPoints = (int)(ElapsedTime() / IncrementalTime);
            if (earnPoints > Limit - Remainder)
                SetStandartTime();
            Stamina = earnPoints + Remainder;
            Stamina = Mathf.Clamp(Stamina, 0, Limit);
        }
        public void SetStandartTime()
        {  
            standardTime = DateTime.Now;    // 객체 생성 시각을 기록
            Remainder = Stamina;  // 저장되어있던 포인트를 기록
        }
        public bool MinusRemainder(int point)
        {
            if (Stamina - point < 0)
                return false;
            Remainder -= point;
            return true;
        }
        public bool PlusRemainder(int point)
        {
            if (Stamina > Limit)
            {
                Stamina = Limit;
                return false;
            }
            Remainder += point;
            return true;
        }
    }
    [System.Serializable]
    public class Experience
    {
        private int[] LevelDesign =
        {
        10,20,30,40,50,60,70,80,90,100,
    };
        public int Level = 1;
        public int ExpPoint = 0;

        public float GetExpRate()
        {
            return ExpPoint / (float)LevelDesign[(Level - 1) % 10];
        }

        public bool LevelUp()
        {
            if(ExpPoint >= LevelDesign[(Level - 1) % 10])
            {
                ExpPoint -= LevelDesign[(Level - 1) % 10];
                Level++;
                return true;
            }
            return false;
        }
    }
    [System.Serializable]
    public class EquipedItem
    {
        public string Name;
        public GameObjectIndexing itemPreb;
        public IItem drivedClass;
        public EquipedItem(string s)
        {
            this.Name = s; itemPreb = new GameObjectIndexing("Sling", null);
        }
    }
    [System.Serializable]
    public class InventoryItem
    {
        public string Name;
        public int Count;
        public int Level;
    }
}

