﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;

#region Data Class 모음
namespace DATA
{
    public interface IData
    {
    }
    public class ItemIndexData : IData
    {
        public List<string> SpriteNames;
        public List<string> ItemKoreanName;
    }
    [System.Serializable]
    public class ItemDictionary : System.IComparable<ItemDictionary>
    {       // List<GameObject> 대신 Dictinary처럼 아이템을 구분하기위해 만든 클래스
        public ItemType itemType;
        public GameObject itemPreb;
        public ItemDictionary(ItemType type, GameObject preb)
        {
            this.itemType = type;
            this.itemPreb = preb;
        }
        public int CompareTo(ItemDictionary other)
        {
            if (other == null)
                return 1;
            return (int)itemType - (int)other.itemType;
        }
    }
    [System.Serializable]
    public class GameObjectIndexing
    {
        public string Key;
        [HideInInspector] public string Name;
        [HideInInspector] public string Path;
        [SerializeField] GameObject go; 
        public GameObject GO
        {
            get
            {
                if (DataManager.IsAllAssetsLoaded)
                    return DataManager.Me.ItemAssets.Find(x=>x.name == Key);
                Debug.Log("아직 DataManager가 준비되지 않았습니다");
                return null;
            }
        }
        public GameObjectIndexing(string key, GameObject _go)
        {
            this.Key = key; this.go = _go;
        }
        public GameObjectIndexing(string name, string key, string path, GameObject go)
        {
            this.Name = name; this.Key = key; this.Path = path; this.go = go;
        }
    }
}
#endregion

#region ENUMS
namespace DATA
{
    [System.Serializable]
    public enum TalentName : int
    {
        HealthEnhancer = 0,
        StrangthEnhancer = 1,
        HeartEnhancer,
        DefenseEnhancer,
        DexterityEnhancer,
        SelfHealEnhancer,
        ItemEnhancer,
        MoreCoinGainer,
        AbilityGainer,
    }
    [System.Serializable]
    public enum CharacterSpecies
    {
        아트레우스,
        우라실,
        포렌,
    }
    public enum AbilityType
    {
        S_AttackBoost,
    }
    [System.Serializable]
    public enum ItemType
    {
        WEAPON = 0,
        ARMOR = 1,
        ACCESSORY,
    }
}
#endregion

#region BehaviourTreeCustomVariable
[System.Serializable]
public class PlayerBTVariable
{
    public float Velocity;
    public Vector3 Forward;
}
[System.Serializable]
public class SharedPlayerVariables : SharedVariable<PlayerBTVariable>
{
    public static implicit operator SharedPlayerVariables(PlayerBTVariable value)
    {
        return new SharedPlayerVariables { Value = value };
    }
}
[System.Serializable]
public class MonsterBTVariable
{
    public float Velocity;
    public Vector3 Forward;
}
[System.Serializable]
public class SharedMonsterVariables : SharedVariable<MonsterBTVariable>
{
    public static implicit operator SharedMonsterVariables(MonsterBTVariable value)
    {
        return new SharedMonsterVariables { Value = value };
    }
}
#endregion