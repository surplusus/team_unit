﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DATA
{
    [CreateAssetMenu(fileName = "DATA_Condition", menuName = "DATA/Condition")]
    public class Condition : ScriptableObject, IData
    {
        public bool IsPiercing;
        public bool IsIce;
        public bool IsFire;
        public bool IsElectric;
        public bool IsPoison;

        public bool IsWallReflection;
        public bool IsForwardArrow;
        public bool IsRearArrow;

        public float ReflectionRate;
        public float RicochetRate;
        public float BoomerangRate;
    }
}