﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string Name;
    public AudioClip Clip;
    private AudioSource source;
    public float Volume;
    public bool IsLoop
    {
        get => isLoop;
        set
        {
            isLoop = value;
            if (isLoop) 
                source.loop = true;
            else 
                source.loop = false;
        }
    }
    private bool isLoop = false;
    public void SetSource(AudioSource fileSource)
    {
        this.source = fileSource;
        if (fileSource.clip != null)
        {
            source.clip = this.Clip;
        }
    }
    public void Play()
    {
        this.source.clip = Clip;
        source.Play();
    }
    public void Stop()
    {
        source.Stop();
    }
}
[CreateAssetMenu(fileName ="DATA/SoundPath_", menuName ="DATA/SoundPath")]
public class SoundPathList : ScriptableObject
{
    public List<Sound> Sounds;
}
