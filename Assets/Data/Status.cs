﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DATA
{
    [CreateAssetMenu(fileName = "DATA_Status", menuName = "DATA/Status")]
    public class Status : ScriptableObject, IData
    {
        public int MaxHealthPoint;
        public int HealthPoint;
        public int Strangth;
        public int CriticalRate;
        public int CriticalDamage;
        public int Dexterity;
        public int RangeDefense;
        public int MeleeDefense;
        public Experience Exp;
        public Status(Status other)
        {
            this.MaxHealthPoint = other.MaxHealthPoint;
            this.HealthPoint = other.HealthPoint;
            this.Strangth = other.Strangth;
            this.CriticalRate = other.CriticalRate;
            this.CriticalDamage = other.CriticalDamage;
            this.Dexterity = other.Dexterity;
            this.RangeDefense = other.RangeDefense;
            this.MeleeDefense = other.MeleeDefense;
            Exp = new Experience();
        }
    }
}