﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace DATA
{
    [CreateAssetMenu(fileName = "DATA_BulletPrebPath", menuName = "DATA/BulletPrebPath")]
    public class BulletPrebPathList : ScriptableObject, IData
    {
        [Header("Sling")]
        public GameObjectIndexing SlingAsset;
        [Header("Bow")]
        public GameObjectIndexing BowAsset;
        [Header("Boomerang")]
        public GameObjectIndexing BoomerangAsset;
    }
}