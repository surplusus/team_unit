Shader "H3D/InGame/Others/Scroll 2 Layers Sine Addtive" {
	Properties {
		_MainTex ("Base layer (RGB)", 2D) = "white" {}
		_MaskTex ("Mask(R _Main Alpha,B DetailTex Alpha)", 2D) = "white" {}
		_DetailTex ("2nd layer (RGB)", 2D) = "white" {}
		_ScrollX ("Base layer Scroll speed X", Float) = 1
		_ScrollY ("Base layer Scroll speed Y", Float) = 0
		_Scroll2X ("2nd layer Scroll speed X", Float) = 1
		_Scroll2Y ("2nd layer Scroll speed Y", Float) = 0
		_SineAmplX ("Base layer sine amplitude X", Float) = 0.5
		_SineAmplY ("Base layer sine amplitude Y", Float) = 0.5
		_SineFreqX ("Base layer sine freq X", Float) = 10
		_SineFreqY ("Base layer sine freq Y", Float) = 10
		_SineAmplX2 ("2nd layer sine amplitude X", Float) = 0.5
		_SineAmplY2 ("2nd layer sine amplitude Y", Float) = 0.5
		_SineFreqX2 ("2nd layer sine freq X", Float) = 10
		_SineFreqY2 ("2nd layer sine freq Y", Float) = 10
		_Color ("Color", Vector) = (1,1,1,1)
		_MMultiplier ("Layer Multiplier", Float) = 2
		_Alpha ("透明度", Range(0, 1)) = 1
	}
	SubShader {
		LOD 200
		Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
		Pass {
			LOD 200
			Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
			Blend SrcAlpha One, SrcAlpha One
			ZWrite Off
			Cull Off
			GpuProgramID 54139
			Program "vp" {
				SubProgram "gles hw_tier00 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					mediump vec4 u_xlat16_0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					float u_xlat9;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = _Time.x * _SineFreqX;
					    u_xlat0.x = sin(u_xlat0.x);
					    u_xlat3.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat3.xy = u_xlat3.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat0.x * _SineAmplX + u_xlat3.x;
					    u_xlat0.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat0.xy = u_xlat1.zw + u_xlat0.xy;
					    u_xlat9 = _Time.x * _SineFreqY2;
					    u_xlat9 = sin(u_xlat9);
					    u_xlat2.w = u_xlat9 * _SineAmplY2 + u_xlat0.y;
					    u_xlat3.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat3.xz = sin(u_xlat3.xz);
					    u_xlat2.y = u_xlat3.x * _SineAmplY + u_xlat3.y;
					    u_xlat2.z = u_xlat3.z * _SineAmplX2 + u_xlat0.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_0 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_0 * in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    SV_Target0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					mediump vec4 u_xlat16_0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					float u_xlat9;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = _Time.x * _SineFreqX;
					    u_xlat0.x = sin(u_xlat0.x);
					    u_xlat3.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat3.xy = u_xlat3.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat0.x * _SineAmplX + u_xlat3.x;
					    u_xlat0.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat0.xy = u_xlat1.zw + u_xlat0.xy;
					    u_xlat9 = _Time.x * _SineFreqY2;
					    u_xlat9 = sin(u_xlat9);
					    u_xlat2.w = u_xlat9 * _SineAmplY2 + u_xlat0.y;
					    u_xlat3.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat3.xz = sin(u_xlat3.xz);
					    u_xlat2.y = u_xlat3.x * _SineAmplY + u_xlat3.y;
					    u_xlat2.z = u_xlat3.z * _SineAmplX2 + u_xlat0.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_0 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_0 * in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    SV_Target0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					mediump vec4 u_xlat16_0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					float u_xlat9;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = _Time.x * _SineFreqX;
					    u_xlat0.x = sin(u_xlat0.x);
					    u_xlat3.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat3.xy = u_xlat3.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat0.x * _SineAmplX + u_xlat3.x;
					    u_xlat0.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat0.xy = u_xlat1.zw + u_xlat0.xy;
					    u_xlat9 = _Time.x * _SineFreqY2;
					    u_xlat9 = sin(u_xlat9);
					    u_xlat2.w = u_xlat9 * _SineAmplY2 + u_xlat0.y;
					    u_xlat3.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat3.xz = sin(u_xlat3.xz);
					    u_xlat2.y = u_xlat3.x * _SineAmplY + u_xlat3.y;
					    u_xlat2.z = u_xlat3.z * _SineAmplX2 + u_xlat0.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_0 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_0 * in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    SV_Target0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "_ON_H3D_H_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					mediump vec4 u_xlat16_0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					float u_xlat9;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = _Time.x * _SineFreqX;
					    u_xlat0.x = sin(u_xlat0.x);
					    u_xlat3.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat3.xy = u_xlat3.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat0.x * _SineAmplX + u_xlat3.x;
					    u_xlat0.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat0.xy = u_xlat1.zw + u_xlat0.xy;
					    u_xlat9 = _Time.x * _SineFreqY2;
					    u_xlat9 = sin(u_xlat9);
					    u_xlat2.w = u_xlat9 * _SineAmplY2 + u_xlat0.y;
					    u_xlat3.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat3.xz = sin(u_xlat3.xz);
					    u_xlat2.y = u_xlat3.x * _SineAmplY + u_xlat3.y;
					    u_xlat2.z = u_xlat3.z * _SineAmplX2 + u_xlat0.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_0 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_0 * in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    SV_Target0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "_ON_H3D_H_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					mediump vec4 u_xlat16_0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					float u_xlat9;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = _Time.x * _SineFreqX;
					    u_xlat0.x = sin(u_xlat0.x);
					    u_xlat3.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat3.xy = u_xlat3.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat0.x * _SineAmplX + u_xlat3.x;
					    u_xlat0.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat0.xy = u_xlat1.zw + u_xlat0.xy;
					    u_xlat9 = _Time.x * _SineFreqY2;
					    u_xlat9 = sin(u_xlat9);
					    u_xlat2.w = u_xlat9 * _SineAmplY2 + u_xlat0.y;
					    u_xlat3.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat3.xz = sin(u_xlat3.xz);
					    u_xlat2.y = u_xlat3.x * _SineAmplY + u_xlat3.y;
					    u_xlat2.z = u_xlat3.z * _SineAmplX2 + u_xlat0.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_0 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_0 * in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    SV_Target0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "_ON_H3D_H_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					vec4 u_xlat0;
					mediump vec4 u_xlat16_0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					float u_xlat9;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = _Time.x * _SineFreqX;
					    u_xlat0.x = sin(u_xlat0.x);
					    u_xlat3.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat3.xy = u_xlat3.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat0.x * _SineAmplX + u_xlat3.x;
					    u_xlat0.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat0.xy = u_xlat1.zw + u_xlat0.xy;
					    u_xlat9 = _Time.x * _SineFreqY2;
					    u_xlat9 = sin(u_xlat9);
					    u_xlat2.w = u_xlat9 * _SineAmplY2 + u_xlat0.y;
					    u_xlat3.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat3.xz = sin(u_xlat3.xz);
					    u_xlat2.y = u_xlat3.x * _SineAmplY + u_xlat3.y;
					    u_xlat2.z = u_xlat3.z * _SineAmplX2 + u_xlat0.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_0 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_0 * in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					#define SV_Target0 gl_FragData[0]
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    SV_Target0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "_ON_H3D_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 h3d_FogParam;
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp float vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					mediump vec4 u_xlat16_1;
					vec4 u_xlat2;
					vec2 u_xlat3;
					vec2 u_xlat6;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = (-u_xlat0.z) + h3d_FogParam.y;
					    u_xlat3.x = _Time.x * _SineFreqX;
					    u_xlat3.x = sin(u_xlat3.x);
					    u_xlat6.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat6.xy = u_xlat6.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat3.x * _SineAmplX + u_xlat6.x;
					    u_xlat3.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat3.xy = u_xlat1.zw + u_xlat3.xy;
					    u_xlat1.x = _Time.x * _SineFreqY2;
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat2.w = u_xlat1.x * _SineAmplY2 + u_xlat3.y;
					    u_xlat1.xy = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat1.xy = sin(u_xlat1.xy);
					    u_xlat2.y = u_xlat1.x * _SineAmplY + u_xlat6.y;
					    u_xlat2.z = u_xlat1.y * _SineAmplX2 + u_xlat3.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_1 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_1 * in_COLOR0;
					    u_xlat3.x = (-h3d_FogParam.x) + h3d_FogParam.y;
					    vs_TEXCOORD2 = u_xlat0.x / u_xlat3.x;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump vec4 h3d_FogDistanceColor;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp float vs_TEXCOORD2;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					float u_xlat9;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    u_xlat16_0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz + (-h3d_FogDistanceColor.xyz);
					    u_xlat9 = vs_TEXCOORD2;
					    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
					    u_xlat0.xyz = vec3(u_xlat9) * u_xlat16_0.xyz + h3d_FogDistanceColor.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "_ON_H3D_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 h3d_FogParam;
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp float vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					mediump vec4 u_xlat16_1;
					vec4 u_xlat2;
					vec2 u_xlat3;
					vec2 u_xlat6;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = (-u_xlat0.z) + h3d_FogParam.y;
					    u_xlat3.x = _Time.x * _SineFreqX;
					    u_xlat3.x = sin(u_xlat3.x);
					    u_xlat6.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat6.xy = u_xlat6.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat3.x * _SineAmplX + u_xlat6.x;
					    u_xlat3.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat3.xy = u_xlat1.zw + u_xlat3.xy;
					    u_xlat1.x = _Time.x * _SineFreqY2;
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat2.w = u_xlat1.x * _SineAmplY2 + u_xlat3.y;
					    u_xlat1.xy = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat1.xy = sin(u_xlat1.xy);
					    u_xlat2.y = u_xlat1.x * _SineAmplY + u_xlat6.y;
					    u_xlat2.z = u_xlat1.y * _SineAmplX2 + u_xlat3.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_1 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_1 * in_COLOR0;
					    u_xlat3.x = (-h3d_FogParam.x) + h3d_FogParam.y;
					    vs_TEXCOORD2 = u_xlat0.x / u_xlat3.x;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump vec4 h3d_FogDistanceColor;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp float vs_TEXCOORD2;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					float u_xlat9;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    u_xlat16_0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz + (-h3d_FogDistanceColor.xyz);
					    u_xlat9 = vs_TEXCOORD2;
					    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
					    u_xlat0.xyz = vec3(u_xlat9) * u_xlat16_0.xyz + h3d_FogDistanceColor.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "_ON_H3D_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 h3d_FogParam;
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp float vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					mediump vec4 u_xlat16_1;
					vec4 u_xlat2;
					vec2 u_xlat3;
					vec2 u_xlat6;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    gl_Position = u_xlat0;
					    u_xlat0.x = (-u_xlat0.z) + h3d_FogParam.y;
					    u_xlat3.x = _Time.x * _SineFreqX;
					    u_xlat3.x = sin(u_xlat3.x);
					    u_xlat6.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat1 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat1 = fract(u_xlat1);
					    u_xlat6.xy = u_xlat6.xy + u_xlat1.xy;
					    u_xlat2.x = u_xlat3.x * _SineAmplX + u_xlat6.x;
					    u_xlat3.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat3.xy = u_xlat1.zw + u_xlat3.xy;
					    u_xlat1.x = _Time.x * _SineFreqY2;
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat2.w = u_xlat1.x * _SineAmplY2 + u_xlat3.y;
					    u_xlat1.xy = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat1.xy = sin(u_xlat1.xy);
					    u_xlat2.y = u_xlat1.x * _SineAmplY + u_xlat6.y;
					    u_xlat2.z = u_xlat1.y * _SineAmplX2 + u_xlat3.x;
					    vs_TEXCOORD0 = u_xlat2;
					    u_xlat16_1 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_1 * in_COLOR0;
					    u_xlat3.x = (-h3d_FogParam.x) + h3d_FogParam.y;
					    vs_TEXCOORD2 = u_xlat0.x / u_xlat3.x;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump vec4 h3d_FogDistanceColor;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp float vs_TEXCOORD2;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec3 u_xlat10_0;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					float u_xlat9;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    u_xlat16_0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz + (-h3d_FogDistanceColor.xyz);
					    u_xlat9 = vs_TEXCOORD2;
					    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
					    u_xlat0.xyz = vec3(u_xlat9) * u_xlat16_0.xyz + h3d_FogDistanceColor.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "_ON_H3D_FOG" "_ON_H3D_H_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 h3d_FogParam;
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp vec2 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					mediump vec4 u_xlat16_1;
					vec4 u_xlat2;
					vec4 u_xlat3;
					vec2 u_xlat8;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat0.x = hlslcc_mtx4x4unity_ObjectToWorld[3].y * in_POSITION0.w + u_xlat0.y;
					    u_xlat0.x = u_xlat0.x + (-h3d_FogParam.z);
					    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
					    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
					    gl_Position = u_xlat1;
					    u_xlat0.y = (-u_xlat1.z) + h3d_FogParam.y;
					    u_xlat8.x = _Time.x * _SineFreqX;
					    u_xlat8.x = sin(u_xlat8.x);
					    u_xlat1.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat2 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat2 = fract(u_xlat2);
					    u_xlat1.xy = u_xlat1.xy + u_xlat2.xy;
					    u_xlat3.x = u_xlat8.x * _SineAmplX + u_xlat1.x;
					    u_xlat8.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat8.xy = u_xlat2.zw + u_xlat8.xy;
					    u_xlat1.x = _Time.x * _SineFreqY2;
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat3.w = u_xlat1.x * _SineAmplY2 + u_xlat8.y;
					    u_xlat1.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat1.xz = sin(u_xlat1.xz);
					    u_xlat3.y = u_xlat1.x * _SineAmplY + u_xlat1.y;
					    u_xlat3.z = u_xlat1.z * _SineAmplX2 + u_xlat8.x;
					    vs_TEXCOORD0 = u_xlat3;
					    u_xlat16_1 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_1 * in_COLOR0;
					    u_xlat8.xy = (-h3d_FogParam.xz) + h3d_FogParam.yw;
					    vs_TEXCOORD2.xy = u_xlat0.yx / u_xlat8.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump vec4 h3d_FogDistanceColor;
					uniform 	mediump vec4 h3d_FogHeightColor;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp vec2 vs_TEXCOORD2;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec3 u_xlat10_0;
					vec2 u_xlat1;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    u_xlat16_0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz + (-h3d_FogDistanceColor.xyz);
					    u_xlat1.xy = vs_TEXCOORD2.xy;
					    u_xlat1.xy = clamp(u_xlat1.xy, 0.0, 1.0);
					    u_xlat0.xyz = u_xlat1.xxx * u_xlat16_0.xyz + h3d_FogDistanceColor.xyz;
					    u_xlat0.xyz = u_xlat0.xyz + (-h3d_FogHeightColor.xyz);
					    u_xlat0.xyz = u_xlat1.yyy * u_xlat0.xyz + h3d_FogHeightColor.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "_ON_H3D_FOG" "_ON_H3D_H_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 h3d_FogParam;
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp vec2 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					mediump vec4 u_xlat16_1;
					vec4 u_xlat2;
					vec4 u_xlat3;
					vec2 u_xlat8;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat0.x = hlslcc_mtx4x4unity_ObjectToWorld[3].y * in_POSITION0.w + u_xlat0.y;
					    u_xlat0.x = u_xlat0.x + (-h3d_FogParam.z);
					    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
					    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
					    gl_Position = u_xlat1;
					    u_xlat0.y = (-u_xlat1.z) + h3d_FogParam.y;
					    u_xlat8.x = _Time.x * _SineFreqX;
					    u_xlat8.x = sin(u_xlat8.x);
					    u_xlat1.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat2 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat2 = fract(u_xlat2);
					    u_xlat1.xy = u_xlat1.xy + u_xlat2.xy;
					    u_xlat3.x = u_xlat8.x * _SineAmplX + u_xlat1.x;
					    u_xlat8.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat8.xy = u_xlat2.zw + u_xlat8.xy;
					    u_xlat1.x = _Time.x * _SineFreqY2;
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat3.w = u_xlat1.x * _SineAmplY2 + u_xlat8.y;
					    u_xlat1.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat1.xz = sin(u_xlat1.xz);
					    u_xlat3.y = u_xlat1.x * _SineAmplY + u_xlat1.y;
					    u_xlat3.z = u_xlat1.z * _SineAmplX2 + u_xlat8.x;
					    vs_TEXCOORD0 = u_xlat3;
					    u_xlat16_1 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_1 * in_COLOR0;
					    u_xlat8.xy = (-h3d_FogParam.xz) + h3d_FogParam.yw;
					    vs_TEXCOORD2.xy = u_xlat0.yx / u_xlat8.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump vec4 h3d_FogDistanceColor;
					uniform 	mediump vec4 h3d_FogHeightColor;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp vec2 vs_TEXCOORD2;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec3 u_xlat10_0;
					vec2 u_xlat1;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    u_xlat16_0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz + (-h3d_FogDistanceColor.xyz);
					    u_xlat1.xy = vs_TEXCOORD2.xy;
					    u_xlat1.xy = clamp(u_xlat1.xy, 0.0, 1.0);
					    u_xlat0.xyz = u_xlat1.xxx * u_xlat16_0.xyz + h3d_FogDistanceColor.xyz;
					    u_xlat0.xyz = u_xlat0.xyz + (-h3d_FogHeightColor.xyz);
					    u_xlat0.xyz = u_xlat1.yyy * u_xlat0.xyz + h3d_FogHeightColor.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "_ON_H3D_FOG" "_ON_H3D_H_FOG" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 h3d_FogParam;
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	mediump vec4 _DetailTex_ST;
					uniform 	mediump float _ScrollX;
					uniform 	mediump float _ScrollY;
					uniform 	mediump float _Scroll2X;
					uniform 	mediump float _Scroll2Y;
					uniform 	mediump float _MMultiplier;
					uniform 	mediump float _SineAmplX;
					uniform 	mediump float _SineAmplY;
					uniform 	mediump float _SineFreqX;
					uniform 	mediump float _SineFreqY;
					uniform 	mediump float _SineAmplX2;
					uniform 	mediump float _SineAmplY2;
					uniform 	mediump float _SineFreqX2;
					uniform 	mediump float _SineFreqY2;
					uniform 	mediump vec4 _Color;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					attribute mediump vec4 in_COLOR0;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp vec2 vs_TEXCOORD2;
					vec4 u_xlat0;
					vec4 u_xlat1;
					mediump vec4 u_xlat16_1;
					vec4 u_xlat2;
					vec4 u_xlat3;
					vec2 u_xlat8;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat0.x = hlslcc_mtx4x4unity_ObjectToWorld[3].y * in_POSITION0.w + u_xlat0.y;
					    u_xlat0.x = u_xlat0.x + (-h3d_FogParam.z);
					    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
					    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
					    gl_Position = u_xlat1;
					    u_xlat0.y = (-u_xlat1.z) + h3d_FogParam.y;
					    u_xlat8.x = _Time.x * _SineFreqX;
					    u_xlat8.x = sin(u_xlat8.x);
					    u_xlat1.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    u_xlat2 = _Time.xyxy * vec4(_ScrollX, _ScrollY, _Scroll2X, _Scroll2Y);
					    u_xlat2 = fract(u_xlat2);
					    u_xlat1.xy = u_xlat1.xy + u_xlat2.xy;
					    u_xlat3.x = u_xlat8.x * _SineAmplX + u_xlat1.x;
					    u_xlat8.xy = in_TEXCOORD0.xy * _DetailTex_ST.xy + _DetailTex_ST.zw;
					    u_xlat8.xy = u_xlat2.zw + u_xlat8.xy;
					    u_xlat1.x = _Time.x * _SineFreqY2;
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat3.w = u_xlat1.x * _SineAmplY2 + u_xlat8.y;
					    u_xlat1.xz = _Time.xx * vec2(_SineFreqY, _SineFreqX2);
					    u_xlat1.xz = sin(u_xlat1.xz);
					    u_xlat3.y = u_xlat1.x * _SineAmplY + u_xlat1.y;
					    u_xlat3.z = u_xlat1.z * _SineAmplX2 + u_xlat8.x;
					    vs_TEXCOORD0 = u_xlat3;
					    u_xlat16_1 = vec4(_MMultiplier) * _Color;
					    vs_TEXCOORD1 = u_xlat16_1 * in_COLOR0;
					    u_xlat8.xy = (-h3d_FogParam.xz) + h3d_FogParam.yw;
					    vs_TEXCOORD2.xy = u_xlat0.yx / u_xlat8.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	mediump vec4 h3d_FogDistanceColor;
					uniform 	mediump vec4 h3d_FogHeightColor;
					uniform 	mediump float _Alpha;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					uniform lowp sampler2D _DetailTex;
					varying mediump vec4 vs_TEXCOORD0;
					varying mediump vec4 vs_TEXCOORD1;
					varying highp vec2 vs_TEXCOORD2;
					#define SV_Target0 gl_FragData[0]
					vec3 u_xlat0;
					mediump vec3 u_xlat16_0;
					lowp vec3 u_xlat10_0;
					vec2 u_xlat1;
					lowp vec3 u_xlat10_1;
					mediump vec3 u_xlat16_2;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat10_1.xyz = texture2D(_DetailTex, vs_TEXCOORD0.zw).xyz;
					    u_xlat16_2.xyz = u_xlat10_0.xyz * u_xlat10_1.xyz;
					    u_xlat16_0.xyz = u_xlat16_2.xyz * vs_TEXCOORD1.xyz + (-h3d_FogDistanceColor.xyz);
					    u_xlat1.xy = vs_TEXCOORD2.xy;
					    u_xlat1.xy = clamp(u_xlat1.xy, 0.0, 1.0);
					    u_xlat0.xyz = u_xlat1.xxx * u_xlat16_0.xyz + h3d_FogDistanceColor.xyz;
					    u_xlat0.xyz = u_xlat0.xyz + (-h3d_FogHeightColor.xyz);
					    u_xlat0.xyz = u_xlat1.yyy * u_xlat0.xyz + h3d_FogHeightColor.xyz;
					    SV_Target0.xyz = u_xlat0.xyz;
					    u_xlat10_0.xy = texture2D(_MaskTex, vs_TEXCOORD0.xy).xy;
					    u_xlat16_2.x = u_xlat10_0.y * u_xlat10_0.x;
					    u_xlat16_2.x = u_xlat16_2.x * _Alpha;
					    SV_Target0.w = u_xlat16_2.x * vs_TEXCOORD1.w;
					    return;
					}
					
					#endif"
				}
			}
			Program "fp" {
				SubProgram "gles hw_tier00 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "_ON_H3D_H_FOG" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "_ON_H3D_H_FOG" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "_ON_H3D_H_FOG" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "_ON_H3D_FOG" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "_ON_H3D_FOG" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "_ON_H3D_FOG" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "_ON_H3D_FOG" "_ON_H3D_H_FOG" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "_ON_H3D_FOG" "_ON_H3D_H_FOG" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "_ON_H3D_FOG" "_ON_H3D_H_FOG" }
					"!!!!GLES"
				}
			}
		}
	}
	Fallback "H3D/InGame/SceneStaticObjects/SimpleDiffuse ( no Supports Lightmap)"
}