Shader "Custom/LogoShader" {
	Properties {
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_ScrollTex ("Sprite Texture", 2D) = "white" {}
		_StencilComp ("Stencil Comparison", Float) = 0
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255
		_ColorMask ("Color Mask", Float) = 15
	}
	SubShader {
		Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
		Pass {
			Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha, SrcAlpha OneMinusSrcAlpha
			ColorMask 0 -1
			Stencil {
				ReadMask 0
				WriteMask 0
				Comp Disabled
				Pass Keep
				Fail Keep
				ZFail Keep
			}
			GpuProgramID 27244
			Program "vp" {
				SubProgram "gles hw_tier00 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 _MainTex_ST;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_COLOR0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					varying highp vec4 vs_COLOR0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    vs_COLOR0 = in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _ScrollTex;
					varying highp vec2 vs_TEXCOORD0;
					varying highp vec4 vs_COLOR0;
					#define SV_Target0 gl_FragData[0]
					vec4 u_xlat0;
					lowp vec4 u_xlat10_0;
					vec4 u_xlat1;
					bool u_xlatb2;
					float u_xlat4;
					bool u_xlatb4;
					float u_xlat5;
					bool u_xlatb7;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat10_0 = texture2D(_MainTex, vs_TEXCOORD0.xy);
					    u_xlat1.x = _Time.y * 0.333333343;
					    u_xlat1.x = trunc(u_xlat1.x);
					    u_xlat1.x = (-u_xlat1.x) * 3.0 + _Time.y;
					    u_xlatb4 = 0.200000003<u_xlat1.x;
					    if(u_xlatb4){
					        u_xlat1.x = u_xlat1.x + -0.200000003;
					        u_xlat4 = u_xlat1.x * 0.899999976;
					        u_xlat1.x = u_xlat1.x * 0.899999976 + -0.800000012;
					        u_xlatb7 = u_xlat1.x<vs_TEXCOORD0.x;
					        u_xlatb4 = vs_TEXCOORD0.x<u_xlat4;
					        u_xlatb4 = u_xlatb4 && u_xlatb7;
					        if(u_xlatb4){
					            u_xlat1.x = (-u_xlat1.x) + vs_TEXCOORD0.x;
					            u_xlat1.x = u_xlat1.x * 1.25;
					            u_xlat1.y = vs_TEXCOORD0.y;
					            u_xlat1 = texture2D(_ScrollTex, u_xlat1.xy);
					        } else {
					            u_xlat1.x = float(1.0);
					            u_xlat1.y = float(1.0);
					            u_xlat1.z = float(1.0);
					            u_xlat1.w = float(0.0);
					        //ENDIF
					        }
					    } else {
					        u_xlat1.x = float(1.0);
					        u_xlat1.y = float(1.0);
					        u_xlat1.z = float(1.0);
					        u_xlat1.w = float(0.0);
					    //ENDIF
					    }
					    u_xlatb2 = 0.5<u_xlat10_0.w;
					    u_xlat5 = (-u_xlat1.w) + 1.0;
					    u_xlat0 = u_xlat10_0 * vec4(u_xlat5);
					    u_xlat0 = u_xlat1 * u_xlat1.wwww + u_xlat0;
					    u_xlat0.w = u_xlat0.w * vs_COLOR0.w;
					    SV_Target0 = mix(vec4(0.0, 0.0, 0.0, 0.0), u_xlat0, vec4(bvec4(u_xlatb2)));
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 _MainTex_ST;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_COLOR0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					varying highp vec4 vs_COLOR0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    vs_COLOR0 = in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _ScrollTex;
					varying highp vec2 vs_TEXCOORD0;
					varying highp vec4 vs_COLOR0;
					#define SV_Target0 gl_FragData[0]
					vec4 u_xlat0;
					lowp vec4 u_xlat10_0;
					vec4 u_xlat1;
					bool u_xlatb2;
					float u_xlat4;
					bool u_xlatb4;
					float u_xlat5;
					bool u_xlatb7;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat10_0 = texture2D(_MainTex, vs_TEXCOORD0.xy);
					    u_xlat1.x = _Time.y * 0.333333343;
					    u_xlat1.x = trunc(u_xlat1.x);
					    u_xlat1.x = (-u_xlat1.x) * 3.0 + _Time.y;
					    u_xlatb4 = 0.200000003<u_xlat1.x;
					    if(u_xlatb4){
					        u_xlat1.x = u_xlat1.x + -0.200000003;
					        u_xlat4 = u_xlat1.x * 0.899999976;
					        u_xlat1.x = u_xlat1.x * 0.899999976 + -0.800000012;
					        u_xlatb7 = u_xlat1.x<vs_TEXCOORD0.x;
					        u_xlatb4 = vs_TEXCOORD0.x<u_xlat4;
					        u_xlatb4 = u_xlatb4 && u_xlatb7;
					        if(u_xlatb4){
					            u_xlat1.x = (-u_xlat1.x) + vs_TEXCOORD0.x;
					            u_xlat1.x = u_xlat1.x * 1.25;
					            u_xlat1.y = vs_TEXCOORD0.y;
					            u_xlat1 = texture2D(_ScrollTex, u_xlat1.xy);
					        } else {
					            u_xlat1.x = float(1.0);
					            u_xlat1.y = float(1.0);
					            u_xlat1.z = float(1.0);
					            u_xlat1.w = float(0.0);
					        //ENDIF
					        }
					    } else {
					        u_xlat1.x = float(1.0);
					        u_xlat1.y = float(1.0);
					        u_xlat1.z = float(1.0);
					        u_xlat1.w = float(0.0);
					    //ENDIF
					    }
					    u_xlatb2 = 0.5<u_xlat10_0.w;
					    u_xlat5 = (-u_xlat1.w) + 1.0;
					    u_xlat0 = u_xlat10_0 * vec4(u_xlat5);
					    u_xlat0 = u_xlat1 * u_xlat1.wwww + u_xlat0;
					    u_xlat0.w = u_xlat0.w * vs_COLOR0.w;
					    SV_Target0 = mix(vec4(0.0, 0.0, 0.0, 0.0), u_xlat0, vec4(bvec4(u_xlatb2)));
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					uniform 	vec4 _MainTex_ST;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_COLOR0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					varying highp vec4 vs_COLOR0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    vs_COLOR0 = in_COLOR0;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _ScrollTex;
					varying highp vec2 vs_TEXCOORD0;
					varying highp vec4 vs_COLOR0;
					#define SV_Target0 gl_FragData[0]
					vec4 u_xlat0;
					lowp vec4 u_xlat10_0;
					vec4 u_xlat1;
					bool u_xlatb2;
					float u_xlat4;
					bool u_xlatb4;
					float u_xlat5;
					bool u_xlatb7;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat10_0 = texture2D(_MainTex, vs_TEXCOORD0.xy);
					    u_xlat1.x = _Time.y * 0.333333343;
					    u_xlat1.x = trunc(u_xlat1.x);
					    u_xlat1.x = (-u_xlat1.x) * 3.0 + _Time.y;
					    u_xlatb4 = 0.200000003<u_xlat1.x;
					    if(u_xlatb4){
					        u_xlat1.x = u_xlat1.x + -0.200000003;
					        u_xlat4 = u_xlat1.x * 0.899999976;
					        u_xlat1.x = u_xlat1.x * 0.899999976 + -0.800000012;
					        u_xlatb7 = u_xlat1.x<vs_TEXCOORD0.x;
					        u_xlatb4 = vs_TEXCOORD0.x<u_xlat4;
					        u_xlatb4 = u_xlatb4 && u_xlatb7;
					        if(u_xlatb4){
					            u_xlat1.x = (-u_xlat1.x) + vs_TEXCOORD0.x;
					            u_xlat1.x = u_xlat1.x * 1.25;
					            u_xlat1.y = vs_TEXCOORD0.y;
					            u_xlat1 = texture2D(_ScrollTex, u_xlat1.xy);
					        } else {
					            u_xlat1.x = float(1.0);
					            u_xlat1.y = float(1.0);
					            u_xlat1.z = float(1.0);
					            u_xlat1.w = float(0.0);
					        //ENDIF
					        }
					    } else {
					        u_xlat1.x = float(1.0);
					        u_xlat1.y = float(1.0);
					        u_xlat1.z = float(1.0);
					        u_xlat1.w = float(0.0);
					    //ENDIF
					    }
					    u_xlatb2 = 0.5<u_xlat10_0.w;
					    u_xlat5 = (-u_xlat1.w) + 1.0;
					    u_xlat0 = u_xlat10_0 * vec4(u_xlat5);
					    u_xlat0 = u_xlat1 * u_xlat1.wwww + u_xlat0;
					    u_xlat0.w = u_xlat0.w * vs_COLOR0.w;
					    SV_Target0 = mix(vec4(0.0, 0.0, 0.0, 0.0), u_xlat0, vec4(bvec4(u_xlatb2)));
					    return;
					}
					
					#endif"
				}
			}
			Program "fp" {
				SubProgram "gles hw_tier00 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					"!!!!GLES"
				}
			}
		}
	}
}