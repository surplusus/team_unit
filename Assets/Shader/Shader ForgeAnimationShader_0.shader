Shader "Shader Forge/AnimationShader" {
	Properties {
		_Tex ("Tex", 2D) = "white" {}
		_U ("U", Float) = 7
		_V ("V", Float) = 7
		_FPS ("FPS", Float) = 50
		[HideInInspector] _Cutoff ("Alpha cutoff", Range(0, 1)) = 0.5
	}
	SubShader {
		Tags { "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
		Pass {
			Name "FORWARD"
			Tags { "IGNOREPROJECTOR" = "true" "LIGHTMODE" = "FORWARDBASE" "QUEUE" = "Transparent" "RenderType" = "Transparent" "SHADOWSUPPORT" = "true" }
			Blend SrcAlpha OneMinusSrcAlpha, SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			GpuProgramID 54920
			Program "vp" {
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" "VERTEXLIGHT_ON" }
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
					attribute highp vec4 in_POSITION0;
					attribute highp vec2 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					void main()
					{
					    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
					    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Time;
					uniform 	vec4 _TimeEditor;
					uniform 	vec4 _Tex_ST;
					uniform 	float _U;
					uniform 	float _V;
					uniform 	float _FPS;
					uniform lowp sampler2D _Tex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec2 u_xlat0;
					vec2 u_xlat1;
					vec2 u_xlat2;
					float u_xlat3;
					float trunc(float x) { return sign(x)*floor(abs(x)); }
					vec2 trunc(vec2 x) { return sign(x)*floor(abs(x)); }
					vec3 trunc(vec3 x) { return sign(x)*floor(abs(x)); }
					vec4 trunc(vec4 x) { return sign(x)*floor(abs(x)); }
					
					void main()
					{
					    u_xlat0.x = _Time.y + _TimeEditor.y;
					    u_xlat0.x = u_xlat0.x * _FPS;
					    u_xlat0.x = trunc(u_xlat0.x);
					    u_xlat1.x = float(1.0) / _U;
					    u_xlat3 = u_xlat0.x * u_xlat1.x;
					    u_xlat3 = floor(u_xlat3);
					    u_xlat2.x = (-_U) * u_xlat3 + u_xlat0.x;
					    u_xlat0.y = u_xlat3 + (-vs_TEXCOORD0.y);
					    u_xlat0.x = vs_TEXCOORD0.x;
					    u_xlat2.y = 1.0;
					    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
					    u_xlat1.y = float(1.0) / (-_V);
					    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
					    u_xlat0.xy = u_xlat0.xy * _Tex_ST.xy + _Tex_ST.zw;
					    SV_Target0 = texture2D(_Tex, u_xlat0.xy);
					    return;
					}
					
					#endif"
				}
			}
			Program "fp" {
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "LIGHTPROBE_SH" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier00 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" }
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					Keywords { "DIRECTIONAL" "SHADOWS_SCREEN" "LIGHTPROBE_SH" }
					"!!!!GLES"
				}
			}
		}
	}
	Fallback "Diffuse"
	CustomEditor "ShaderForgeMaterialInspector"
}