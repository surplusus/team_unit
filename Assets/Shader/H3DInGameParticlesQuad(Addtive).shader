Shader "H3D/InGame/Particles/Quad(Addtive)" {
	Properties {
		_MainTex ("Base texture", 2D) = "white" {}
		_MaskTex ("Mask(Alpha)", 2D) = "white" {}
		_Color ("Color", Vector) = (1,1,1,1)
		_Angle ("Angle", Float) = 0
		_RotateSpeed ("RotateSpeed", Float) = 0
	}
	SubShader {
		LOD 200
		Tags { "DisableBatching" = "true" "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
		Pass {
			LOD 200
			Tags { "DisableBatching" = "true" "IGNOREPROJECTOR" = "true" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
			Blend SrcAlpha One, SrcAlpha One
			ColorMask RGB -1
			ZWrite Off
			Cull Off
			GpuProgramID 17294
			Program "vp" {
				SubProgram "gles hw_tier00 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4glstate_matrix_projection[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	float _Angle;
					uniform 	float _RotateSpeed;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					bool u_xlatb5;
					void main()
					{
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3].yyyy * hlslcc_mtx4x4unity_MatrixV[1];
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[0] * hlslcc_mtx4x4unity_ObjectToWorld[3].xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[2] * hlslcc_mtx4x4unity_ObjectToWorld[3].zzzz + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[3] * hlslcc_mtx4x4unity_ObjectToWorld[3].wwww + u_xlat0;
					    u_xlat1.x = _RotateSpeed * _Time.y + _Angle;
					    u_xlat1.x = u_xlat1.x * 0.00277777785;
					    u_xlatb5 = u_xlat1.x>=(-u_xlat1.x);
					    u_xlat1.x = fract(abs(u_xlat1.x));
					    u_xlat1.x = (u_xlatb5) ? u_xlat1.x : (-u_xlat1.x);
					    u_xlat1.x = u_xlat1.x * 6.28318548;
					    u_xlat2.x = cos(u_xlat1.x);
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat3.x = (-u_xlat1.x);
					    u_xlat3.y = u_xlat2.x;
					    u_xlat3.z = u_xlat1.x;
					    u_xlat1.x = dot(u_xlat3.xy, u_xlat3.xy);
					    u_xlat1.x = inversesqrt(u_xlat1.x);
					    u_xlat1.xy = u_xlat1.xx * u_xlat3.yx;
					    u_xlat1.x = dot(u_xlat1.xy, in_POSITION0.xy);
					    u_xlat2.x = dot(u_xlat3.yz, u_xlat3.yz);
					    u_xlat2.x = inversesqrt(u_xlat2.x);
					    u_xlat2.xy = u_xlat2.xx * u_xlat3.zy;
					    u_xlat1.y = dot(u_xlat2.xy, in_POSITION0.xy);
					    u_xlat2.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
					    u_xlat2.x = sqrt(u_xlat2.x);
					    u_xlat3.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
					    u_xlat2.y = sqrt(u_xlat3.x);
					    u_xlat1.z = in_POSITION0.z;
					    u_xlat1.w = 0.0;
					    u_xlat2.z = float(1.0);
					    u_xlat2.w = float(1.0);
					    u_xlat0 = u_xlat1 * u_xlat2 + u_xlat0;
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4glstate_matrix_projection[1];
					    u_xlat1 = hlslcc_mtx4x4glstate_matrix_projection[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4glstate_matrix_projection[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4glstate_matrix_projection[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Color;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec4 u_xlat0;
					lowp vec3 u_xlat10_0;
					lowp float u_xlat10_1;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat0.xyz = u_xlat10_0.xyz * _Color.xyz;
					    u_xlat10_1 = texture2D(_MaskTex, vs_TEXCOORD0.xy).x;
					    u_xlat0.w = u_xlat10_1 * _Color.w;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier01 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4glstate_matrix_projection[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	float _Angle;
					uniform 	float _RotateSpeed;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					bool u_xlatb5;
					void main()
					{
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3].yyyy * hlslcc_mtx4x4unity_MatrixV[1];
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[0] * hlslcc_mtx4x4unity_ObjectToWorld[3].xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[2] * hlslcc_mtx4x4unity_ObjectToWorld[3].zzzz + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[3] * hlslcc_mtx4x4unity_ObjectToWorld[3].wwww + u_xlat0;
					    u_xlat1.x = _RotateSpeed * _Time.y + _Angle;
					    u_xlat1.x = u_xlat1.x * 0.00277777785;
					    u_xlatb5 = u_xlat1.x>=(-u_xlat1.x);
					    u_xlat1.x = fract(abs(u_xlat1.x));
					    u_xlat1.x = (u_xlatb5) ? u_xlat1.x : (-u_xlat1.x);
					    u_xlat1.x = u_xlat1.x * 6.28318548;
					    u_xlat2.x = cos(u_xlat1.x);
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat3.x = (-u_xlat1.x);
					    u_xlat3.y = u_xlat2.x;
					    u_xlat3.z = u_xlat1.x;
					    u_xlat1.x = dot(u_xlat3.xy, u_xlat3.xy);
					    u_xlat1.x = inversesqrt(u_xlat1.x);
					    u_xlat1.xy = u_xlat1.xx * u_xlat3.yx;
					    u_xlat1.x = dot(u_xlat1.xy, in_POSITION0.xy);
					    u_xlat2.x = dot(u_xlat3.yz, u_xlat3.yz);
					    u_xlat2.x = inversesqrt(u_xlat2.x);
					    u_xlat2.xy = u_xlat2.xx * u_xlat3.zy;
					    u_xlat1.y = dot(u_xlat2.xy, in_POSITION0.xy);
					    u_xlat2.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
					    u_xlat2.x = sqrt(u_xlat2.x);
					    u_xlat3.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
					    u_xlat2.y = sqrt(u_xlat3.x);
					    u_xlat1.z = in_POSITION0.z;
					    u_xlat1.w = 0.0;
					    u_xlat2.z = float(1.0);
					    u_xlat2.w = float(1.0);
					    u_xlat0 = u_xlat1 * u_xlat2 + u_xlat0;
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4glstate_matrix_projection[1];
					    u_xlat1 = hlslcc_mtx4x4glstate_matrix_projection[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4glstate_matrix_projection[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4glstate_matrix_projection[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Color;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec4 u_xlat0;
					lowp vec3 u_xlat10_0;
					lowp float u_xlat10_1;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat0.xyz = u_xlat10_0.xyz * _Color.xyz;
					    u_xlat10_1 = texture2D(_MaskTex, vs_TEXCOORD0.xy).x;
					    u_xlat0.w = u_xlat10_1 * _Color.w;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
				SubProgram "gles hw_tier02 " {
					"!!!!GLES
					#ifdef VERTEX
					#version 100
					
					uniform 	vec4 _Time;
					uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
					uniform 	vec4 hlslcc_mtx4x4glstate_matrix_projection[4];
					uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
					uniform 	mediump vec4 _MainTex_ST;
					uniform 	float _Angle;
					uniform 	float _RotateSpeed;
					attribute highp vec4 in_POSITION0;
					attribute highp vec4 in_TEXCOORD0;
					varying highp vec2 vs_TEXCOORD0;
					vec4 u_xlat0;
					vec4 u_xlat1;
					vec4 u_xlat2;
					vec3 u_xlat3;
					bool u_xlatb5;
					void main()
					{
					    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3].yyyy * hlslcc_mtx4x4unity_MatrixV[1];
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[0] * hlslcc_mtx4x4unity_ObjectToWorld[3].xxxx + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[2] * hlslcc_mtx4x4unity_ObjectToWorld[3].zzzz + u_xlat0;
					    u_xlat0 = hlslcc_mtx4x4unity_MatrixV[3] * hlslcc_mtx4x4unity_ObjectToWorld[3].wwww + u_xlat0;
					    u_xlat1.x = _RotateSpeed * _Time.y + _Angle;
					    u_xlat1.x = u_xlat1.x * 0.00277777785;
					    u_xlatb5 = u_xlat1.x>=(-u_xlat1.x);
					    u_xlat1.x = fract(abs(u_xlat1.x));
					    u_xlat1.x = (u_xlatb5) ? u_xlat1.x : (-u_xlat1.x);
					    u_xlat1.x = u_xlat1.x * 6.28318548;
					    u_xlat2.x = cos(u_xlat1.x);
					    u_xlat1.x = sin(u_xlat1.x);
					    u_xlat3.x = (-u_xlat1.x);
					    u_xlat3.y = u_xlat2.x;
					    u_xlat3.z = u_xlat1.x;
					    u_xlat1.x = dot(u_xlat3.xy, u_xlat3.xy);
					    u_xlat1.x = inversesqrt(u_xlat1.x);
					    u_xlat1.xy = u_xlat1.xx * u_xlat3.yx;
					    u_xlat1.x = dot(u_xlat1.xy, in_POSITION0.xy);
					    u_xlat2.x = dot(u_xlat3.yz, u_xlat3.yz);
					    u_xlat2.x = inversesqrt(u_xlat2.x);
					    u_xlat2.xy = u_xlat2.xx * u_xlat3.zy;
					    u_xlat1.y = dot(u_xlat2.xy, in_POSITION0.xy);
					    u_xlat2.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
					    u_xlat2.x = sqrt(u_xlat2.x);
					    u_xlat3.x = dot(hlslcc_mtx4x4unity_ObjectToWorld[1].xyz, hlslcc_mtx4x4unity_ObjectToWorld[1].xyz);
					    u_xlat2.y = sqrt(u_xlat3.x);
					    u_xlat1.z = in_POSITION0.z;
					    u_xlat1.w = 0.0;
					    u_xlat2.z = float(1.0);
					    u_xlat2.w = float(1.0);
					    u_xlat0 = u_xlat1 * u_xlat2 + u_xlat0;
					    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4glstate_matrix_projection[1];
					    u_xlat1 = hlslcc_mtx4x4glstate_matrix_projection[0] * u_xlat0.xxxx + u_xlat1;
					    u_xlat1 = hlslcc_mtx4x4glstate_matrix_projection[2] * u_xlat0.zzzz + u_xlat1;
					    gl_Position = hlslcc_mtx4x4glstate_matrix_projection[3] * u_xlat0.wwww + u_xlat1;
					    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
					    return;
					}
					
					#endif
					#ifdef FRAGMENT
					#version 100
					
					#ifdef GL_FRAGMENT_PRECISION_HIGH
					    precision highp float;
					#else
					    precision mediump float;
					#endif
					precision highp int;
					uniform 	vec4 _Color;
					uniform lowp sampler2D _MainTex;
					uniform lowp sampler2D _MaskTex;
					varying highp vec2 vs_TEXCOORD0;
					#define SV_Target0 gl_FragData[0]
					vec4 u_xlat0;
					lowp vec3 u_xlat10_0;
					lowp float u_xlat10_1;
					void main()
					{
					    u_xlat10_0.xyz = texture2D(_MainTex, vs_TEXCOORD0.xy).xyz;
					    u_xlat0.xyz = u_xlat10_0.xyz * _Color.xyz;
					    u_xlat10_1 = texture2D(_MaskTex, vs_TEXCOORD0.xy).x;
					    u_xlat0.w = u_xlat10_1 * _Color.w;
					    SV_Target0 = u_xlat0;
					    return;
					}
					
					#endif"
				}
			}
			Program "fp" {
				SubProgram "gles hw_tier00 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier01 " {
					"!!!!GLES"
				}
				SubProgram "gles hw_tier02 " {
					"!!!!GLES"
				}
			}
		}
	}
}