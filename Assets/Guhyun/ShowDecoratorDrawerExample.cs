﻿using UnityEngine;
using System.Collections;

public class ShowDecoratorDrawerExample : MonoBehaviour
{
    public int a = 1;
    public int b = 2;
    public int c = 3;

    // this shows our custom Decorator Drawer between the groups of properties
    [ColorSpacer(30, 30, 100, 1, 0, 0)]

    public string d = "d";
    public string e = "e";
    public string f = "f";
}